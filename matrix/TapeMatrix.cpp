#include <iostream>
#include <vector>

using namespace std;

struct ribbon {
	vector<int> r1;
	vector<int> r2;
	vector<int> r3;
	vector<int> r4;
	vector<int> r5;
};

ribbon get_ribbon_matrix(int* mt, int n) {
	ribbon res;

	int j = 0;
	for(int i = (n + 1) / 2; i < n; i++) {
		res.r1.push_back(mt[i + j*n]);
		j++;
	}

	j = 0;
	for(int i = (n + 1) / 2; i < n; i++) {
		res.r5.push_back(mt[j + i*n]);
		j++;
	}

	j = 1;
	for(int i = 0; i < n - 1; i++) {
		res.r2.push_back(mt[n*i + j]);
		j++;
	}

	j = 0;
	for(int i = 1; i < n; i++) {
		res.r4.push_back(mt[n*i + j]);
		j++;
	}

	j = 0;
	for(int i = 0; i < n; i++) {
		res.r3.push_back(mt[n*i + j]);
		j++;
	}



	return res;
}

void mul_on_vector(ribbon rib, int* x, int n, int* res) {

	for(int i = 0; i < n; i++) {
		res[i] += x[i] * rib.r3[i];
	}

	for(int i = 1; i < n; i++) {
		res[i - 1] += x[i] * rib.r2[i - 1];
	}

	for(int i = 0; i < n - 1; i++) {
		res[i + 1] += x[i] * rib.r4[i];
	}

	int j = n / 2;
	for(int i = 0; i < n / 2; i++) {
		res[i] += x[j] * rib.r1[i];
		res[j] += x[i] * rib.r5[i];
		j++;
	}

}

int main() {
	const int n = 5;
	int* mt = new int[n * n];

	for(int i = 0; i < n * n; i++) {
		mt[i] = 0;
	}

	mt[0] = 1; mt[1] = 7; mt[3] = 8;
	mt[5] = 6; mt[6] = 2; mt[7] = 9; mt[9] = 3;
	mt[11] = 8; mt[12] = 3; mt[13] = 7;
	mt[15] = 5; mt[17] = 5; mt[18] = 4; mt[19] = 2;
	mt[21] = 1; mt[23] = 1; mt[24] = 5;

	for(int i = 0; i < n; i++) {

		for (int j = 0; j < n; j++) {
			cout << mt[i*n + j] << " ";
		}

		cout << endl;
	}

	ribbon rib = get_ribbon_matrix(mt, n);

	cout << endl;
	for(int i = 0; i < rib.r1.size(); i++) {
		cout << rib.r1[i];
	}
	cout << endl;
	for(int i = 0; i < rib.r2.size(); i++) {
		cout << rib.r2[i];
	}
	cout << endl;
	for(int i = 0; i < rib.r3.size(); i++) {
		cout << rib.r3[i];
	}
	cout << endl;
	for(int i = 0; i < rib.r4.size(); i++) {
		cout << rib.r4[i];
	}
	cout << endl;
	for(int i = 0; i < rib.r5.size(); i++) {
		cout << rib.r5[i];
	}

	int* x = new int[n];
	int* res = new int[n];
	
	x[0] = 2; x[1] = 4; x[2] = 6; x[3] = 8; x[4] = 1;
	
	for(int i = 0; i < n; i++) {
		res[i] = 0;
	}

	mul_on_vector(rib, x, n, res);

	cout << endl;
	for(int i = 0; i < n; i++) {
		cout << res[i] << " ";
	}

	cin.get();
}