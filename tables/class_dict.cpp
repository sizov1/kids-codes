#include <iostream>
#include <string>
#include <vector>
using namespace std;

// �����
// �������
// ��������

class Word
{
    private:
        string name_;
        string defintion_;
        int length_;
    public:
        Word();
        Word(string name, string definition);

        bool operator == (const Word& w) const;
        bool operator > (const Word& w) const;
        bool operator < (const Word& w) const;
        string GetName() { return name_; }
        string GetDefinition() { return defintion_; }

        friend ostream& operator << (ostream& os, const Word& w);

        friend class Dictionary;
};
Word::Word()
{
    name_ = " ";
    defintion_ = " ";
}
Word::Word(string name, string definition)
{
    bool isWord = true;
    int length = name.length();
    for (int i = 0; i < length; i++) {
        if (!isalpha(name[i])) {
            isWord = false;
            break;
        }
    }
    
    if (!isWord) {
        throw ("cannot create: is not a word");
    }

    name_ = name;
    defintion_ = definition;
    length_ = name_.length();
}

bool Word::operator == (const Word& w) const
{
    if (length_ != w.length_) {
        return false;
    }

    for (int i = 0; i < length_; i++) {

        if (name_[i] != w.name_[i]) {
            return false;
        }

    }

    return true;
}
        
bool Word::operator < (const Word& w) const
{
    int length = length_ > w.length_ ? w.length_ : length_;
    for (int i = 0; i < length; i++) {

        if (name_[i] == w.name_[i]) {
            continue;
        }
        else if (name_[i] > w.name_[i]) {
            return true;
        }
        else {
            return false;
        }

    }
}

bool Word::operator > (const Word& w) const
{
    return !(((*this) < w) | ((*this) == w));
}

ostream& operator << (ostream& os, const Word& w)
{
    os << w.name_ << " " << w.defintion_;
    return os;
}

class Dictionary
{
    private:
        Word* dict;
        int size;
    public:
        Dictionary();
        void Insert(string name, string definition);
        void Delete(string name);
        string Find(string name, int left, int right);
        int GetSize() {return size;}
        friend ostream& operator << (ostream& os, const Dictionary& dic);
};

Dictionary::Dictionary()
{
    dict = new Word[20000];
    size = 0;
}

void Dictionary::Insert(string name, string definition)
{
    Word ins(name, definition);
    int k = 0;
        
    // ���� ����� ��� �������
    for (int i = 0; i < size; i++) {
        k++;
        if (dict[i] == ins) {
            return;
        }
        else if (dict[i] > ins) {
            break;
        }

    }

    Word* temp = new Word[20000];
    // �������� �������, �������� ������� �� ������ �����
    for (int j = 0; j < size + 1; j++) {
        if (k > j) {
            temp[j] = dict[j];
        }
        else if (j > k) {
            temp[j] = dict[j - 1];
        }
        else {
            temp[j] = ins;
        }
    }

    dict = &temp[0];
    size++;
}

void Dictionary::Delete(string name)
{
    int k = 0;

    // ���� ������� ������� ���� �������
    for (int i = 0; i < size; i++) {
        k++;
        if (dict[i].name_ == name) {
            break;
        }
    }

    if (k == size && dict[k].name_ != name) {
        throw ("cannot delete: word not found");
    }

    Word* temp = new Word[20000];
    // ������� �������� ������� ��� ��� ���������� �����
    for (int j = 0; j < size - 1; j++) {
        if (k < j) {
            temp[j] = dict[j];
        }
        else if (k >= j) {
            temp[j] = dict[j + 1];
        }
    }

    dict = temp;
    size--;
}

string Dictionary::Find(string name, int left, int right)
{
    int midd = 0;
    while (true) {
        midd = (left + right)/2;
        
        if (name < dict[midd].name_) {
            right = midd - 1;
        }
        else if (name > dict[midd].name_) {
            left = midd + 1;
        }
        else {
            return dict[midd].defintion_;
        }

        if (left > right) {
            throw ("cannot find");
        }
    }
}

ostream& operator << (ostream& os, const Dictionary& dic)
{
    for (int i = 0; i < dic.size; i++) {
        os << dic.dict[i] << endl;
    }
    return os;
}

int main()
{
    Dictionary Ojegov;

    Ojegov.Insert("aa", "AA");
    Ojegov.Insert("bb", "BB");
    Ojegov.Insert("ba", "BA");
    Ojegov.Insert("ab", "AB");
    cout << Ojegov;
    Ojegov.Delete("aa");
    cout << "after delete" << endl << Ojegov;

    cout << "FIND(ab) = " << Ojegov.Find("ab", 0, Ojegov.GetSize()) << endl;
    system("pause");
}

