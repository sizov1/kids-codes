#include <iostream>
#include <vector>
#include <algorithm>
#include <list>
#include <string>

using namespace std;

class Word;

typedef list<Word> wordsRow;
typedef vector<wordsRow> wordsList;



class Word
{
    private:
        pair<string, string> _word;
    public:
        
        Word(string name = " ", string def = " ") {
            _word.first = name;
            _word.second = def;
        }

        string first() const { return _word.first; }
        string second() const { return _word.second; }

        bool operator == (const Word& w) const {
            return (w.first() == _word.first);
        }

        int HashFuction()
        {
            string tmp = _word.first;
            int res = 0, length = tmp.length();
            for (auto it = tmp.cbegin(); it != tmp.cend(); it++) {
                res += int((*it));
            }
            return res % 256;
        }

        
};


class DictionaryHashTable
{
    private:
        wordsList dict;

    public:
        DictionaryHashTable();

        bool Find(string name);
        string GetDefenition(string name);
        void Insert(string name, string def);
        void Delete(string name);

        void print();
};

DictionaryHashTable::DictionaryHashTable()
{
    if (dict.size() == (size_t)0) {
        dict.resize(256);
    }
}

bool DictionaryHashTable::Find(string name)
{
      Word w(name);
      wordsRow row = dict[w.HashFuction()];   // ��������� ����� ������ � ���������� � ���
      auto it = find(row.begin(), row.end(), w); // ���� � ��������� ������ ������ �����
      if (it != row.end()) {
          return true;
      }
      return false;
}

void DictionaryHashTable::Insert(string name, string def)
{
    Word ins(name, def);
    if (!Find(name)) {
        dict[ins.HashFuction()].push_back(ins);  // ��������� ����� ������ ������ 
                                                 // � ��������� � �� ����� �����
    }
}

void DictionaryHashTable::Delete(string name)
{
    Word del(name);
    wordsRow* row = &dict[del.HashFuction()];   // ��������� ����� ������ � ���������� � ���
    auto it = find(row->begin(), row->end(), del); // ���� � ��������� ������ ������ �����
    if (it != row->end()) {
        row->erase(it);  // ������� ����� �� ���������� �����
        return;
    }
}

void DictionaryHashTable::print()
{
    for (auto it1 : dict) {
        for (auto it2 : it1) {
            cout << it2.first() << " - " << it2.second() << "; ";
        }
        if (!it1.empty()) {
            cout << endl;
        }
    }
}

int main()
{
    DictionaryHashTable engDict;
    engDict.Insert("a", "A");
    engDict.Insert("ab", "AB");
    engDict.Insert("ba", "BA");
    engDict.Insert("abc", "ABC");
    engDict.Insert("cba", "CBA");
    engDict.Insert("bac", "BAC");
    engDict.print();

    cout << endl << endl;
    engDict.Delete("ab");
    engDict.print();

    cout << engDict.Find("cba") << endl;
    system("pause");
}

