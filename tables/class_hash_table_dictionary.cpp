#include <iostream>
#include <algorithm>
#include <string>
#include <list>

#define MAX_SIZE 256

using namespace std;

// ������� �������� ����� 

class Word
{
    protected:
        string name_;
        int length_;
    public:
        Word();
        Word(string name);

        bool operator == (const Word& w) const;
        bool operator > (const Word& w) const;
        bool operator < (const Word& w) const;
        string GetName() const { return name_; }

        int GetSum();

        friend ostream& operator << (ostream& os, const Word& w);

        friend class Dictionary;
};

Word::Word()
{
    name_ = " ";
}

Word::Word(string name)
{
    bool isWord = true;
    int length = name.length();
    for (int i = 0; i < length; i++) {
        if (!isalpha(name[i])) {
            isWord = false;
            break;
        }
    }

    if (!isWord) {
        throw ("cannot create: is not a word");
    }

    name_ = name;
    length_ = name_.length();
}

int Word::GetSum()
{
    int res = 0, length = name_.length();
    for (auto it = name_.begin(); it != name_.end(); it++) {
        res += int((*it));
    }
    return res % 256;
}

bool Word::operator == (const Word& w) const
{
    if (length_ != w.length_) {
        return false;
    }

    for (int i = 0; i < length_; i++) {

        if (name_[i] != w.name_[i]) {
            return false;
        }

    }

    return true;
}

bool Word::operator < (const Word& w) const
{
    int length = length_ > w.length_ ? w.length_ : length_;
    for (int i = 0; i < length; i++) {

        if (name_[i] == w.name_[i]) {
            continue;
        }
        else if (name_[i] > w.name_[i]) {
            return true;
        }
        else {
            return false;
        }

    }
}

bool Word::operator > (const Word& w) const
{
    return !(((*this) < w) | ((*this) == w));
}

class WordDef : public Word
{
    private:
        string definition_;        
    public:
        WordDef(string name = " ", string definition = " ");

        string GetDefinition() { return definition_; }
        bool operator == (const WordDef& w) const;
        friend ostream& operator << (ostream& os, const WordDef& w);
};

bool WordDef::operator == (const WordDef& w) const {
    if (length_ != w.length_) {
        return false;
    }

    for (int i = 0; i < length_; i++) {

        if (name_[i] != w.name_[i]) {
            return false;
        }

    }

    return true;
}

WordDef::WordDef(string name, string definition) : Word(name)
{
    definition_ = definition;
}

ostream& operator << (ostream& os, const WordDef& w)
{
    os << w.name_ << " " << w.definition_;
    return os;
}



class HashTable 
{
    private:
        list<WordDef> table[MAX_SIZE];

        list<WordDef>::iterator Search(string name);
    public:
        HashTable() {};
        
        void Insert(string name, string def);
        void Delete(string name);

        string Find(string name);
        
        friend ostream& operator << (ostream& os, const HashTable& w);
};

list<WordDef>::iterator HashTable::Search(string name)
{
    WordDef tmp(name);
    list<WordDef>* row = &(table[tmp.GetSum()]);
    auto it = find(row->begin(), row->end(), tmp);
    return it;
}

string HashTable::Find(string name)
{
    WordDef tmp(name);
    list<WordDef>* row = &(table[tmp.GetSum()]);
    auto it = find(row->begin(), row->end(), tmp);
    if (it != row->end()) {
        return (*it).GetDefinition();
    }
    else {
        return string("???");
    }
}

void HashTable::Insert(string name, string def)
{
    WordDef ins(name, def);
    int index = ins.GetSum();

    if (Find(name) == "???") {
        table[index].push_back(ins);
    } 
}

void HashTable::Delete(string name)
{
    auto it = Search(name);
    Word tmp(name);
    if (it != table[tmp.GetSum()].end()) {
        table[tmp.GetSum()].erase(it);
        return;
    }
}

ostream& operator << (ostream& os, const HashTable& w)
{
    for (int i = 0; i < MAX_SIZE; i++) {
        if (!(w.table[i]).empty()) {
            cout << i << "  ";
            for (auto it = w.table[i].begin(); it != w.table[i].end(); it++) {
                os << (*it) << "|";
            }
            os << endl;
        }
    }   
    return os;
}


int main()
{   
    HashTable dict;

    dict.Insert("ab", "AB");
    dict.Insert("ba", "BA");
    dict.Insert("c", "C");
    cout << dict;
    
    cout << endl << "dict.Find(ab) = " << dict.Find("ab") << endl;

    dict.Delete("c");
    cout << dict;

    system("pause");
}




