// �������� ����������
// � �������� ���������
// ��� ������: ��������� ��������
// �������, �����, ��������

#include <iostream>
#include <string>

#define DEFAULT "##########"
#define SIZE_TABLE 10000
using namespace std;


bool isNumber(string x)
{
    if (x[0] == '#') return true;
    if (x.length() == 11 || x.length() == 12) {
        auto it = x.begin();
        if (*it = '+') it++;
        for (it; it != x.end(); it++) {
            if (!isdigit(*it)) {
                return false;
            }
        }
        return true;
    }
    else {
        return false;
    }

}

class TelephoneNumber
{
    public:
        string number;
        TelephoneNumber(string num = DEFAULT) {
            if (isNumber(num)) {
                number = num;
            }
        }
};


int Hash(const string& x)
{
    if (!isNumber(x)) return -1;

    int length = x.length();
    size_t pos = (length == 11) ? 4 : 5;
    int hash = 0;
    for (pos; pos < length; pos++) {
        hash *= 10;
        hash += x[pos] - 48;
    }
    return hash % SIZE_TABLE;
}

class HashTable
{
    public:
        pair<TelephoneNumber, bool> table[SIZE_TABLE];
        HashTable();
        void Insert(string number);
        void Delete(string number);
        bool Find(string number);
        void Print();
};

HashTable::HashTable()
{
    for (int i = 0; i < SIZE_TABLE; i++) {
        TelephoneNumber num(DEFAULT);
        table[i].first = num;
        table[i].second = true;
    }
}

void HashTable::Print()
{
    for (int i = 0; i < SIZE_TABLE; i++) {
        if (table[i].first.number != DEFAULT) {
            cout << table[i].first.number << endl;
        }
    }
}

void HashTable::Insert(string number)
{
    int hash = Hash(number);
    int i = 1;
    while (table[hash].first.number != DEFAULT && (i != SIZE_TABLE)) {
        hash = (hash + i) % SIZE_TABLE;
        i++;
    }
    if (i != SIZE_TABLE) {
        TelephoneNumber num(number);
        table[hash].first = num;
        table[hash].second = false;
    }
    else {
        // TABLE IS FULL
    }
}


bool HashTable::Find(string number)
{
    int hash = Hash(number);
    int i = 1;
    while (table[hash].first.number != number && (table[hash].second == false) && (i != SIZE_TABLE)) {
        hash = (hash + i) % SIZE_TABLE;
        i++;
    }

    if ((table[hash].second == false) && (i != SIZE_TABLE)) {
        return true;
    }
    else if ((table[hash].second == true) && (i != SIZE_TABLE)) {
        return false;
    }
    else {
        // TABLE IS FULL AND FIND = FALSE
    }
}

void HashTable::Delete(string number)
{

    int hash = Hash(number);
    int i = 1;
    while (table[hash].first.number != number && (table[hash].second == false) && (i != SIZE_TABLE)) {
        hash = (hash + i) % SIZE_TABLE;
        i++;
    }

    if ((table[hash].second == false) && (i != SIZE_TABLE)) {
        table[hash].first.number = DEFAULT;
    }
    else {
        // NOT FOUND
    }
}


int main()
{
    HashTable nums;

    nums.Insert("+79873945389");
    nums.Insert("89096068166");
    nums.Insert("89658023894");

    nums.Print();

    cout << nums.Find("89096068166");

    nums.Delete("89658023894");

    cout << endl << "-----------------" << endl;
    nums.Print();

    system("pause");
    return 0;
}
