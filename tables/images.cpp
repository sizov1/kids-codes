#define _CRT_SECURE_NO_WARNINGS
#include "readMNIST.h"
#include "stdlib.h"
#include "stdio.h"
#include <iostream>
#include <list>

#define MAX_SIZE 1024
const int image_size = 28 * 28;

using namespace std;



struct Image
{
    uchar pp[image_size];
    Image(uchar* pu) {
        for (int i = 0; i < image_size; i++) {
            pp[i] = *pu;
            pu++;    
        }
    }
};

int HashFunction(Image img)
{   
    int sum = 0;
    const int image_size = 28 * 28;
    for (int i = 0; i < 28; i++) {
        for (int j = 5; j < 24; j++) {
            sum += img.pp[j + i*28];  // ������� ����� ��������� ���������
        }
    }
    return sum % 1024;
}

class HashTable
{
    private:
        list<Image> table[MAX_SIZE];
    public:
            
        int GetMax�ollision();
    
        void Insert(Image img);
        void Delete(Image img);
};

int HashTable::GetMax�ollision()
{   
    int max = 0;
    for (int i = 0; i < MAX_SIZE; i++) {
        int len = table[i].size();
        if (len > max) {
            max = len;
        }
    }
    return max;
}

void HashTable::Insert(Image img)
{
    int index = HashFunction(img);
    table[index].push_back(img);
}

void HashTable::Delete(Image img)
{

}

int main()
{
    uchar* pData;
    int numbers_of_images, size_image;
    pData = read_mnist_images((char*)"images.ubyte", numbers_of_images, size_image);


    HashTable images;
    for (int i = 0; i < 10000; i++) {
        Image ins(pData);
        images.Insert(ins);
        pData += image_size;
    }

    cout << images.GetMax�ollision();
    system("pause");
}