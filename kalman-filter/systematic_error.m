n = 100;
w = 10; % dispersion of white noise
R = w * w;
x = 3; % systematic error 

hatx = 1:n;
K = 1:n;
Ps = 1:n; % P^*
Pv = 1:n; % \tilde{P}

hatx(1) = 0;
Ps(1) = 400;
K(1) = Ps(1) / (Ps(1) + R);
Pv(1) = (1 - K(1)) * Ps(1);
y(1) = x + randn(1) * w;

for k = 2:n
  Ps(k) = Pv(k - 1);
  K(k) = K(k - 1) * (K(k - 1) + 1)^(-1);
  Pv(k) = (1 - K(k)) * Ps(k);
  y(k) = x + randn(1) * w;
  hatx(k) = hatx(k - 1) + K(k) * (y(k) - hatx(k - 1));
endfor

%bar(K); hold on;
subplot(4, 2, 1:4);
for k = 1:n
  plot(ones(2) * k, [0, Ps(k)], "color", "b"); hold on;
endfor
plot(1:n, Ps, "color", "r");
xlabel("k");
ylabel("\\sigma_x", "interpreter", "tex");
stitle = "";
stitle = strcat("x = ", num2str(x));
stitle = strcat(stitle, "; w = ", num2str(w));
stitle = strcat(stitle, " ;P^*(1) = ", num2str(Ps(1)));
title(stitle, "fontsize", 16);
grid on;

subplot(4, 2, 5:6);
for k = 1:n
  plot(ones(2) * k, [0, y(k)], "color", "b"); hold on;
endfor
plot(1:n, y, "color", "r");
xlabel("k");
ylabel("y", "interpreter", "tex");
grid on;

subplot(4, 2, 7:8);
for k = 1:n
  plot(ones(2) * k, [0, hatx(k)], "color", "b"); hold on;
endfor
plot(1:n, hatx, "color", "r");
xlabel("k");
ylabel("\hat{x}", "interpreter", "tex");
grid on;

display(mean(y));
display(mean(hatx));

