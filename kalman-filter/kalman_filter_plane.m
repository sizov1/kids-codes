function kalman_filter_plane()
  Ts = 0.05; % шаг дискретизации 
  nx = 2; % размерность вектора состояния
  ny = 1; % размерность вектора измерений
  trb = 5; % длина временного промежутка 
  
  Ac = [0 1; 0 0];
  Bc = [0; 0];
  Qc = [100.02];
  Rc = [1.01];
  G = [0; 1];
  C = [1 0];
  D = [0];

  sysc = ss(Ac, Bc, C, D);
  sysd = c2d(sysc, Ts, 'zoh');

  A = sysd.a;
  C = sysd.c;
  [Qd, Rd] = VanLoanAlgorithm(G, Qc, Rc, nx, Ts, Ac);
  
  H = chol(Qd, 'lower');
  L = chol(Rd, 'lower');
  
  t = 0:Ts:trb;
  n = length(t);
  
  x = zeros(nx, n);
  y = zeros(ny, n);
  hatx = zeros(nx, n);
  xs = zeros(nx, n);
  K = zeros(nx, n);
  
  % initial condition
  x([1, 2], [1]) = [1; 1];
  Psk = [1e-2 1e-1; 1e-1 1e-2];
  Pwk = Psk;
  hatx([1:nx], [1]) = [1, 2];
  y(1) = L * randn(ny, 1);
  for k = 2:n

    % calculate x(k)
    xk = A * x([1:nx], [k - 1]) + H * randn(nx, 1); 
    x([1:nx], [k]) = xk;
    
    % calculate y(k)
    % y(k) = C * x(k) + L * randn(ny, 1)
    yk = C * xk + L * randn(ny, 1);
    y([1:ny], [k]) = yk;
    
    % calculate P*(k) = A * P~(k - 1) * A' + Q 
    Psk = A * Pwk * A' + Qd; 
    
    % calculate K(k) = P*(k) * C' * (C * P*(k) * C' + Rd)^(-1)
    Kk = Psk * C' * (C * Psk * C' + Rd)^(-1);
    K([1:nx], [k]) = Kk;
    
    % calculate P~(k) = P*(k) - K(k) * C * P*(k)
    Pwk = Psk - Kk * C * Psk;
    
    % calculate x~(k) = A * x^(k - 1) 
    xsk = A * hatx([1, 2], [k - 1]);
    xs([1:nx], [k]) = xsk;
    
    % calculate x^(k) = x~(k) + K(k) * (y(k) - C * x~(k))
    hatxk = xsk + Kk * (yk - C * xsk);
    hatx([1:nx], [k]) = hatxk;
  endfor;
 
  figure();
  
  sigma_y = std(y([1], [1:n]), 1);
  sigma_hatx = std(hatx([1], [1:n]), 1);
  sigma_e = std(hatx([1], [1:n]) - y([1], [1:n]), 1);
  
  sTs = strcat("Ts = ", num2str(Ts));
  sQRs = strcat(" ; Qc = ", num2str(Qc), " ;Rc = ", num2str(Rc));
  sVarY = strcat(" ; \\sigma_y = ", num2str(sigma_y));
  sVarHatX = strcat(" ; \\sigma_{hatx} = ", num2str(sigma_hatx));
  sVarE = strcat(" ; \\sigma_e = ", num2str(sigma_e));
  
  line1 = strcat(sTs, sQRs);
  line2 = strcat(sVarY, sVarHatX, sVarE);
 
  subplot(2, 4, 5:6);
  line_hatx = plot(t, hatx([1], [1:n]), "linewidth", 1);
  xlabel("t", "fontsize", 16);
  ylabel("\hat{y}", "fontsize", 16, "interpreter", "latex");
  grid on;
  
  subplot(2, 4, 7:8);
  line_y = plot(t, y([1], [1:n]), "linewidth", 1);
  xlabel("t", "fontsize", 16);
  ylabel("y", "fontsize", 16);
  grid on;
  
  subplot(2, 4, 1:4);
  line_e = plot(t, hatx([1], [1:n]) - y([1], [1:n]), "linewidth", 1);
  xlabel("t", "fontsize", 16);
  ylabel("\hat{y}- y", "fontsize", 16, "interpreter", "latex");
  grid on;
  title(strcat(line1, line2), "interpreter", "tex", "fontsize", 16);
endfunction

function [Qd, Rd] = VanLoanAlgorithm(G, Qc, Rc, nx, Ts, Ac)
  Qd = G * Qc * G';
  Qd = (Qd + Qd') / 2;

  M = [-Ac Qd; zeros(nx) Ac'];
  phi = expm(M * Ts);
  phi12 = phi(1:nx, nx+1:2*nx);
  phi22 = phi(nx+1:2*nx,nx+1:2*nx);
  Qd = phi22'*phi12;
  Qd = (Qd+Qd')/2;
  Rc = (Rc+Rc')/2;
  Rd = Rc / Ts;
endfunction
