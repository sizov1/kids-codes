import numpy as np
from states_generator import generate_states

def test_generator(c, j, r):
	states = generate_states(c, j, r)
	assert(np.size(states) > 0) 		# power of states set should be greater than 0
	assert(states.shape[1] == r + 2)	# size of state should be equals r + 2
	assert((states[0] == np.zeros(r + 2)).all()) # first state should be only zero state
	for i in range(states.shape[0] - 1):
		assert(states[i + 1][1] >= states[i][1]) # all states should be order by j 
		if states[i][1] == states[i + 1][1]:
			curr_state, next_state = "", ""
			for k in range(2, r + 2):
				curr_state += str(states[i][k])
				next_state += str(states[i + 1][k])
			curr_state += str(states[i][0]) 
			next_state += str(states[i + 1][0])
			assert(curr_state > next_state) # states in a same layer must satisfy the lexicographic order
		assert(np.sum(states[i]) - states[i][1] <= c) # numbers of active servers should not be grater than c
	assert(np.sum(states[-1]) - states[-1][1] <= c) # numbers of active servers should not be grater than

test_generator(5, 9, 4)