import numpy as np

def only_one_operation(a, b):
	d = a - b
	if np.count_nonzero(d) > 2:
		return False
	if not np.all(np.abs(d) <= 1):
		return False
	return True

def is_new_request(state_i, state_j):
	sb = state_j - state_i

	if sb[1] == 1:
		if sb[2] == 1:
			return True
		if np.count_nonzero(sb) == 1:
			return True

def is_end_serve(state_i, state_j, number_of_servers):
	sb = state_j - state_i

	if sb[1] != -1 or state_i[0] == 0:
		return False

	if np.count_nonzero(state_i[2:]) == 0 and sb[0] == -1:
		return True	

	if sb[0] == 0:
		if np.count_nonzero(sb[2:]) == 0 and state_i[1] > number_of_servers:
			return True
		if np.count_nonzero(sb[2:]) == 1:
			ind = np.where(sb[2:] != 0)[0]
			if sb[2:][ind] == -1:
				return True
	return False

def is_new_phase(states_i, states_j):
	sd = states_j - states_i
	if sd[1] == 0:
		inzeosd = np.nonzero(sd)[0]
		if abs(inzeosd[1] - inzeosd[0]) > 1 and inzeosd[1] == len(states_i) - 1:
			return True
		if abs(inzeosd[1] - inzeosd[0]) == 1:
			return True
	return False
