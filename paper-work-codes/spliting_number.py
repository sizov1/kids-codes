def split_number(n, r):
	splits = []
	m = n
	a = [1 for i in range(0, m + 1)]
	a[m] = 1
	m = 1
	a[0], x, q = 0, 0, 0
	isp2 = True
	while 1:
		if isp2:
			a[m] = n
			q = m - (n == 1)
			isp2 = False
		if not m > r + 1:
			splits.append(a[1:(m+1)])
		if a[q] != 2:
			if q == 0:
				return splits
			else:
				x = a[q] - 1
				a[q] = x
				n = m - q + 1
				m = q + 1
				while n > x:
					a[m] = x
					m += 1
					n = n - x
				isp2 = True
		else:
			a[q] = 1
			q -= 1
			m += 1
