import csv

def state_name(state):
	row = ""
	for i in state:
		row += str(i)
	return row

def str_row_from_array(arr):
	str_row = ""
	for value in arr:
		str_row += str(value) + ";"
	return str_row

def header(states):
	n_states = len(states)
	header_row = ";"
	for i in range(n_states):
		header_row += state_name(states[i])
		header_row += ";"
	return header_row

def write_Q_in_file(Q, states):
	with open('mt.csv', 'w', newline='') as csvfile:
		table = csv.writer(csvfile, delimiter=';', quotechar=' ', quoting=csv.QUOTE_MINIMAL)
		header_row = header(states)
		table.writerow([header_row])
		for i, row in enumerate(Q):
			str_row = state_name(states[i]) + ";"
			str_row += str_row_from_array(row)
			table.writerow([str_row[:-1]])

def write_matrix_in_file(mt, name):
	with open(name, 'w', newline='') as csvfile:
		table = csv.writer(csvfile, delimiter=';', quotechar=' ', quoting=csv.QUOTE_MINIMAL)
		for row in mt:
			str_row = str_row_from_array(row)
			table.writerow([str_row[:-1]])
