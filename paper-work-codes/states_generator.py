import numpy as np
from base_objects import sys_parameters
from spliting_number import split_number
from itertools import *

def generate_states(sp):
	c, s, r = sp.c, sp.s, sp.r
	layers = []
	n = min(c, s)
	for j in range(1, n + 1):
		layer = []
		splits = split_number(j, r)
		for split in splits:
			if len(split) < r + 1:
				addzero = [0 for i in range (r + 1 - len(split))]
				split += addzero
			for state in set([i for i in permutations(split)]):
				layer.append(list(state))
		layers.append(layer)

	states = []
	layers_size = []
	states.append([0 for i in range(r + 2)])
	for i, layer in enumerate(layers):
		layers_size.append(len(layer))
		layers[i] = sorted(layer)[::-1]
		for state in layers[i]:
			states.append([state[-1], sum(state)] + state[:-1])
	for j in range(n + 1, s + 1):
		layers_size.append(len(layers[-1]))
		for state in layers[-1]:
			states.append([state[-1], j] + state[:-1])
	return np.array(states), layers_size