import numpy as np

def get_border_k_layer(k, layers_size):
	if k == 0:
		return 0, 1
	a = sum(layers_size[:k]) + 1
	b = sum(layers_size[:(k + 1)]) + 1
	return a, b

def Nblock(Q, k, layers_size):
	if k == 0:
		return Q[0:1,0:1]
	a, b = get_border_k_layer(k, layers_size)
	return Q[a:b, a:b]


def Mblock(Q, k, layers_size):
	if k > 0:
		a, b = get_border_k_layer(k, layers_size)
		c, d = get_border_k_layer(k - 1, layers_size)
		return Q[a:b, c:d]

def Lblock(Q, k, layers_size):
	if k == 0:
		return Q[0:1,1:layers_size[1]+1]
	a, b = get_border_k_layer(k, layers_size)
	c, d = get_border_k_layer(k + 1, layers_size)
	return Q[a:b, c:d]

def homogeneous_blocks(Q, c, layers_size):
	a1, b1 = get_border_k_layer(c, layers_size)
	a2, b2 = get_border_k_layer(c + 1, layers_size)
	a3, b3 = get_border_k_layer(c + 2, layers_size)
	return Q[a2:b2, a3:b3], Q[a2:b2, a2:b2], Q[a2:b2, a1:b1]