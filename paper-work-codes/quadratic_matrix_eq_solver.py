import numpy as np

def quadratic_matrix_eq(L, N, M, niter):
	w = np.min(np.abs(np.diagonal(N)))
	R = np.zeros(N.shape)
	E = np.eye(N.shape[0])
	for k in range(niter):
		R = (L + R.dot(N + w * E) + (R.dot(R)).dot(M)) / w
	return R

def sum_matrix_geom_progression(A, n=0):
	res = np.zeros(A.shape)
	for i in range(n):
		res += np.linalg.matrix_power(A, i + 1);
	return res