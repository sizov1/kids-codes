from base_objects import *
from intense_matrix_generator import intense_matrix
from finite_system_solver import solve_finite_system
from states_generator import generate_states
from intense_matrix_blocks import homogeneous_blocks
from quadratic_matrix_eq_solver import *  
from matrix_output import write_Q_in_file
from matrix_output import write_matrix_in_file
import scipy.linalg
sp = sys_parameters(9, 10, 7)
dp = distr_parameters(0.1, 0.5, 0.3)

print("generate states...", end=' ')
states, layers_size = generate_states(sp)
print("DONE")
Z = 0
for i in layers_size:
	Z += i * i
print(Z/(states.shape[0]**2))
print("generate matrix Q...", end = ' ')
Q = intense_matrix(sp, dp, states, layers_size)
print("DONE")
L, N, M = homogeneous_blocks(Q, sp.c, layers_size)
R = quadratic_matrix_eq(L, N, M)
layers_size = [0] + layers_size
print("searching stationary distribution...", end=' ')
p = solve_finite_system(sp.c, Q, R, layers_size)
print("DONE")
S = 0 
for a in p:
	S += np.sum(a)
print(S)