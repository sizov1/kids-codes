import numpy as np 
from intense_matrix_blocks import *
import scipy.linalg
from scipy.sparse import csc_matrix
from scipy.sparse.linalg import lgmres

def sum_matrix_geom_progression(A, n=0):
	res = np.zeros(A.shape)
	for i in range(n):
		res += np.linalg.matrix_power(A, i + 1);
	return res

def Ri1(c, Q, R, layers_size):
	Ri = []
	currR = R
	for i in range(1, c + 1)[::-1]:
		currN = Nblock(Q, i, layers_size)
		currM = Mblock(Q, i + 1, layers_size)
		currL = Lblock(Q, i - 1, layers_size)
		currR = -currL.dot(np.linalg.inv(currN + currR.dot(currM)))
		Ri.append(currR)
	return Ri

def Ri2(c, Q, R, layers_size):
	Ri = []
	for i in range(1, c + 1)[::-1]:
		Ti = Nblock(Q, i, layers_size) + R.dot(Mblock(Q, i + 1, layers_size))
		TiT = np.transpose(Ti)
		nj = layers_size[i - 1] if layers_size[i - 1] != 0 else 1
		ni = layers_size[i]
		T = np.zeros((nj*ni, nj*ni))
		for j in range(nj):
			a = j * ni
			b = (j + 1) * ni
			T[a:b, a:b] = TiT
		Lcol = -Lblock(Q, i - 1, layers_size).reshape((nj*ni, 1))
		G = csc_matrix(T, dtype=float)
		R, exitCode = lgmres(G, Lcol, tol=1e-08, maxiter=1000)
		R = np.expand_dims(R, axis = 0).reshape((nj, ni))
		Ri.append(R)	
	return Ri

def solve_finite_system(c, Q, R, layers_size):
	Ri = Ri2(c, Q, R, layers_size)
	p0 = 0
	F = [np.array([[1]])]
	for mat in Ri[::-1]:
		p0 += np.sum(F[-1])
		F.append(F[-1].dot(mat))
	p0 += np.sum(F[-1])
	p0 += np.sum(F[-1].dot(sum_matrix_geom_progression(R, 10)))
	p0 = 1 / p0
	p = [np.array([[p0]])]
	sizesp = [1]
	for mat in Ri[::-1]:
		p.append(p[-1].dot(mat))
		sizesp.append(p[-1].shape[1])
	for j in range(500):
		p.append(p[-1].dot(R))
	return p