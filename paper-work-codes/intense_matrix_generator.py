import numpy as np
from intense_cases import *

def intense_matrix(sp, dp, states, layers_size):
	nstates = len(states)
	Q = np.zeros((nstates, nstates))
	for i in range(0, nstates):
		# область изменения итератора j можно существенно уменьшить
		for j in range(0, nstates):  
			if not only_one_operation(states[i], states[j]):
				Q[i][j] = 0.0
			elif i == j:
				Q[i][j] = -dp.lymbda - dp.alpha*sum(states[i][2:]) - dp.mu*states[i][0]
			else:
				if is_new_request(states[i], states[j]):
					Q[i][j] = dp.lymbda
				elif is_end_serve(states[i], states[j], sp.c):
					Q[i][j] = states[i][0] * dp.mu
				elif i < j and is_new_phase(states[i], states[j]):
					sd = states[j] - states[i]
					Q[i][j] = states[i][np.where(sd < 0)[0][0]] * dp.alpha
		print(float(i)/nstates)
	return Q

