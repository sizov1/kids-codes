import random

def mergesort(arr):
	N = len(arr)
	tmp = [None] * N
	step = 1
	while step < N:
		ind = 0
		left = 0
		mid = left + step
		right = left + step*2
		while True:
			mid = mid if mid < N else N
			right = right if right < N else N
			i1, i2 = left, mid
			while (i1 < mid and i2 < right):
				if arr[i1] < arr[i2]:
					tmp[ind] = arr[i1]
					ind, i1 = ind + 1, i1 + 1
				else:
					tmp[ind] = arr[i2]
					ind, i2 = ind + 1, i2 + 1

			while i1 < mid:
				tmp[ind] = arr[i1]
				ind, i1 = ind + 1, i1 + 1
			while i2 < right:
				tmp[ind] = arr[i2]
				ind, i2 = ind + 1, i2 + 1

			left += step*2
			right += step*2
			mid += step*2

			if left >= N:
				break
		for i in range(N):
			arr[i] = tmp[i]
		step *= 2

N = int(input())
arr = [random.randint(1,10) for i in range(N)]

print(arr)
mergesort(arr)
print(arr)
