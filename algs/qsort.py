import random 

def paritition(arr, p, r):
	x = arr[p]
	left, right = p, r
	while True:
		while arr[left] < x:
			left += 1
		while arr[right] > x:
			right -= 1
		if left >= right:
			return right
		arr[left], arr[right] = arr[right], arr[left]
		right -= 1
		left += 1

def quicksort(arr, start, end):
	if start < end:
		p = paritition(arr, start, end)
		quicksort(arr, start, p)
		quicksort(arr, p + 1, end)


N = int(input())
arr = [random.randint(1, 10) for i in range(N)]
print(arr)
quicksort(arr, 0, N - 1)
print(arr)