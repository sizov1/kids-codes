import random 

def selectionsort(arr):
	N = len(arr)
	for i in range(N - 1):
		minind = i
		for j in range(i + 1, N):
			if arr[j] < arr[minind]:
				minind = j
		if minind != i:
			arr[minind], arr[i] = arr[i], arr[minind]

N = int(input())
arr = [random.randint(1,10) for i in range(N)]
print(arr)
selectionsort(arr)
print(arr)