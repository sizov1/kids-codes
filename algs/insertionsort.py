import random 

def insetionsort(arr):
	N = len(arr)
	for i in range(1, N):
		x = arr[i]
		j = i - 1
		while (arr[j] > x and j > 0):
			arr[j + 1] = arr[j]
			j--
		arr[i + 1] = x

N = int(input())
arr = [random.randint(1, 10) for i in range(N)]

print(arr)
insetionsort(arr)
print(arr)