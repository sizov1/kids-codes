import random 
import sys, getopt

def bubblesort(arr):
	N = len(arr)
	for i in range(N):
		for j in range(i + 1, N):
			if arr[i] > arr[j]:
				arr[j], arr[i] = arr[i], arr[j]

def main(argv):
	N = int(argv[0])
	arr = [random.randint(1, 10) for i in range(N)]

	print("arr = ", arr)
	bubblesort(arr)
	print("sorted(arr) = ", arr)

if __name__ == "__main__":
   main(sys.argv[1:])