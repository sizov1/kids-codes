\providecommand \autonum@processReference [2]{}
\babel@toc {russian}{}
\contentsline {section}{\numberline {1}Постановка задачи}{3}%
\contentsline {section}{\numberline {2}Построение модели}{3}%
\contentsline {section}{\numberline {3}Вычисление интенсивностей переходов}{5}%
\contentsline {section}{\numberline {4}Стационарное распределение}{8}%
\contentsline {subsection}{\numberline {4.1}Система уравнений для поиска стационарного распределения}{8}%
\contentsline {subsection}{\numberline {4.2}Поиск матриц $R$}{9}%
\contentsline {subsection}{\numberline {4.3}Поиск неоднородных компонент вектора $\vec {\pi }$}{10}%
\contentsline {section}{\numberline {5}Вычисление характеристики энергопотребления}{11}%
\contentsline {section}{\numberline {6}Проведение численных экспериментов}{11}%
\contentsline {subsection}{\numberline {6.1}Зависимость численных характеристик от $\alpha $}{11}%
\contentsline {subsection}{\numberline {6.2}Зависимость численных характеристик от $c$}{14}%
\contentsline {subsection}{\numberline {6.3}Зависимость численных характеристик от $\rho $}{15}%
\contentsline {subsection}{\numberline {6.4}Итоги экспериментов}{17}%
\contentsline {section}{\numberline {7}Заключение}{18}%
