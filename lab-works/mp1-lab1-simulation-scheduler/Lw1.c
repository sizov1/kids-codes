
#define _CRT_SECURE_NO_WARNINGS 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h> 
#include <time.h>
#define N 10001
#define P 255

void shell_Sort(int  sort_flow[N], int n);
void cocktail_Sort(int sort_flow[N], int n);
void counting_Sort(int sort_flow[N], int n);
int binary_search(int sort_flow[N], int n, int a_bi);
int liner_search(int save_flow[N], int n, int a_li);
unsigned file_count_n(const char* fn);
static int comparisons_cocktail = 0, comparisons_counting = 0, comparisons_shell = 0;
static int permutation_cocktail = 0, permutation_counting = 0, permutation_shell = 0;

int main() {
	setlocale(LC_ALL, "Russian");
	FILE *myfile;
	FILE *myfileCPU;
	FILE *output;
	int n, i, j, k, B = 1, enter_flow, sort, enter_cp, available = 0, n_cpu, j_sort, order = 1;
	int search = 0;
	int save_flow[N], CPU[N], sort_flow[N], inc[N], order_flow[N];
	char fn[] = "inCPU.txt";
	printf("Введите кол-во потоков \n");
	scanf("%i", &n);
	printf("Выбирите способ задания времени потоков \n");
	printf("1 - Ручной ввод \n");
	printf("2 - Генерация случайным образом \n");
	printf("3 - Чтение из файла \n");
	while (B != 0) {
		scanf("%i", &enter_flow);
		switch (enter_flow) {
		case 1:
			B = 0;
			printf("Задайте время потоков \n");
			for (i = 1; i <= n; i++) {
				scanf("%i", &save_flow[i]);
			}
			break;
		case 2:
			B = 0;
			for (i = 1; i <= n; i++) {
				save_flow[i] = 1 + rand() % 255;
			}
			break;
		case 3:
			B = 0;
			myfile = fopen("in.txt", "r");
			for (i = 1; i <= n; i++) {
				fscanf(myfile, "%i", &save_flow[i]);
			}
			fclose(myfile);
			break;
		default:
			printf("Неккоректный ввод, повторите еще раз \n");
			B = 1;
		}
	}
	for (i = 1; i <= n; i++) {
		sort_flow[i] = save_flow[i];
	}			//дублируем массив с целью сохранения исходного
	for (i = 0; i < n - 1; i++) {
		inc[i] = 0;
	}
L1:	
	printf("Выбирите метод сортировки потоков \n");
	printf("1 - Шейкерная сортировка \n");
	printf("2 - Сортировка подсчетом \n");
	printf("3 - Сортировка Шелла \n");
	B = 1;
	while (B != 0) {
		scanf("%i", &sort);
		switch (sort) {
		case 1:
			B = 0;
			algorithm_running_time = clock();
			cocktail_Sort(sort_flow, n);
			algorithm_running_time = clock() - algorithm_running_time;
			printf("Время выполнения сортировки: %.0f мс \n",(double)algorithm_running_time);
			printf("Колличество сравнений %10i\n Колличетсво перестановок %10i\n", comparisons_cocktail, permutation_cocktail);
			break;
		case 2:
			B = 0;
			algorithm_running_time = clock();
			counting_Sort(sort_flow, n);
			algorithm_running_time = clock() - algorithm_running_time;
			printf("Время выполнения сортировки: %.0f мс \n",(double)algorithm_running_time);
			printf("Колличество сравнений %10i\n Колличетсво перестановок %10i\n", comparisons_counting, permutation_counting);
			break;
		case 3:
			B = 0;
			algorithm_running_time = clock();
			shell_Sort(sort_flow, n);
			algorithm_running_time = clock() - algorithm_running_time;
			printf("Время выполнения сортировки: %.0f мс \n",(double)algorithm_running_time);
			printf("Колличество сравнений %10i\n Колличетсво перестановок %10i\n", comparisons_shell, permutation_shell);
			break;
		default:
			printf("Неккоректный ввод, повторите еще раз");
			B = 1;
		}
	}
	printf("Выбирите способ задания времени ЦП \n");
	printf("1 - Ручной ввод \n");
	printf("2 - Генерация случайным образом \n");
	printf("3 - Чтение из файла \n");
	B = 1;
	while (B != 0) {
		scanf("%i", &enter_cp);
		switch (enter_cp) {
		case 1:
			B = 0;
			printf("Введите кол-во квантов процессора\n");
			scanf("%i", &n_cpu);
			printf("Задайте кванты времени ЦП \n");
			for (i = 1; i <= n_cpu; i++) {
				scanf("%i", &CPU[i]);
			}
			break;
		case 2:
			B = 0;
			n_cpu = n;
			for (i = 1; i <= n_cpu; i++) {
				CPU[i] = 0 + rand() % 1000;
			}
			break;
		case 3:
			myfileCPU = fopen("inCPU.txt", "r");
			B = 0;
			n_cpu = file_count_n(fn);
			for (i = 1; i <= n_cpu; i++) {
				fscanf(myfileCPU, "%i", &CPU[i]);
			}
			fclose(myfileCPU);
			break;
		default:
			printf("Неккоректный ввод, повторите еще раз");
			B = 1;
		}
	}
	if (n < 21) {
		printf("_________________________________________________________________________________________________________________________________\n");
		printf("|Номер итерации|Квант времени ЦП|Суммарное доступное время ЦП|Номер выбранного потока|Время выбранного потока|Оставшееся время ЦП|\n");
		for (k = 1; k <= n_cpu; k++) {
			available = CPU[k] + available;
			search = binary_search(sort_flow, n, available);
			j = liner_search(save_flow, n, search);
			order_flow[order] = j;
			order++;
			save_flow[j] = 0;
			if (search == 0) {
				printf("|%7i       |%9i       |%14i              |           -           |           -           |%10i         |\n", k, CPU[k], available, (available - search));
			} else {
				printf("|%7i       |%9i       |%14i              |%12i           |%12i           |%10i         |\n", k, CPU[k], available, j, search, (available - search));
			}
			j_sort = liner_search(sort_flow, n, search);
			available = (available - search);
			sort_flow[j_sort] = 0;
			sort_flow[0] = sort_flow[j_sort];
			for (i = j_sort; i >= 1; i--) {
				sort_flow[i] = sort_flow[i - 1];
			}
			search = binary_search(sort_flow, n, available);
			while ((available > 0) && (search != 0)) {
				search = binary_search(sort_flow, n, available);
				j = liner_search(save_flow, n, search);
				order_flow[order] = j;
				order++;
				if (search == 0) {
					break;
				}
				save_flow[j] = 0;
				printf("|              |                |%14i              |%12i           |%12i           |%10i         |\n", available, j, search, (available - search));
				j_sort = liner_search(sort_flow, n, search);
				available = (available - search);
				j_sort = liner_search(sort_flow, n, search);
				sort_flow[j_sort] = 0;
				sort_flow[0] = sort_flow[j_sort];
				for (i = j_sort; i >= 1; i--) {
					sort_flow[i] = sort_flow[i - 1];
				}
			}
		}
		printf("Порядок выполнения потоков:\n");
		for (i = 1; i <= n; i++) {
			printf("%i, ", order_flow[i]);
		}
	} else if (n >= 21) {
		output = fopen("out.txt", "w");
		fprintf(output, "_________________________________________________________________________________________________________________________________\n");
		fprintf(output, "|Номер итерации|Квант времени ЦП|Суммарное доступное время ЦП|Номер выбранного потока|Время выбранного потока|Оставшееся время ЦП|\n");
		for (k = 1; k <= n_cpu; k++) {
			available = CPU[k] + available;
			search = binary_search(sort_flow, n, available);
			j = liner_search(save_flow, n, search);
			order_flow[order] = j;
			order++;
			save_flow[j] = 0;
			if (search == 0) {
				fprintf(output, "|%7i       |%9i       |%14i              |           -           |           -           |%10i         |\n", k, CPU[k], available, (available - search));
			} else {
				fprintf(output, "|%7i       |%9i       |%14i              |%12i           |%12i           |%10i         |\n", k, CPU[k], available, j, search, (available - search));
			}
			j_sort = liner_search(sort_flow, n, search);
			available = (available - search);
			sort_flow[j_sort] = 0;
			sort_flow[0] = sort_flow[j_sort];
			for (i = j_sort; i >= 1; i--) {
				sort_flow[i] = sort_flow[i - 1];
			}
			search = binary_search(sort_flow, n, available);
			while ((available > 0) && (search != 0)) {
				search = binary_search(sort_flow, n, available);
				j = liner_search(save_flow, n, search);
				order_flow[order] = j;
				order++;
				if (search == 0) {
					break;
				}
				save_flow[j] = 0;
				fprintf(output, "|              |                |%14i              |%12i           |%12i           |%10i         |\n", available, j, search, (available - search));
				j_sort = liner_search(sort_flow, n, search);
				available = (available - search);
				j_sort = liner_search(sort_flow, n, search);
				sort_flow[j_sort] = 0;
				sort_flow[0] = sort_flow[j_sort];
				for (i = j_sort; i >= 1; i--) {
					sort_flow[i] = sort_flow[i - 1];
				}
			}
		}
		fclose(output);
	}

	return 0;
}

void cocktail_Sort(int sort_flow[N], int n) {
	int i, swap;
	int left = 1, right = n;
	while (left < right) {
		for (i = left; i < right; i++) {
			comparisons_cocktail++;
			if (sort_flow[i] > sort_flow[i + 1]) {
				swap = sort_flow[i];
				sort_flow[i] = sort_flow[i + 1];
				sort_flow[i + 1] = swap;
				permutation_cocktail++;
			}
		}
		right--;
		for (i = right; i > left; i--) {
			comparisons_cocktail++;
			if (sort_flow[i - 1] > sort_flow[i]) {
				swap = sort_flow[i - 1];
				sort_flow[i - 1] = sort_flow[i];
				sort_flow[i] = swap;
				permutation_cocktail++;
			}
		}
		left++;
	}
}
void counting_Sort(int s_flow[N], int n) {
	int C[P] = { 0 };
	int i, k, v = 1;
	for (i = 1; i <= n; i++) {
		C[s_flow[i]]++;
	}
	for (i = 1; i <= P; i++) {
		for (k = 0; k < C[i]; k++) {
			s_flow[v++] = i;
			permutation_counting++;
		}
	}
}
void shell_Sort(int sort_flow[N], int n) {
	int inc[P] = { 0 }, s = -1, i, j;
	int p1, p2, p3;
	int temp;
	p1 = p2 = p3 = 1;
	do {
		if (++s % 2) {
			inc[s] = 8 * p1 - 6 * p3 + 1;
		} else {
			inc[s] = 9 * p1 - 9 * p3 + 1;
			p2 *= 2;
			p3 *= 2;
		}
		p1 *= 2;
	} while (3 * inc[s] < n);
	while (s >= 0) {
		for (i = inc[s]; i <= n; i++) {
			temp = sort_flow[i];
			comparisons_shell++;
			for (j = i - inc[s]; (j >= 0) && (sort_flow[j] > temp); j -= inc[s]) {
				sort_flow[j + inc[s]] = sort_flow[j];
				permutation_shell++;
			}
			sort_flow[j + inc[s]] = temp;
		}
		s--;
	}
}
int binary_search(int sort_flow[N], int n, int a_bi) {
	int left = 1, right, mid;
	int search;
	right = n;
	mid = left + ((right - left) / 2);
	while (left <= right) {
		mid = left + ((right - left) / 2);
		if (a_bi < sort_flow[1]) {
			search = 0;
			break;
		}
		if (a_bi > sort_flow[mid]) {
			left = mid;
			if (a_bi < sort_flow[mid + 1]) {
				search = sort_flow[mid];
				break;
			} else if (a_bi >= sort_flow[n]) {
				search = sort_flow[n];
				break;
			}
		} else if (a_bi < sort_flow[mid]) {
			right = mid;
			if (a_bi > sort_flow[mid - 1]) {
				search = sort_flow[mid - 1];
				break;
			}
		} else {
			search = sort_flow[mid];
			break;
		}
	}
	return search;
}
int liner_search(int save_flow[N], int n, int a_li) {
	int j, search_1 = 0;
	for (j = 1; j <= n; j++) {
		if (save_flow[j] == a_li) {
			search_1 = j;
			j = n + 1;
		}
	}
	return search_1;
}
unsigned file_count_n(const char* fn) {
	int  i, j;
	char b[256], *p;
	unsigned n = 0;
	FILE*   fp = fopen(fn, "rt");
	if (fp == NULL)
		return 0;

	i = 0;
	while (fgets(b, sizeof(b) - 1, fp) != NULL) {
		if (ferror(fp) != 0) {
			break;
		}
		for (p = &b[0]; *p; ++p) {
			j = (*p >= '0' && *p <= '9');
			if (j) {
				++i;
			} else if (!j) {
				if (i > 0) {
					++n;
				}
				i = 0;
			}
		}
	}
	fclose(fp);
	return n + (unsigned)(i != 0);
}
