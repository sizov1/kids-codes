#include <iostream>
#include <fstream>
#include <cstdlib>
#include <random>
#include "Vector.h"
#include "Matrix.h"
#include "Solver.h"
using namespace std;

int main() {
	unsigned n, enterInput, enterLead, enterRepeat;

	cout << "Solution of the system of algebraic equations" << endl;
	cout << "............................................." << endl;
	cout << "Enter the number of equations: ";
	cin >> n;
	cout << "Choose a method for selecting the leading element" << endl;
	cout << "1 - The leading element is the first nonzero" << endl;
	cout << "2 - The leading element is the maximum in the column" << endl;
	cout << "3 - The leading element is the maximum in the entire matrix" << endl;
	cin >> enterLead;
	cout << "Select the input method for the matrix" << endl;
	cout << "1 - command line input" << endl;
	cout << "2 - reading from a file" << endl;
	cout << "3 - random generation" << endl << "Method: " << endl;
	cin >> enterInput;

	Solver res(n);

	if (enterLead == 1) {
		Solver1 A(n), C(n);
		A.Input(enterInput);
		for (unsigned i = 0; i < n; i++) { res[i] = A[i]; }
		C = A;
		A.Solve();
	} else if (enterLead == 2) {
		Solver2 A(n), C(n);
		A.Input(enterInput);
		for (unsigned i = 0; i < n; i++) { res[i] = A[i]; }
		C = A;
		A.Solve();
	} else if (enterLead == 3) {
		Solver3 A(n), C(n);
		A.Input(enterInput);
		for (unsigned i = 0; i < n; i++) { res[i] = A[i]; }
		C = A;
		A.Solve();
	}

	cout << "Do you want to repeat a different solution method?" << endl;
	cout << "1 - Yes" << endl;
	cout << "2 - No" << endl;
	cin >> enterRepeat;

	while (enterRepeat == 1) {
		cout << "Choose a method for selecting the leading element" << endl;
		cout << "1 - The leading element is the first nonzero" << endl;
		cout << "2 - The leading element is the maximum in the column" << endl;
		cout << "3 - The leading element is the maximum in the entire matrix" << endl;
		cin >> enterLead;
		if (enterLead == 1) {
			Solver1 R(n);
			for (unsigned i = 0; i < n; i++) { R[i] = res[i]; }
			R.Solve();
		} else if (enterLead == 2) {
			Solver2 R(n);
			for (unsigned i = 0; i < n; i++) { R[i] = res[i]; }
			R.Solve();
		} else if (enterLead == 3) {
			Solver3 R(n);
			for (unsigned i = 0; i < n; i++) { R[i] = res[i]; }
			R.Solve();
		}
		cout << "Do you want to repeat a different solution method?" << endl;
		cout << "1 - Yes" << endl;
		cout << "2 - No" << endl;
		cin >> enterRepeat;
	}

	
	system("pause");
	return 0;
}
