#include "Matrix.h"
#include "Vector.h"
#include <iostream>

using namespace std;

Matrix::Matrix(unsigned s) {
	n = s;
	Vector temp(n + 1);
	pMatrix = new Vector[n];
	for (unsigned i = 0; i < n; i++) {
		pMatrix[i] = temp;
	}
}

Matrix::Matrix(const Matrix& m) {
	n = m.GetSize();
	pMatrix = new Vector[n];
	for (unsigned i = 0; i < n; i++) {
		pMatrix[i] = m[i];
	}
}

Matrix::~Matrix() {
	delete[] pMatrix;
}

Vector& Matrix::operator [](const unsigned i) const {
	return pMatrix[i];
}

istream& operator >>(istream& is, Matrix& m) {
	unsigned n = m.GetSize();
	for (unsigned i = 0; i < n; i++) {
		is >> m[i];
	}
	return is;
}

ostream& operator <<(ostream& os, const Matrix& m) {
	unsigned n = m.GetSize();
	for (unsigned i = 0; i < n; i++) {
		os << m[i] << endl;
	}
	return os;
}

const Matrix& Matrix::operator=(const Matrix& m) {
	if (this != &m) {
		delete[] pMatrix;
		n = m.GetSize();
		pMatrix = new Vector[n];
		for (unsigned i = 0; i < n; i++) {
			pMatrix[i] = m[i];
		}
	}
	return *this;
}

void Matrix::RearrangeRow(unsigned i1, unsigned i2) {
	Vector temp;
	temp = pMatrix[i1];
	pMatrix[i1] = pMatrix[i2];
	pMatrix[i2] = temp;
}

void Matrix::RearrangeCow(unsigned i1, unsigned i2) {
	Vector i2Cow(n);
	for (unsigned i = 0; i < n; i++) {
		i2Cow[i] = (*this)[i][i2];
	}
	for (unsigned i = 0; i < n; i++) {
		(*this)[i][i2] = (*this)[i][i1];
	}
	for (unsigned i = 0; i < n; i++) {
		(*this)[i][i1] = i2Cow[i];
	}
}

Vector Matrix::operator *(Vector x) {
	Vector Ax((*this).n);
	for (unsigned i = 0; i < (*this).n; i++) {
		for (unsigned j = 0; j < (*this).n; j++) {
			Ax[i] += (*this)[i][j] * x[j];
		}
	}
	return Ax;
}





