#include <string>
#include <iostream>
#include <iomanip>
#include "Vector.h"
using namespace std;

Vector::Vector(unsigned s) {
	size = s;
	pVector = new double[s];
	for (unsigned i = 0; i < s; i++) {
		pVector[i] = 0;
	}
}

Vector::Vector(const Vector& v) {
	size = v.GetSize();
	pVector = new double[size];
	for (unsigned i = 0; i < size; i++) {
		pVector[i] = v[i];
	}
}

Vector::~Vector() {
	delete[] pVector;
}

ostream& operator <<(ostream& os, const Vector& v) {
	unsigned n = v.GetSize();

	for (unsigned i = 0; i < n; i++) {
		os << setw(20) << v[i];
	}
	return os;
}

istream& operator >>(istream& is, Vector& v) {
	unsigned n = v.GetSize();
	for (unsigned i = 0; i < n; i++) {
		is >> v[i];
	}
	return is;
}

const Vector& Vector::operator =(const Vector& v) {
	if (this != &v) {
		delete[] pVector;
		size = v.GetSize();
		pVector = new double[size];
		for (unsigned i = 0; i < size; i++) {
			pVector[i] = v[i];
		}
	}
	return *this;
}

Vector& Vector::operator +=(const Vector &v) {
	for (unsigned i = 0; i < size; i++) {
		pVector[i] += v[i];
	}
	return *this;
}

Vector& Vector::operator -=(const Vector &v) {
	for (unsigned i = 0; i < size; i++) {
		pVector[i] -= v[i];
	}
	return *this;
}

const Vector Vector::operator +(const Vector& v) const {
	Vector temp(*this);
	temp += v;
	return temp;
}

const Vector Vector::operator -(const Vector& v) const {
	Vector temp(*this);
	temp -= v;
	return temp;
}

Vector Vector::operator *(double d) const {
	Vector temp(size);
	for (unsigned i = 0; i < size; i++) {
		temp[i] = pVector[i] * d;
	}
	return temp;
}

Vector operator *(double a, const Vector& b) { return b * a; }

Vector Vector::operator /(double d) const {
	Vector temp(size);
	for (unsigned i = 0; i < size; i++) {
		temp[i] = pVector[i] / d;
	}
	return temp;
}

Vector Vector :: operator -() {
	Vector temp(size);
	for (unsigned i = 0; i < size; i++) {
		temp[i] = pVector[i] * (-1);
	}
	return temp;
}

double& Vector::operator [](const unsigned i) const {
	return pVector[i];
}

const bool Vector::operator ==(const Vector& v) {
	if ((*this).size != v.size) {
		//exit(1);
		cout << "Error: size mismatch";
	}
	unsigned count = 0;
	for (unsigned i = 0; i < (*this).size; i++) {
		if ((*this)[i] == v[i]) {
			count++;
		}
	}
	if (count == (*this).size) {
		return true;
	} else {
		return false;
	}
}






