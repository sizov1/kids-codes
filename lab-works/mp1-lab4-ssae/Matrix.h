#ifndef __MATRIX_H
#define __MATRIX_H
#include "Vector.h"

class Matrix {

	protected:
		Vector* pMatrix;
		unsigned GetSize() const { return n; }
	public:
		unsigned n;
		Matrix(unsigned n = 3);
		Matrix(const Matrix& m);
		~Matrix();

		friend ostream& operator <<(ostream& os, const Matrix& m);
		friend istream& operator >>(istream& is, Matrix& m);

		Vector operator *(Vector x);
		Vector& operator [](const unsigned i) const;
		const Matrix& operator =(const Matrix& m);
		void RearrangeRow(unsigned i1, unsigned i2);
		void RearrangeCow(unsigned i1, unsigned i2);

		virtual void GetBase(unsigned& cow) { std::cout << "q"; };
};

#endif
