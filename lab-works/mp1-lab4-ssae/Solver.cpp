#include <iostream>
#include <random>
#include <fstream>
#include <cmath>
#include "Solver.h"
#include "Matrix.h"
#include "Vector.h"

static unsigned rowBase = 0, cowBase = 0;
unsigned freezeMax[1000] = { 0 };

void swap(unsigned *mass, unsigned i1, unsigned i2) {
	unsigned temp;
	temp = mass[i1];
	mass[i1] = mass[i2];
	mass[i2] = temp;
}

void Solver::Input(unsigned enterInput) {
	ifstream fin("matrix.txt");
	random_device rd;
	mt19937 gen(rd());
	uniform_real_distribution<> dis(-20000, 20000);
	switch (enterInput) {
	case 1:
		cout << "Line by line enter the elements of the matrix" << endl;
		cin >> (*this);
		break;
	case 2:
		fin >> (*this);
		break;
	case 3:
		for (unsigned j = 0; j <= n; j++) {
			for (unsigned i = 0; i < n; i++) {
				(*this)[i][j] = dis(gen);
			}
		}
		break;
	default:
		cout << "Invalid input";
		break;
	}
}

double Solver::Solve() {
	ofstream fout;
	fout.open("Solve.log");
	fout << " Matrix: " << endl << (*this) << endl;

	Solver res((*this).n);
	for (unsigned i = 0; i < (*this).n; i++) { res[i] = (*this)[i]; }
	Vector x((*this).n);
	const Vector nullVector((*this).n + 1);
	const Vector incompatible((*this).n + 1); incompatible[(*this).n] = 1;
	double e;
	unsigned lead = 0, k = (*this).n - 1, count = 0, back[1000] = { 0 };

	for (unsigned i = 0; i < 1000; i++) {
		back[i] = i;
	}

	/*������ ��� ������*/
	while (count != (*this).n) {    //������� ������������ ��������
		k = (*this).n - 1;
		(*this).GetBase(count);                 //����� ������� �������
		(*this).RearrangeCow(cowBase, count);   //��������� ������� � ������� ��������� �� ������ �������
		swap(back, count, cowBase);				//��������� ������������ �������� ��� ��������� ����
		fout << (*this) << endl;        	    //���������� �������� �� �����
		(*this).RearrangeRow(rowBase, count);   //��������� ������ � ������� ��������� �� ������ ������
		fout << (*this) << endl;			    //���������� �������� �� �����
		(*this)[count] = (*this)[count] / (*this)[count][count];    //������� ������������ ������� ������ 1
		fout << (*this) << endl;	  //���������� �������� �� �����
		while (k > count) {                     //�������� ��� �������� � �������, ������� ���� ��������
			(*this)[k] -= ((*this)[count] * (*this)[k][count]);    //�������� ������ �������
			fout << (*this) << endl;		    //���������� �������� �� �����
			if ((*this)[k] == nullVector) {                //�������� �� ����������� ����� �������
				cout << "The system has infinitely many solutions" << endl;
			}
			else if ((*this)[k] == incompatible) {	   //�������� �� �������������� �������
				cout << "The system of equations is incompatible" << endl;
			}
			k--;
		}
		count++;
	}

	/*�������� ��� ������*/
	for (unsigned i = ((*this).n - 1); i > 0; i--) {   //�������� ��� �������� � �������, ���� �������������
		k = i;
		while (k != 0) {
			(*this)[k - 1] -= (*this)[i] * (*this)[k - 1][i];   //�������� ������ �������
		    fout << (*this) << endl;	  //���������� �������� �� �����
			k--;
		}
	}



	for (unsigned i = 0; i < (*this).n; i++) {
		x[back[i]] = (*this)[i][(*this).n];
	}
	e = Check(res, x);
	return e;
	fout << "|Ax - b| = " << e << endl;
	fout << "Answer: x = ( " << x << " )" ;
	fout.close();
}

double Solver::Check(Solver res, Vector x) {
	double max = 0;
	Vector E((*this).n);
	Vector b((*this).n);
	for (unsigned i = 0; i < (*this).n; i++) {
		b[i] = res[i][(*this).n];
	}
	E = res*x - b;
	for (unsigned i = 0; i < (*this).n; i++) {
		if (E[i] > max) {
			max = E[i];
		}
	}
	return max;
}


void Solver1::GetBase(unsigned& cow) {
	for (unsigned row = cow; row < (*this).n; row++) {
		if ((*this)[row][cow] != 0) {
			rowBase = row;
			cowBase = cow;
			row = (*this).n;
		}
	}
}

void Solver2::GetBase(unsigned& cow) {
	double MAX = 0;
	unsigned maxRow = 0;
	for (unsigned p = cow; p < (*this).n; p++) {
		if (abs((*this)[p][cow]) > abs(MAX)) {
			MAX = (*this)[p][cow];
			maxRow = p;
		}
	}
	rowBase = maxRow;
	cowBase = cow;
}

void Solver3::GetBase(unsigned& cow) {
	double MAX = -2000000;
	unsigned maxCow = cow, maxRow = cow;
	for (unsigned i = cow; i < (*this).n; i++) {        //���� ������������ ������� � �������
		for (unsigned j = cow; j < (*this).n; j++) {
			if (abs((*this)[j][i]) > abs(MAX)) {
				MAX = (*this)[j][i];
				maxRow = j;
				maxCow = i;
			}
		}
	}
	rowBase = maxRow;
	cowBase = maxCow;
}
