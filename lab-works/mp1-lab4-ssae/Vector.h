#ifndef __VECTOR_H
#define __VECTOR_H
#include <iostream>
using namespace std;

class Vector {
	private:
		double* pVector;
		unsigned size;
	public:
		Vector(unsigned s = 2);
		Vector(const Vector& v);
		~Vector();

		friend ostream& operator <<(ostream& os, const Vector& v);
		friend istream& operator >>(istream& is, Vector& v);

		unsigned GetSize() const { return size; }
		double& operator [](const unsigned i) const;
		int GetSum();
		const Vector& operator =(const Vector& v);
		Vector& operator +=(const Vector& v);
		Vector& operator -=(const Vector& v);
		const Vector operator +(const Vector& v) const;
		const Vector operator -(const Vector& v) const;
		Vector operator -();
		Vector operator *(double d) const;
		Vector operator /(double d) const;

		const bool operator ==(const Vector& v);


};

#endif
