#include <iostream>
#include <fstream>
#include <cstdlib>
#include <random>
#include "Vector.h"
#include "Matrix.h"
#include "Solver.h"
using namespace std;

int main() {
	ofstream fout;
	fout.open("ForGraph.csv");

	unsigned enterInput, enterLead;
	unsigned begin, step, end;
	double e1, e2, e3;

	cout << "Solution of the system of algebraic equations" << endl;
	cout << "............................................." << endl;
	cout << "Begin: " << endl;
	cin >> begin;
	cout << "Step: " << endl;
	cin >> step;
	cout << "End: " << endl;
	cin >> end;
	cout << "Choose a method for selecting the leading element" << endl;
	cout << "1 - The leading element is the first nonzero" << endl;
	cout << "2 - The leading element is the maximum in the column" << endl;
	cout << "3 - The leading element is the maximum in the entire matrix" << endl;
	cin >> enterLead;
	cout << "Select the input method for the matrix" << endl;
	cout << "1 - command line input" << endl;
	cout << "2 - reading from a file" << endl;
	cout << "3 - random generation" << endl << "Method: " << endl;
	cin >> enterInput;

	/*for (unsigned j = 0; (begin + (j * step)) <= end; j++) {
		Solver res(begin + (j*step));
		Solver1 A(begin + (j*step));
		Solver2 B(begin + (j*step));
		Solver3 C(begin + (j*step));
		res.Input(enterInput);
		for (unsigned i = 0; i < begin + (j*step); i++) { A[i] = res[i]; }
		for (unsigned i = 0; i < begin + (j*step); i++) { B[i] = res[i]; }
		for (unsigned i = 0; i < begin + (j*step); i++) { C[i] = res[i]; }
		e1 = A.Solve();
		e2 = B.Solve();
		e3 = C.Solve();
		fout << e1 << ";" << e2 << ";" << e3 << ";" << (begin + (j*step)) << endl;
	}*/


	if (enterLead == 1) {
		for (unsigned j = 0; (begin + (j * step)) <= end; j++) {
			Solver1 A(begin + (j*step));
			A.Input(enterInput);
			e1 = A.Solve();
			fout << e1 << ";" << (begin + (j*step)) << endl;
		}
	} else if (enterLead == 2){
		for (unsigned j = 0; (begin + (j * step)) <= end; j++) {
			Solver2 A(begin + (j*step));
			A.Input(enterInput);
			e1 = A.Solve();
			fout << e1 << ";" << (begin + (j*step)) << endl;
		}
	} else if (enterLead == 3){
		for (unsigned j = 0; (begin + (j * step)) <= end; j++) {
			Solver3 A(begin + (j*step));
			A.Input(enterInput);
			e1 = A.Solve();
			fout << e1 << ";" << (begin + (j*step)) << endl;
		}
	}
		
	system("pause");
	fout.close();
	return 0;
}
