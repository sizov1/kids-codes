#ifndef __SOLVER_H
#define __SOLVER_H
#include <iostream>
#include "Matrix.h"
#include "Vector.h"

class Solver : public Matrix {

	protected:
		Matrix m;
	public:
		Solver(unsigned s = 0) : Matrix(s) {};
		//Solver(const Solver& c);
		double Check(Solver res, Vector x);
		double Solve();
		void Input(unsigned enterInput);
	
};

class Solver1 : public Solver {
	public:
		Solver1(unsigned s) : Solver(s) {};
		void GetBase(unsigned& cow);
};

class Solver2 : public Solver {
	public:
		Solver2(unsigned s) : Solver(s) {};
		void GetBase(unsigned& cow);
};

class Solver3 : public Solver {
	public:
		Solver3(unsigned s) : Solver(s) {};
		void GetBase(unsigned& cow);
};
#endif