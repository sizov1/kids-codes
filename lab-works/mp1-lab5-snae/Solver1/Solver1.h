#ifndef __SOLVER1_H
#define __SOLVER1_H

typedef double(*function)(double x);

class Solver {
	public:
		virtual double* solve(function f, function df, double eps, double a, double b, double x0) = 0;
};

class Solver1 : public Solver {
	public:
		double* solve(function f, function df, double eps, double a, double b, double x0);
};

#endif
