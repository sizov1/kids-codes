#include "stdlib.h"
#include "Solver1.h"
#define N_MAX 10000

extern "C" {
	__declspec(dllexport) Solver* GetFunction() {
		return new Solver1();
	}
}

double* Solver1::solve(function f, function df, double eps, double a, double b, double x0) {
	int n = 0;
	double *res = new double[3];
	double c, e;
	if (f(a)*f(b) > 0) {
		return NULL;
	} else {
		while (((b - a) / 2 > eps)&&(n < N_MAX)) {
			c = (a + b) / 2;
			if ((f(c)*f(a)) > 0) {
				a = c;
			} else {
				b = c;
			}
			n++;
		}
	}
	e = (b-a)/2;
	res[0] = c;
	res[1] = e;
	res[2] = n;
	return res;
}




