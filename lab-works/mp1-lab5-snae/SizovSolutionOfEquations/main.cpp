#define _CRT_SECURE_NO_WARNINGS
#include <Windows.h>
#include <stdio.h>
#include <iostream>
#include <cmath>
#include "Solver.h"

using namespace std;
#define _FUNCTION_NAME "GetFunction"

typedef Solver* (*MYPROC)();

double f(double x) { return x*x-5*x-42; }
double df(double x) { return 2*x-5; }

int main(int argc, char* argv[]) {
	HINSTANCE hinstLib;
	MYPROC ProcAdd;
	BOOL fFreeResult;

	if (argc < 5) {
		cout << "Error!\nUsage: MainApp.exe <dynamic library name> <eps> <distance> <starting point>" << endl;
		return 0;
	}

	double *res = new double[3];
	double eps, a, b, x0;
	sscanf(argv[2], "%lf", &eps);
	sscanf(argv[3], "%lf", &a);
	sscanf(argv[4], "%lf", &b);
	sscanf(argv[5], "%lf", &x0);
	//argv[1] = Solver1
	hinstLib = LoadLibrary(argv[1]);

	if (hinstLib != NULL) {
		ProcAdd = (MYPROC)GetProcAddress(hinstLib, _FUNCTION_NAME);

		if (NULL != ProcAdd) {
			cout.precision(15);
			cout.setf(ios::scientific);
			Solver* solver = ProcAdd();
			res = solver->solve(f, df, eps, a, b, x0);
			cout << "x = " << res[0] << endl;
			cout << "f(x) = " << f(res[0]) << endl;
			cout << "eps = " << res[1] << endl;
			cout << "n = " << res[2] << endl;

			delete solver;
		}

		fFreeResult = FreeLibrary(hinstLib);
	} else {
		//printf("Can not find libary %s\n", argv[1]);
		cout << "Can not find libary %s\n" << argv[1];
	}
	delete[] res;
	return 0;
}