#ifndef __SOLVER_H
#define __SOLVER_H

typedef double(*function)(double x);

class Solver {
	public:
		virtual double* solve(function f, function df, double eps, double a, double b, double x0) = 0;
};

#endif
