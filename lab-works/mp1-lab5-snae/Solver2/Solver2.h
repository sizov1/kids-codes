#ifndef __SOLVER2_H
#define __SOLVER2_H

typedef double(*function)(double x);

class Solver {
	public:
		virtual double* solve(function f, function df, double eps, double a, double b, double x0) = 0;
};

class Solver2 : public Solver {
	public:
		double* solve(function f, function df, double eps, double a, double b, double x0);
};

#endif

