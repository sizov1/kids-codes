#include "stdlib.h"
#include <cmath>
#include "Solver2.h"
#include <iostream>
#define N_MAX 10000
using namespace std;

extern "C" {
	__declspec(dllexport) Solver* GetFunction() {
		return new Solver2();
	}
}


double* Solver2::solve(function f, function df, double eps, double a, double b, double x0) {
	int n = 0;
	double *res = new double[3];
	double e, tmp;
	while (fabs(b - a)>eps) {
		tmp = b;
		b = b - (b - a)*f(b) / (f(b) - f(a));
		a = tmp;
		n++;
		//cout << "b = " << b << " a = " << a << " b - a = " << b - a << endl;
	}
	e = b - a;
	res[0] = b;
	res[1] = e;
	res[2] = n;
	return res;
}




