#include "stdlib.h"
#include "stdio.h"
#include "Solver3.h"
#include <iostream>
#include <cmath>
#define N_MAX 10000

extern "C" {
	__declspec(dllexport) Solver* GetFunction() {
		return new Solver3();
	}
}

double* Solver3::solve(function f, function df, double eps, double a, double b, double x0) {
	int n = 0;
	double *res = new double[3];
	double e, x1;
	if (df(x0) != 0) {
		x1 = x0 - f(x0) / df(x0);
	} else {
		return NULL;
	}
	while ((fabs(x1 - x0) > eps)&&(n < N_MAX)) {
		x0 = x1;
		x1 = x1 - f(x1) / df(x1);
		n++;
	}
	e = x1 - x0;
	res[0] = x1;
	res[1] = e;
	res[2] = n;
	return res;
}

