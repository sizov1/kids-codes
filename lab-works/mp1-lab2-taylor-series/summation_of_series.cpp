#define _CRT_SECURE_NO_WARNINGS 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h> 
#define pi 3.141592

void sin(int N, double x, double eps);
void cos(int N, double x, double eps);
void exp(int N, double x, double eps);
void tanh(int N, double x, double eps);
double factorial(int n);
double bin_k(int n, int k);
double bernouli(int n);

int main(){
	setlocale(LC_ALL, "Russian");
	//вводимые переменные
	int enter_function, N, mod;
	double x, eps;
	
	printf("Выбирите режим запуска программы\n");
	printf("1 - Единичный расчет функции в точке\n");
	printf("2 - Проведение серийных экспериментов\n");
	scanf("%i", &mod);

	printf("Выберете функцию\n");
	printf("1 - Синус sin(x)\n");
	printf("2 - Косинус cos(x)\n");
	printf("3 - Экспонента e^(x)\n");
	printf("4 - Гиперболический тангенс tanh(x)\n");
	scanf("%i", &enter_function);
	
	printf("Задайте точку, в которой необходимо выполнить расчет\n");
	scanf("%lf", &x);

	if (mod == 1) {
		printf("Задайте точность вычеслений\n");
		scanf("%lf", &eps);
		printf("Задайте колличество слагаемых в разложении\n");
		scanf("%i", &N);
		
		if (enter_function == 1)
			sin(N, x, eps);
		else if (enter_function == 2) 
			cos(N, x, eps);
		else if (enter_function == 3) 
			exp(N, x, eps);
		else if (enter_function == 4) 
			tanh(N, x, eps);

	} else if ( mod == 2 ){
		double S_n1, S_n, next, next1, eps1;
		int n;	
		printf("Задайте колличество экспериментво\n");
		scanf("%i", &N);
		if (enter_function == 1) {
			while (x > 2 * pi) {
				x = x - 2 * pi;
			}
			n = 1;
			next = x;
			S_n = next;
			eps1 = x*x*x / 6;
			printf("Колличество слагаемых|Посчитанная сумма|Разница между суммой и эталонной функцией|Достигнутая погрешность|\n");
			printf("%10i           |%12lf     |%23lf                  |%15lf        |\n", n, S_n, fabs(sin(x) - S_n), eps1);
			for (n; n < N; n++) {
				next = next * (-1)*x*x / ((2 * n)*(2 * n + 1));
				S_n = S_n + next;
				next1 = next * (-1)*x*x / ((2 * (n + 1))*(2 * (n + 1) + 1));
				S_n1 = S_n + next1;
				eps1 = S_n1 - S_n;
				printf("Колличество слагаемых|Посчитанная сумма|Разница между суммой и эталонной функцией|Достигнутая погрешность|\n");
				printf("%10i           |%12lf     |%23lf                  |%15lf        |\n", n+1, S_n, fabs(sin(x) - S_n), fabs(eps1));
			}
		} else if (enter_function == 2){
			while (x > 2 * pi) {
				x = x - 2 * pi;
			}
			n = 1;
			S_n = 1;
			next = 1;
			eps1 = pow(x, 2) / 2;
			printf("Колличество слагаемых|Посчитанная сумма|Разница между суммой и эталонной функцией|Достигнутая погрешность|\n");
			printf("%10i           |%12lf     |%23lf                  |%15lf        |\n", n, S_n, fabs(cos(x) - S_n), eps1);
			for (n; n < N; n++) {
				next = next * (-1)*x*x / ((2 * n - 1)*(2 * n));
				S_n = S_n + next;
				next1 = next * (-1)*x*x / ((2 * n + 1)*(2 * (n + 1) + 2));
				S_n1 = S_n + next1;
				eps1 = S_n1 - S_n;
				printf("Колличество слагаемых|Посчитанная сумма|Разница между суммой и эталонной функцией|Достигнутая погрешность|\n");
				printf("%10i           |%12lf     |%23lf                  |%15lf        |\n", n + 1, S_n, fabs(cos(x) - S_n), fabs(eps1));
			}
		} else if (enter_function == 3) {
			n = 1;
			S_n = 1;
			next = 1;
			eps1 = x / 2;
			printf("Колличество слагаемых|Посчитанная сумма|Разница между суммой и эталонной функцией|Достигнутая погрешность|\n");
			printf("%10i           |%12lf     |%23lf                  |%15lf        |\n", n, S_n, fabs(exp(x) - S_n), eps1);
			for (n = 0; n < (N - 1); n++) {
				next = next * x / (n + 1);
				S_n = S_n + next;
				next1 = next * x / (n + 2);
				S_n1 = S_n + next1;
				eps1 = S_n1 - S_n;
				printf("Колличество слагаемых|Посчитанная сумма|Разница между суммой и эталонной функцией|Достигнутая погрешность|\n");
				printf("%10i           |%12lf     |%23lf                  |%15lf        |\n", n + 2, S_n, fabs(exp(x) - S_n), fabs(eps1));
			}
		} else if (enter_function == 4) {
			if (x > pi / 2) {
				while (x > (pi / 2))
					x = x - pi;
			}
			if (x < pi / 2) {
				while (x < (-1)*pi / 2)
					x = x + pi;
			}
			n = 1;
			next = x;
			S_n = next;
			eps1 = x*x*x / 3;
			printf("Колличество слагаемых|Посчитанная сумма|Разница между суммой и эталонной функцией|Достигнутая погрешность|\n");
			printf("%10i           |%12lf     |%23lf                  |%15lf        |\n", n, S_n, fabs(tanh(x) - S_n), eps1);
			for (n; n < N; n++) {
				next = next * (bernouli(2 * n + 2)*(pow(4, n + 1) - 1) * 4 * x*x) / (bernouli(2 * n)*(pow(4, n) - 1)*(2 * n + 1)*(2 * n + 2));
				S_n = S_n + next;
				next1 = next *(bernouli(2 * (n + 1) + 2)*(pow(4, (n + 1) + 1) - 1) * 4 * x*x) / (bernouli(2 * (n + 1))*(pow(4, (n + 1)) - 1)*(2 * (n + 1) + 1)*(2 * (n + 1) + 2));
				S_n1 = S_n + next1;
				eps1 = S_n1 - S_n;
				printf("Колличество слагаемых|Посчитанная сумма|Разница между суммой и эталонной функцией|Достигнутая погрешность|\n");
				printf("%10i           |%12lf     |%23lf                  |%15lf        |\n", n + 1, S_n, fabs(tanh(x) - S_n), fabs(eps1));
			}
		}
	}
	

	return 0;
}

void sin(int N, double x, double eps) {
	double S_n1, S_n, next, next1, eps1;
	int n, stop = 0;
	while (x > 2 * pi) {
		x = x - 2 * pi;
	}
	next = x;
	S_n = next;
	eps1 = x*x*x / 6;
	for (n = 1; n < N; n++) {
		next = next * (-1)*pow(x, 2) / ((2 * n)*(2 * n + 1));
		S_n = S_n + next;
		next1 = next * (-1)*pow(x, 2) / ((2 * (n + 1))*(2 * (n + 1) + 1));
		S_n1 = S_n + next1;
		eps1 = S_n1 - S_n;
		if (fabs(eps1) < eps) {
			n++;
			stop = 1;
			break;
		}
	}
	printf("Эталонное значение функции в точке x: %lf\n", sin(x));
	printf("Посчитанная сумма ряда: %lf\n", S_n);
	printf("Разница между суммой и эталонной функцией: %lf\n", fabs(sin(x) - S_n));
	printf("Достигнутая погрешность: %lf\n", fabs(eps1));
	printf("Колличество слагаемых которые были использованны для вычисления суммы: %i\n", n);
	if (stop == 1) printf("Остановка вычислений произошла по критерию точности\n");
	else printf("Остановка произошла по критерию интераций\n");
}

void cos(int N, double x, double eps) {
	double S_n1, S_n, next, next1, eps1;
	int n, stop = 0;
	while (x > 2 * pi) {
		x = x - 2 * pi;
	}
	n = 1;
	S_n = 1;
	next = 1;
	eps1 = pow(x, 2) / 2;
	for (n; n < N; n++) {
		next = next * (-1)*x*x / ((2 * n - 1)*(2 * n));
		S_n = S_n + next;
		next1 = next * (-1)*x*x / ((2 * n + 1)*(2 * (n + 1) + 2));
		S_n1 = S_n + next1;
		eps1 = S_n1 - S_n;
		if (fabs(eps1) < eps) {
			n++;
			stop = 1;
			break;
		}
	}
	printf("Эталонное значение функции в точке x: %lf\n", cos(x));
	printf("Посчитанная сумма ряда: %lf\n", S_n);
	printf("Разница между суммой и эталонной функцией: %lf\n", fabs(cos(x) - S_n));
	printf("Достигнутая погрешность: %lf\n", fabs(eps1));
	printf("Колличество слагаемых которые были использованны для вычисления суммы: %i\n", n);
	if (stop == 1) printf("Остановка вычислений произошла по критерию точности\n");
	else printf("Остановка произошла по критерию интераций\n");
}

void exp(int N, double x, double eps) {
	double S_n1, S_n, next, next1, eps1;
	int n, stop = 0;
	n = 1;
	S_n = 1;
	next = 1;
	eps1 = x / 2;
	for (n = 0; n < (N - 1); n++) {
		next = next * x / (n + 1);
		S_n = S_n + next;
		next1 = next * x / (n + 2);
		S_n1 = S_n + next1;
		eps1 = S_n1 - S_n;
		if (fabs(eps1) < eps) {
			stop = 1;
			break;
		}
	}
	printf("Эталонное значение функции в точке x: %lf\n", exp(x));
	printf("Посчитанная сумма ряда: %lf\n", S_n);
	printf("Разница между суммой и эталонной функцией: %lf\n", fabs(exp(x) - S_n));
	printf("Достигнутая погрешность: %lf\n", fabs(eps1));
	printf("Колличество слагаемых которые были использованны для вычисления суммы: %i\n", n + 1);
	if (stop == 1) printf("Остановка вычислений произошла по критерию точности\n");
	else printf("Остановка произошла по критерию интераций\n");
}

void tanh(int N, double x, double eps) {
	double S_n1, S_n, next, next1, eps1;
	int n, stop = 0;
	if (x > pi / 2) {
		while (x > (pi / 2))
			x = x -  pi;
	}
	if (x < pi / 2) {
		while (x < (-1)*pi / 2)
			x = x +  pi;
	}
	n = 1;
	next = x;
	S_n = next;
	eps1 = x*x*x / 3;
	for (n; n < N; n++) {
		next = next * (bernouli(2 * n + 2)*(pow(4, n + 1) - 1) * 4 * x*x) / (bernouli(2 * n)*(pow(4, n) - 1)*(2 * n + 1)*(2 * n + 2));
		S_n = S_n + next;
		next1 = next *(bernouli(2 * (n + 1) + 2)*(pow(4, (n + 1) + 1) - 1) * 4 * x*x) / (bernouli(2 * (n + 1))*(pow(4, (n + 1)) - 1)*(2 * (n + 1) + 1)*(2 * (n + 1) + 2));
		S_n1 = S_n + next1;
		eps1 = S_n1 - S_n;
		if (fabs(eps1) < eps) {
			n++;
			stop = 1;
			break;
		}
	}
	printf("Эталонное значение функции в точке x: %lf\n", tanh(x));
	printf("Посчитанная сумма ряда: %lf\n", S_n);
	printf("Разница между суммой и эталонной функцией: %lf\n", fabs(tanh(x) - S_n));
	printf("Достигнутая погрешность: %lf\n", fabs(eps1));
	printf("Колличество слагаемых которые были использованны для вычисления суммы: %i\n", n);
	if (stop == 1) printf("Остановка вычислений произошла по критерию точности\n");
	else printf("Остановка произошла по критерию интераций\n");
}


double factorial(int n) {
	if (n == 0 || n == 1)
		return 1;
	return n * factorial(n - 1);
}
//биномиальные коэфф-ты
double bin_k(int n, int k) {
	return 1.*factorial(n) / factorial(k) / (factorial(n - k));
}
//числа Бернулли
double bernouli(int n) {
	if (n <= 0)
		return 1;
	else {
		double s = 0;
		int k;1
		for (k = 1; k <= n; k++)
			s = s + bin_k(n + 1, k + 1)*bernouli(n - k);
		return -1. / (n + 1)*s;
	}
}
