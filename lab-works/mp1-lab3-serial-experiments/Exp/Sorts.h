#ifndef __SORTS_H
#define __SORTS_H

#define start QueryPerformanceCounter(&sQP)
#define stop QueryPerformanceCounter(&fQP)

void Insertion_Sort(float *mass, int size);
void Merge_Sort(float *mass, int size);
float compare(const void * val1, const void * val2);

#endif
