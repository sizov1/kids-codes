#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <time.h>
#include "Sorts.h"

#define start QueryPerformanceCounter(&sQP)
#define stop QueryPerformanceCounter(&fQP)


float Randomize();

int main(int argc, char *argv[]) {

	LARGE_INTEGER freq;         
	LARGE_INTEGER sQP, fQP;

	int is_error = 0;			
	int size, seed, sort_id;		
	float *mass;

	QueryPerformanceFrequency(&freq);			
	srand(GetTickCount());

	if (argc != 4) {        
		is_error = -1;

		printf("Error!\n");
		printf("Usage: Attempt_2.exe <arg1> <arg2> <arg3> \n");
		printf("<arg1> - size mass\n");
		printf("<arg2> - seed for random\n");
		printf("<arg3> - sort: 1 - Insrtion sort\n");
		printf("               2 - Merge sort\n");
		printf("               3 - Qsort");

	}
	else {
		sscanf(argv[1], "%d", &size);
		sscanf(argv[2], "%d", &seed);
		sscanf(argv[3], "%d", &sort_id);
		if ((size < 0) || (sort_id < 1) || (sort_id > 3)) {
			is_error = -2;
		}
	}

	mass = (float*)malloc(sizeof(float)*size);	
	srand(seed);					

								
	for (int i = 0; i < size; i++) {
		mass[i] = Randomize();
		if (_isnan(mass[i]) != 0) {
			i--;
		}
	}


	if (sort_id == 1) {
		start;
		Insertion_Sort(mass, size);
		stop;
	}
	else if (sort_id = 2) {
		start;
		Merge_Sort(mass, size);
		stop;
	}
	else if (sort_id = 3) {
		start;
		qsort(mass, size, sizeof(float), compare);
		stop;
	}

	printf("%f %f", mass[size / 13], ((fQP.QuadPart - sQP.QuadPart) / (double)freq.QuadPart));
	free(mass);
	return is_error;
}

float Randomize() {
	float y = 0;
	unsigned char *pc = (unsigned char *)&y;
	for (int j = 0; j < sizeof(float); j++) {
		pc[j] = rand();
	}
	return y;
}