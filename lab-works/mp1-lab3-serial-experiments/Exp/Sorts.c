#include <stdlib.h>
#include "Sorts.h"

void Insertion_Sort(float *mass, int size) {
	float newElement;
	int location;

	for (int i = 1; i < size; i++) {
		newElement = mass[i];
		location = i - 1;
		while (location >= 0 && mass[location] > newElement) {
			mass[location + 1] = mass[location];
			location--;
		}
		mass[location + 1] = newElement;
	}
}
void Merge_Sort(float *mass, int size) {
	int step = 1;      
	float *Temp;                                
	Temp = (float*)malloc(size * sizeof(float));  

	while (step < size) {
		int Index = 0;				   
		int Left = 0;				  
		int Mid = Left + step;		   
		int Right = Left + step * 2;   
		do {
			Mid = Mid < size ? Mid : size;  
			Right = Right < size ? Right : size;
			int i1 = Left, i2 = Mid;		

			for (; i1 < Mid && i2 < Right;) {   
				if (mass[i1] < mass[i2]) {
					Temp[Index++] = mass[i1++];    
				} else {
					Temp[Index++] = mass[i2++];
				}
			}

			while (i1 < Mid) {					
				Temp[Index++] = mass[i1++];		
			}
			while (i2 < Right) {
				Temp[Index++] = mass[i2++];
			}

			Left += step * 2;   
			Mid += step * 2;
			Right += step * 2;

		} while (Left < size);  
		for (int i = 0; i < size; i++) {
			mass[i] = Temp[i];
		}
		step *= 2;
	}
}

float compare(const void * val1, const void * val2) {
	return (*(float*)val1 - *(float*)val2);
}
