#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ExtRun.h"

#define BUF_SIZE 1024

double minimum(double times[]);

int main(int argc, char *argv[]) {
	int is_error = 0;
	
	int input, method;
	int begin, end, step;
	char *app;

	if (argc != 7) {
		is_error = -1;

		printf("Error!\n");
		printf("Usage: Tests.exe <app> <arg1> <arg2> <arg3> <arg4> <arg5>\n");
		printf("<app> - app name\n");
		printf("<arg2> - input method\n");
		printf("<arg3> - method: 1 - Insrtion sort\n");
		printf("                 2 - Merge sort\n");
		printf("                 3 - Qsort");
		printf("<arg4> - begin\n");
		printf("<arg5> - end\n");
		printf("<arg6> - step\n")

	} else {
		sscanf(argv[2], "%d", &input);
		sscanf(argv[3], "%d", &method);
		sscanf(argv[4], "%d", &begin);
		sscanf(argv[5], "%d", &end);
		sscanf(argv[6], "%d", &step);
	}

	app = argv[1];
	FILE *element13;
	FILE *for_graph;
	for_graph = fopen("for_graph.csv", "w");
	

	for (int i = 0; begin + (i*step) <= end; i++) {
		char cmdLine[100];	 
		char buf[BUF_SIZE];  
		int readBytes;
		float element;		
		double times[3];   
		double time;       
		double min_time;   
		
		sprintf(cmdLine, "%s %i %i %i", app, begin + (i*step), input, method);    
		memset(buf, 0, sizeof(buf));	

		for (int k = 0; k < 3; k++) {
			readBytes = RunProgramm(cmdLine, buf, BUF_SIZE);    
			sscanf(buf, "%lf%lf", &element, &time);
			times[k] = time;
		}

		min_time = minimum(times);

		fprintf(element13, "%f ", element);            
		fprintf(element13, "%lf\n", min_time);

		fprintf(for_graph, "%i,", (begin + (i*step)));  
		fprintf(for_graph, "%lf\n", min_time);

		printf("%i\a\n", (begin + (i*step)));
	}

	fclose(element13);
	fclose(for_graph);

	return is_error;

}


double minimum(double times[]) {
	double m = 10000;
	for (int p = 0; p < 3; p++) {
		if (times[p] < m) {
			m = times[p];
		}
	}
	return m;
}