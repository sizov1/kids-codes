#include <windows.h>

#define true 1

int RunProgramm(char *cmdLine, char *buf, int bufSize){
    DWORD readBytes;   
    DWORD avail;
    HANDLE read_stdout,write_stdout; 
    SECURITY_ATTRIBUTES sa;          
    PROCESS_INFORMATION pi;          
    STARTUPINFO info;
 
    sa.lpSecurityDescriptor = NULL;
    sa.nLength = sizeof(SECURITY_ATTRIBUTES);
    sa.bInheritHandle = true;
  
    CreatePipe(&read_stdout,&write_stdout,&sa,0);

    memset(&info, 0, sizeof(STARTUPINFO));
    info.dwFlags = STARTF_USESTDHANDLES|STARTF_USESHOWWINDOW;
    info.wShowWindow = SW_SHOWNORMAL;
    info.hStdOutput = write_stdout;
    info.hStdError = write_stdout;   

    CreateProcess(NULL,cmdLine,NULL,NULL,TRUE,CREATE_NEW_CONSOLE,NULL,NULL,&info,&pi);
    WaitForSingleObject(pi.hProcess, INFINITE);

    PeekNamedPipe(read_stdout,buf,bufSize-1,&readBytes,&avail,NULL);

    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);
    CloseHandle(write_stdout);
    CloseHandle(read_stdout);

    return readBytes;
}
