import numpy as np

def Q(x, y):
	return (1 - x*x + y)**2 + (np.cos(x*x*y) + x)**3

def dQdx(x, y):
	return -4*x*(1 - x*X + y) + 3*(1 - 2*x*y*np.sin(x*x*y))*(np.cos(x*x*y)+x)**2

def d2Qdx2(x, y):
	return 8*x**2 + 4*(x*x - y - 1) \
	+ 6*(np.cos(x*x*y)+x)*(1 - 2*x*y*np.sin(x*x*y))**2 \
	- 6*y*(np.sin(x*x*y)+2*x*x*np.cos(x*x*y))*(np.cos(x*x*y)+x)**2

def dQdy(x, y):
	return -3*x**2*np.sin(x*x*y)*(np.cos(x*x*y) + x)**2 - 2*x**2 + 2*y + 2

def d2Qdy2(x, y):
	return -3*(x**4)*np.cos(x*x*y)*((np.cos(x*x*y) + x)**2) \
	+ 6*(x**4)*(np.cos(x*x*y) + x)*(np.sin(x*x*y)**2) + 2

def d2Qdxdy(x, y):
	return -4*x - 6*(x**3)*y*np.cos(x*x*y)*(x + np.cos(x*x*y))**2 \
	- 6*x*np.sin(x*x*y)*(x + np.cos(x*x*y))**2 \
	- 6*(x**2)*(x + np.cos(x*x*y))*np.sin(x*x*y)*(1 - 2*x*y*np.sin(x*x*y))

def hessian(x, y):
	hessian = np.empty((2,2), dtype=x.dtype)
	hessian[0][0] = d2Qdx2(x, y)
	hessian[0][1] = d2Qdxdy(x, y)
	hessian[1][0] = hessian[0][1]
	hessian[1][1] = d2Qdy2(x, y)
	return hessian


x = np.arange(-1, 5, 0.1)
y = np.arange(0, 5, 0.1)
eigs = []
good_x = []
good_y = []
for xi in x:
	for yi in y:
		eigs.append(np.linalg.eig(hessian(xi, yi))[0])
		if not (eigs[-1]>0).any():
			good_x.append(xi)
			good_y.append(yi)
			break
print(good_x[0], good_y[0])
print(len(good_x))