function plane_turbulence()
  Ts = 0.05; % ��� �������������
  nx = 5; % ����������� ������� ���������
  ny = 2; % ����������� ������� ���������
  trb = 30; % ����� ���������� ����������
  sigma_v = 980; % ��� ���������� ���������
  
  a_mz_alpha = 4.2; a_mz_omegaz = 1.5;
  a_mz_delta = -7.5;
  a_y_alpha = -0.77; V = 350.0; L = 400.0;
  
  Ac = [0 1 0 0 0; 
  -a_mz_alpha -a_mz_omegaz a_mz_alpha 0 -a_mz_alpha / V;
  -a_y_alpha 0 a_y_alpha 0 -a_y_alpha / V;
  0 0 0 -V/L 0;
  0 0 0 -V/L -V/L];
  Cc = [1 0 0 0 0; 0 1 0 0 0];
  G = [0; 0; 0; (sqrt(3) - 1) * sqrt(V / L); sqrt(3*V / L)];
  Bc = [0; a_mz_delta; 0; 0; 0];
  
  Qc = sigma_v^2;
  Rc = [0.01 0; 0 0.001];
  
  sysc = ss(Ac, Bc, Cc, [0]);
  sysd = c2d(sysc, Ts, 'zoh');

  A = sysd.a;
  B = sysd.b;
  C = sysd.c;
  Qc = G * Qc * G';
  Qc = (Qc + Qc') / 2;
  Rc = (Rc+Rc')/2;
  M = [-Ac Qc; zeros(nx) Ac'];
  phi = expm(M * Ts);
  phi12 = phi(1:nx, nx+1:2*nx);
  phi22 = phi(nx+1:2*nx,nx+1:2*nx);
  Qd = phi22'*phi12;
  Qd = (Qd+Qd')/2;
  Rd = Rc / Ts;
  H = chol(Qd, 'lower');
  L = chol(Rd, 'lower');
  
  t = 0:Ts:trb;
  n = length(t);
  
  x = zeros(nx, n + 1);
  y = zeros(ny, n);
  hatx = zeros(nx, n);
  xs = zeros(nx, n);
  u = [0];
  
  %initial condition
  x(1:nx, 1) = [2;1;1;0;0];
  Kk = zeros(nx, ny);
  Psk = [1 0 0 0 0;
  0 2 0 0 0; 
  0 0 3 0 0;
  0 0 0 4 0;
  0 0 0 0 5];
  sigma_vartheta = 0.0
  
  %Kalman filter
  for k = 1:n
    x(1:nx, k + 1) = A * x(1:nx, k) + B * u + H * randn(nx, 1); 
    y(1:ny, k) = C * x(1:nx, k) + L * randn(ny, 1);
      
    Kk = Psk * C' * (C * Psk * C' + Rd)^(-1);
    Pwk = Psk - Kk * C * Psk;
    Psk = A * Pwk * A' + Qd;
      
    hatx(1:nx, k) = xs(1:nx, k) + Kk * (y(1:ny, k) - C * xs(1:nx, k));
    xs(1:nx, k + 1) = A * hatx(1:nx, k);
  endfor;
  
  haty = GetHatY(C, hatx, nx, ny, n);
  
  sigma_omega = std(x([2], 1:n) - hatx([2], 1:n), 1);
  sigma_vartheta = std(x([1], 1:n) - hatx([1], 1:n), 1);
  str_title = strcat("\\sigma_v = ", num2str(sigma_v));
  str_title1 = strcat(str_title, " ;\\sigma_{e_1} = ", num2str(sigma_vartheta));
  str_title2 = strcat(str_title, " ;\\sigma_{e_2} = ", num2str(sigma_omega));
  
  figure();
  subplot(2, 1, 1);
  line_hatvartheta = plot(t, haty([1], 1:n), "color", "b", "linewidth", 2); 
  hold on;
  line_yvartheta = plot(t, y([1], 1:n), "color", "r");
  grid on;
  xlabel("t", "fontsize", 16);
  ylabel("haty_1, y_1", "fontsize", 16);
  title(str_title1, "fontsize", 16, "interpreter", "tex");
  leg1 = legend("haty_1", "y_1");
  legend (leg1, "location", "northeastoutside");
  set(leg1, "fontsize", 16);
  
  subplot(2, 1, 2);
  line_evartheta = plot(t, x([1], 1:n) -  hatx([1], 1:n), "color", "k");
  xlabel("t", "fontsize", 16);
  ylabel("e_1 = x_1 - hatx_1", "interpreter", "tex", "fontsize", 16);
  grid on;
  
  figure();
  subplot(2, 1, 1);
  line_hatomega = plot(t, haty([2], 1:n), "color", "b");
  hold on;
  line_yomega = plot(t, y([2], 1:n), "color", "r");
  grid on;
  xlabel("t", "fontsize", 16);
  ylabel("haty_2, y_2", "fontsize", 16);
  title(str_title2, "fontsize", 16, "interpreter", "tex");
  leg2 = legend("haty_2", "y_2");
  legend (leg2, "location", "northeastoutside");
  set(leg2, "fontsize", 16);
  
  subplot(2, 1, 2);
  line_evartheta = plot(t, x([2], 1:n) -  hatx([2], 1:n), "color", "k");
  xlabel("t", "fontsize", 16);
  ylabel("e_2 = x_2 - hatx_2", "interpreter", "tex", "fontsize", 16);
  grid on;
  
endfunction

function haty = GetHatY(C, hatx, nx, ny, n)
  haty = zeros(ny, n);
  for k = 1:n
    haty(1:ny, k) = C * hatx(1:nx, k);
  endfor
endfunction
