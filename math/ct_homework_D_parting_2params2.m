border = 5;
step = 0.1
t1 = -border:step:0;
t2 = 0:step:2;
t3 = 2:step:border;

n1 = length(t1);
n2 = length(t2);
n3 = length(t3);

x1 = zeros(n1);
x2 = zeros(n2);
x3 = zeros(n3);

y1 = x1;
y2 = x2;
y3 = x3;

for j = 1:n1
  w = t1(j);
  x1(j) = (w^2 - 4*w +  2) / (-2 * w^2);
  y1(j) = (w^2 + 4*w + 2) / (-2 * w^2);
endfor

for j = 1:n2
  w = t2(j);
  x2(j) = (-w^3 + 3 * w^2 - 4 * w + 2) / (2*w^3 - 6 * w^2 + 4 * w);
  y2(j) = (-w^3 + 3 * w^2 - 4 * w + 2) / (2*w^3 - 6 * w^2 + 4 * w);
endfor

for j = 1:n3
  w = t3(j);
  x3(j) = (-w^3 + 3 * w^2 - 2 * w + 2) / (w^3 - 3 * w^2 - 2 * w + 8);
  y3(j) = (-w^2 + 8 * w - 14) / (w^3 - 3 * w^2 - 2 * w + 8);
endfor

plot(x1, y1, clr="g")
hold on
plot(x2, y2, clr="r")
hold on
plot(x3, y3)