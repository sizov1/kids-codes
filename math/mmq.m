function mmq()
   X = [0, 4, 10, 15, 21, 29, 36, 51, 68];
   Y = [66.7, 71.0, 76.3, 80.6, 85.7, 92.9, 99.4, 113.6, 125.1];
   n = length(X);
   K = 1;
   X2 = 1:n;
   YX = 1:n;
   
   A = [length(X) sum(X); sum(X) X*X'];
   b = [sum(Y); Y*X'];
   coeffs = A\b;
   display(coeffs);
   
   x = 0:0.01:70;
   hatY = x;
   N = length(x);
   for k = 1:N
     hatY(k) = coeffs(1) + coeffs(2)*x(k);
   endfor
   
   plot(x, hatY); hold on;
   
   for k = 1:n
     scatter(X(k), Y(k), "r", "filled");
   endfor
   
   leg1 = legend("���-�����������", "�������� ������");
   %legend (leg1, "location", "northeastoutside");
   
   xlabel("X");
   ylabel("Y");
   grid on;
   
   figure();
   eps = 1:n
   for k = 1:n
     eps(k) = Y(k) - coeffs(1) - coeffs(2)*X(k);
     scatter(X(k), eps(k), "r", "filled"); hold on;
   endfor
   plot(X, eps);
   title("������ ��������")
   xlabel("X");
   ylabel("\\epsilon", "interpreter", "tex");
   grid on;
endfunction
