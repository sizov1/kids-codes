x = -5:0.01:5;
y = x;

y1 = x;
y2 = x;
y3 = x;

n = length(x);

for i = 1:n
  y1(i) = (9/8)*x(i)*x(i); 
  y2(i) = 0.5 - 0.5*x(i);
  y3(i) = sqrt(1 - 1.5*(x(i))^3);
endfor


plot(y1, x, "r", "linewidth", 2); hold on
plot(x, y2, "g", "linewidth", 2);
plot(y3,x, "b", "linewidth", 2); hold on
%plot(-y3,x, "b", "linewidth", 2);
grid on;

