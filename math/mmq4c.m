function mmq4c()
  X1 = [0.1, 0.2, 0.4, 0.5, 0.5];
  X2 = [0.1, 0.3, 0.7, 0.9, 1.0];
  Y = [3.0, 3.5, 3.6, 2.9, 2.8];
  
  A = [X1*X1' X1*X2'; X1*X2' X2*X2'];
  b = [X1*Y'; X2*Y']
  coeffs = A\b
  
  [x1 x2] = meshgrid(0:0.1:1.1, 0:0.1:1.1);
  haty = coeffs(1) * x1 + coeffs(2) * x2;
  mesh(x1, x2, haty, "facecolor", "none", "edgecolor", "b");
  hatY = 1:5;
  hold on;
  for k = 1:5
    hatY(k) = coeffs(1) * X1(k) + coeffs(2) * X2(k);
  endfor
  eps = max(abs(Y - hatY))
  for k = 1:5
    scatter3(X1(k), X2(k), Y(k), 80, "r", "filled"); hold on;
  endfor
  legend("���-�����������", "�������� ������");
  xlabel("X1");
  ylabel("X2");
  zlabel("Y");
  grid on;
  figure();
  for k = 1:5
    scatter(k, abs(Y(k) - hatY(k)), "b", "filled"); hold on
  endfor
  plot(1:5, abs(Y - hatY));
  xlabel("����� �������, i");
  ylabel("\\epsilon", "interpreter", "tex");
    title("������ ��������");
  grid on;
endfunction
