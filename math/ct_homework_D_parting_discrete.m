function ct_homework_D_parting_discrete()
  step = 0.01
  k = 0:step:2*pi;
  n = length(k);
  x1 = zeros(1, n);
  y1 = zeros(1, n);
  x2 = x1;
  y2 = y1;
  for j = 1:n
    f = k(j);
    z = e^(f*i);
    x1(j) = real((z - 1)/2 + sqrt(5*z^2 - 10*z - 3));
    y1(j) = imag((z - 1)/2 + sqrt(5*z^2 - 10*z - 3));
    x2(j) = real((z - 1)/2 - sqrt(5*z^2 - 10*z - 3));
    y2(j) = imag((z - 1)/2 - sqrt(5*z^2 - 10*z - 3));
  endfor
  plot(x1(1:n/2), y1(1:n/2)); hold on 
  plot(x2(1:n/2), y2(1:n/2));
  %plot(x1, y1); hold on
  %plot(x2, y2);
endfunction
