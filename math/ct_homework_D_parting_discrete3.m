a = -2:0.01:2;
n = length(a);
x = ones(4, n)*i;
for i = 1:n
  c = [1-a(i), 1, a, a, a];
  rs = roots(c);
  m = length(rs);
  for k = 1:m
    x(k, i) = rs(k);
  endfor
endfor

plot(real(x(1, [1:n])), imag(x(1, [1:n])), clr="r");
hold on
plot(real(x(2, [1:n])), imag(x(2, [1:n])), clr="b");
hold on
plot(real(x(3, [1:n])), imag(x(3, [1:n])), clr="g");
hold on
plot(real(x(4, [1:n])), imag(x(4, [1:n])), clr="y");

