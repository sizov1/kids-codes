#include <iostream>
#include <string>
using namespace std;

template <class ValType>
class TQueue {
	
	private: 

		ValType* data;
		unsigned Size;
		unsigned CurrSize;
		unsigned StartIndex;
		unsigned EndIndex;

	public:

		TQueue(unsigned s = 10);
		TQueue(const TQueue& q);
		~TQueue();

		bool IsEmpty();
		bool IsFull();

		void push(const ValType& val);
		ValType pop();

};


template <class ValType>
TQueue<ValType>::TQueue(unsigned s) {
	Size = s;
	StartIndex = 0;
	EndIndex = 0;
	CurrSize = 0;
	data = new ValType[Size];
}

template <class ValType>
TQueue<ValType>::TQueue(const TQueue& q) {
	Size = q.Size;
	CurrSize = q.CurrSize;
	StartIndex = q.StartIndex;
	EndIndex = q.EndIndex;
	data = new ValType[Size];

	for(unsigned i = 0; i < Size; i++) {
		data[i] = q.data[i];
	}
}

template <class ValType>
TQueue<ValType>::~TQueue() {
	delete[] data;
}

template <class ValType>
bool TQueue<ValType>::IsFull() {
	if (CurrSize == Size) {
		return true;
	}
	return false;
}

template <class ValType>
bool TQueue<ValType>::IsEmpty() {
	if (CurrSize == 0) {
		return true;
	}
	return false;
}

template <class ValType>
void TQueue<ValType>::push(const ValType& val) {
	if (CurrSize != 0 && !IsFull()) {
		data[EndIndex] = val;
		CurrSize++;
		EndIndex = (EndIndex + 1) % Size;
	} else {
		throw ("cannot push: queue full");
	}
}

template <class ValType>
ValType TQueue<ValType>::pop() {
	if (!IsEmpty()) {
		CurrSize--;
		ValType val = data[StartIndex];
		StartIndex = (StartIndex +1) % Size;
		return val;
	} else {
		throw ("cannot pop: queue empty");
	}
}

int main() {

	TQueue<int> test_queue(10);
	cout << "create queue with size 10" << endl;

	cout << "TEST FUNCTION IsEmpty()" << endl;
	cout << "expect: queue empty" << endl;
	string test_isempty;
	if (test_queue.IsEmpty()) {
		test_isempty = "empty";
	} else {
		test_isempty = "not empty";
	}
	cout << "actual: queue " << test_isempty << endl << endl;

	test_queue.push(4);
	test_queue.push(111);
	test_queue.push(98);
	test_queue.push(21);
	test_queue.push(0);
	test_queue.push(-9);
	test_queue.push(45);
	test_queue.push(7);
	test_queue.push(12);
	test_queue.push(8);


	cout << "TEST FUNCTION IsFull()" << endl;
	cout << "expect: queue full" << endl;
	string test_isfull;
	if (test_queue.IsFull()) {
		test_isfull = "full";
	} else {
		test_isfull = "not full";
	}
	cout << "actual: queue " << test_isfull << endl << endl;

	cout << "TEST FUNCTION pop()" << endl;
	cout << "expect: 4" << endl;
	cout << "actual: " << test_queue.pop() << endl << endl;

	test_queue.pop();
	test_queue.pop();
	test_queue.pop();
	test_queue.pop();
	test_queue.pop();
	test_queue.pop();
	test_queue.pop();
	test_queue.pop();
	test_queue.pop();

	cout << "TEST FUNCTION IsEmpty()" << endl;
	cout << "expect: queue empty" << endl;
	if (test_queue.IsEmpty()) {
		test_isempty = "empty";
	} else {
		test_isempty = "not empty";
	}
	cout << "actual: queue " << test_isempty << endl << endl;

	cin.get();
}