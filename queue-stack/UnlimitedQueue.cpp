#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Exception
{
    private:
        string msg_;
    public:
        Exception(string msg) {
            msg_ = msg;
            cerr << msg_ << endl;
        }
};

template <class ValType>
class TStack
{
    private:

		ValType* data;
		int top_;
		int maxSize_;

	public:

        TStack(int maxSize);
        TStack();
        TStack(const TStack& s);
        ~TStack();

        bool isFull();
        bool isEmpty();

        void push(const ValType& v);
        ValType pop();
        ValType getTop();

};

template <class ValType>
class TQueue
{
    private:

		TStack<ValType> st1_;
		TStack<ValType> st2_;

	public:

		TQueue(){;}

		void push(const ValType& v);
		ValType pop();

		bool isEmpty();

};


template <class ValType>
void TQueue<ValType>::push(const ValType& val)
{
	if (st1_.isEmpty()) {
		while(!st2_.isEmpty()) {
			st1_.push(st2_.pop());
		}
	}
	st1_.push(val);
}

template <class ValType>
ValType TQueue<ValType>::pop() 
{
    if (isEmpty()) {
	    throw Exception("Queue is empty");
	}

    while(!st1_.isEmpty()){
        st2_.push(st1_.pop());
    }
    return st2_.pop();
}

template <class ValType>
bool TQueue<ValType>::isEmpty() 
{
	return (st1_.isEmpty() && st2_.isEmpty());
}


template <class ValType>
TStack<ValType>::TStack(int maxSize)
{
    if (maxSize <= 0) {
        throw Exception("cant create stack with null size");
    }

    maxSize_ = maxSize;
    data = new ValType[maxSize_];
    top_ = -1;
}

template <class ValType>
TStack<ValType>::TStack() 
{
    maxSize_ = 100;
    data = new ValType[maxSize_];
    top_ = -1;
}

template <class ValType>
TStack<ValType>::TStack(const TStack& s)
{
    maxSize_ = s.maxSize_;
    top_ = s.top_;
    data = new ValType[maxSize_];

    for (int i = 0; i < top_; i++) {
        data[i] = s.data[i];
    }
}

template <class ValType>
TStack<ValType>::~TStack() 
{
    delete[] data;
}

template <class ValType>
bool TStack<ValType>::isFull()
{
    if (top_ == maxSize_ - 1) {
        return true;
    }

    return false;
}

template <class ValType>
bool TStack<ValType>::isEmpty() 
{
    if (top_ == -1) {
        return true;
    }

    return false;
}

template <class ValType>
void TStack<ValType>::push(const ValType& val) 
{
    if (isFull()) {

        maxSize_ = (maxSize_ + 1) * 2;
        ValType* tmp = new ValType[maxSize_];

        for (int i = 0; i < top_; i++) {
            tmp[i] = data[i];
        }

        delete[] data;
        data = tmp;

    }

    data[++top_] = val;
}

template <class ValType>
ValType TStack<ValType>::pop() 
{
    if (isEmpty()) {
        throw Exception("stack is Empty");
    }
    
    return data[top_--];
}

template <class ValType>
ValType TStack<ValType>::getTop() 
{
    if (isEmpty()) {
        throw Exception("stack is Empty");
    }

    return data[top_];
}


int main() {

	TQueue<int> q;

	q.push(1);
	q.push(2);
	q.push(3);
	cout << q.pop();
	q.push(4);
	cout << q.pop();

	system("pause");
}