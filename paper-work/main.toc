\providecommand \autonum@processReference [2]{}
\babel@toc {russian}{}
\contentsline {section}{Введение}{3}%
\contentsline {section}{\numberline {1}Серверные фермы с разогревом}{4}%
\contentsline {section}{\numberline {2}Постановка задачи}{5}%
\contentsline {section}{\numberline {3}Построение модели}{6}%
\contentsline {section}{\numberline {4}Вычисление интенсивностей переходов}{8}%
\contentsline {section}{\numberline {5}Матрица интенсивностей переходов}{11}%
\contentsline {subsection}{\numberline {5.1}Система дифференциальных уравнений Колмогорова}{11}%
\contentsline {subsection}{\numberline {5.2}Структура вектора $\vec {p}(t)$ и матрицы $Q$}{12}%
\contentsline {subsection}{\numberline {5.3}Структура матриц $N_k, M_k, \Lambda _k$}{15}%
\contentsline {section}{\numberline {6}Стационарное распределение}{16}%
\contentsline {subsection}{\numberline {6.1}Система уравнений для поиска стационарного распределения}{16}%
\contentsline {subsection}{\numberline {6.2}Поиск однородных компонент вектора $\vec {\pi }$}{18}%
\contentsline {subsection}{\numberline {6.3}Поиск неоднородных компонент вектора $\vec {\pi }$}{19}%
\contentsline {section}{\numberline {7}Вычисление основных характеристик системы}{20}%
\contentsline {subsection}{\numberline {7.1}Среднее число занятых серверов в стационарном режиме}{20}%
\contentsline {subsection}{\numberline {7.2}Среднее число разогревающихся серверов в стационарном режиме}{21}%
\contentsline {subsection}{\numberline {7.3}Среднее число требований системы в стационарном режиме}{21}%
\contentsline {subsection}{\numberline {7.4}Среднее значение потребляемой энергии}{21}%
\contentsline {section}{\numberline {8}Программная реализация поиска стационарного распределения}{22}%
\contentsline {section}{\numberline {9}Проведение численных экспериментов}{25}%
\contentsline {subsection}{\numberline {9.1}Зависимость численных характеристик от $\alpha $}{25}%
\contentsline {subsection}{\numberline {9.2}Зависимость численных характеристик от $c$}{27}%
\contentsline {subsection}{\numberline {9.3}Зависимость численных характеристик от $\rho $}{29}%
\contentsline {subsection}{\numberline {9.4}Итоги экспериментов}{30}%
\contentsline {section}{\numberline {10}Заключение}{31}%
