import numpy as np
import math
import random as random
import matplotlib.pyplot as plt

## 1.1) Implement the calculation of the integral of the function of one variable by the Monte-Carlo method1
## 1.2) Implement the calculation of the integral of the function of one variable by the Monte-Carlo method2
## 1.3) Implement the calculation of the integral of the function of one variable by rectangle rule

## 2.1) Conduct a series of tests for each method with an increasing number of points
## 2.2) Display a graphical representation of the Monte Carlo symbol1

def f(x):
	return np.sin(x)

def df_dx(x):
	return np.cos(x)

def ExactSolution1():
	a, b = GetInterval()
	return -np.cos(b) + np.cos(a)

def GetInterval():
	return 2.0, 5.0

def GetVerge(yi):
	return np.max(yi) + 1, np.min(yi) - 1

def RectangleRule(n):
	a, b = GetInterval()
	h = (b - a) / n
	xi = np.arange(a, b, h)
	si = f(xi - (h / 2.0))
	return np.sum(si) * h

def MonteCarlo2(n):
	uniform_xi = np.random.uniform(a, b, n)
	si = f(uniform_xi)
	return (b - a) * np.sum(si) / n

def MonteCarlo1(n):
	xi = np.arange(a, b, (b - a) / n)
	yi = f(xi)

	k1, k2 = 0.0, 0.0
	for i in range (0, n):
		x, y = np.random.uniform(a, b), np.random.uniform(d, c)
		listX[i], listY[i] = x, y
		if y >= 0 and y < f(x):
			k1 += 1
		elif y < 0 and y > f(x):
			k2 += 1

	S1 = ((k1 - k2) / n) * (b - a) * (c - d)
	return S1

def VisualizateMonteCarlo1(n, c, d, listX, listY):
	xi = np.arange(a, b, (b - a) / n)
	yi = f(xi)
	ai = np.ones(np.size(xi)) * a
	bi = np.ones(np.size(xi)) * b
	cd = np.arange(d, c, (c - d) / n)

	fig, ax = plt.subplots()
	l1 = plt.plot(xi, yi, linewidth=3, label='f(x)', color = "r")
	plt.plot(xi, d * np.ones(np.size(xi)), color = "b")
	plt.plot(xi, c * np.ones(np.size(xi)), color = "b")
	plt.plot(ai, cd, color = "b")
	plt.plot(bi, cd, color = "b")
	ax.scatter(listX[0:n], listY[0:n], linewidth=0.3, marker = ".", color = "g")

	chartBox = ax.get_position()
	ax.set_position([chartBox.x0, chartBox.y0, chartBox.width*0.9, chartBox.height])
	ax.legend(loc='upper center', bbox_to_anchor=(1.125, 0.8))
	ax.text(2.1, 2.3, u"number of dots = 1000", fontsize=12)
	plt.show()

def RunExperiment(n):
	I = ExactSolution1()
	I1 = MonteCarlo1(n)
	I2 = MonteCarlo2(n)
	I3 = RectangleRule(n)
	return abs(I - I1), abs(I - I2), abs(I - I3)

results = np.array([100, 0.0, 0, 0, 
					1000, 0, 0, 0, 
					10000, 0, 0, 0, 
					100000, 0, 0, 0, 
					1000000, 0, 0, 0]).reshape(5, 4)

ns = [100, 1000, 10000, 100000, 1000000]
a, b = GetInterval()
xi = np.linspace(a, b, ns[1])
yi = f(xi)
c, d = GetVerge(yi)
listX = np.zeros((ns[4]))
listY = np.zeros((ns[4]))

for i in range(0, 5):
	results[i][1], results[i][2], results[i][3] = RunExperiment(ns[i])
print(results)

VisualizateMonteCarlo1(ns[1], c, d, listX, listY)
