import numpy as np
import math
import random as random
import matplotlib.pyplot as plt


def calculate_combinations(n, r):
    return math.factorial(n) / (math.factorial(n - r) * math.factorial(r))

def calculate_exact_probability():		
	S = 0
	for j in range (3, 6):
	    S += pow(0.6, j) * pow(0.4, 5 - j) * calculate_combinations(5, j)
	S += pow(0.6, 2) * pow(0.4, 3) * 0.7 * calculate_combinations(5, 2)
	S += 0.6 * pow(0.4, 4) * 0.5 * calculate_combinations(5, 1)
	return S

def get_shots():
    shots = []
    for i in range(0, 5):
        shots.append(random.random())
    return shots


def experiments():
	x = []
	y = []
	for N in range(1, 10000):
		PA = 0
		for i in range(0, N):
		    shots = get_shots()
		    shots.sort()
		    if shots[2] < 0.6:
		        PA += 1
		    elif shots[1] < 0.6:
		        if random.random() < 0.7:
		            PA += 1
		    elif shots[0] < 0.6:
		        if random.random() < 0.5:
		            PA += 1
		Q = (float)(N)
		PA = (float)(PA)
		x.append(N)
		y.append(PA/Q)
	return x, y


def plot_results():
	x, y = experiments()
	exact = calculate_exact_probability() * np.ones(len(x))
	
	fig, ax = plt.subplots()
	l1 = plt.plot(x, y, label='frequency')
	l2 = plt.plot(x, exact, label='exact probability')
	plt.setp(l1, linewidth=0.5, color='r')  
	plt.setp(l2, linewidth=1, color='b')  

	ax.set(xlabel='Number of experiments', ylabel='Frequency',
       title='Dependence of frequency on the number of experiments')
	#ax.grid()


	ax.legend()
	plt.show()
	plt.savefig("cmpp-lab1-frequency.svg", format = "svg")
	return 1

plot_results()
print("probability = %.10f" % calculate_exact_probability())
#print(expriment())