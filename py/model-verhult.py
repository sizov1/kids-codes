# -*- coding: utf-8 -*-
"""
Created on Sun Sep 22 16:53:35 2019

@author: sizov
"""

from sympy import *

x = symbols('x')
k = symbols('k')
r = symbols('r')
l = symbols('l')

pprint(( 1 / (-r*(x**2)+k*x*(r - l))))
pprint(integrate( 1 / (-r*(x**2)+k*x*(r - l)) , x))