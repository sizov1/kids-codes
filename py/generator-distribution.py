import numpy as np
import math
import random as random
import matplotlib.pyplot as plt

def InverseF(y):
	return np.exp(2*y)

def ThF(x):
	if x <= 1:
		return 0
	elif x > np.exp(2):
		return 1
	return np.log(x) / 2.0

def PractF(x):
	if x <= 1:
		return 0
	elif x > np.exp(2):
		return 1
	return np.sum(vs < x) / (float)(n)

def DrawDistrFuncs():
	fig, ax = plt.subplots()
	l1 = plt.plot(xi, thF, linewidth=1.5, label='ThF(x)', color = "r")
	l2 = plt.plot(xi, prF, linewidth=1.5, label='PractF(x)', color = "g")
	ax.grid()
	plt.show()

n = int(input())
y = np.random.uniform(0, 1, n)
vs = InverseF(y) # variation series 
vs = np.sort(vs)

xi = np.linspace(-1, np.exp(2)+2, n)
vecThF = np.vectorize(ThF, otypes=[float])
vecPractF = np.vectorize(PractF, otypes=[float])
prF, thF = vecPractF(xi), vecThF(xi)


M = np.sum(vs) / n
ExactM = (np.exp(2) - 1) / 2.0
D = np.sum((vs - M) * (vs - M)) / n
Me = xi[np.where(thF > 0.5)[0][0]]
lamda = np.max(np.abs(prF - thF)) * np.sqrt(n)

print("--------------------------")
print("M = %.10f" % M)
print("Exact M = %.10f" % ExactM)
print("|M - ExactM| = %.10f" % (np.abs(M - ExactM)))
print("--------------------------")
print("D = %.10f" % D)
print("Exact D = %.10f" % ExactM)
print("|D - ExactD| = %.10f" % (np.abs(D - ExactM)))
print("--------------------------")
print("Me = %.10f" % Me)
print("Exact Me = %.10f" % np.exp(1))
print("|Me - ExactMe| = %.10f" % (np.abs(Me - np.exp(1))))
print("--------------------------")
print("lamda = %.10f" % lamda)


DrawDistrFuncs()
n, bins, patches = plt.hist(vs, 100, density=False, facecolor='r', alpha=0.8)

plt.grid(True)
plt.show()