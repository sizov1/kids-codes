import random as random
import numpy as np

def get_shots():
	rshots = []
	for i in range(0, 5):
		rshots.append(random.random())
	return rshots

N = int(input())


def expriments():
	PA = 0
	for i in range(0, N):
		shots = get_shots()
		shots.sort()
		if shots[2] < 0.6:
			PA += 1
		elif shots[1] < 0.6:
			if random.random() < 0.7:
				PA += 1
		elif shots[0] < 0.6:
			if random.random() < 0.5:
				PA += 1
	return PA

PA = expriments()
print((float)(PA) / (float)(N))

