import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from math import *

A = np.matrix('-500.005, 499.995; 499.995, -500.005')
u0 = np.matrix('7; 13')
w1 = np.matrix('1; 1')
w2 = np.matrix('1; -1')

N = int(input())
h = float(input())

def GetNumSolve(v, h):
	return v + h * A * (A * v * (h / 2.0) + v)

def GetExactSolve(x):
	return 10 * w1 * exp(-0.01 * x) - 3 * w2 * exp(-1000 * x)

v = u0
p = u0
x = np.linspace(0, 10, N)
dots = []
for i in range(0, N):
	p = np.squeeze(np.asarray(GetExactSolve(x[i])))
	dots.append(p)

u1 = []
u2 = []
for i in range (0, N):
	u1.append(dots[i][0])
	u2.append(dots[i][1])


u1 = np.asarray(u1)
u2 = np.asarray(u2)

'''
fig, ax = plt.subplots()
ax.plot(x, u1)
ax.grid()
plt.show()
'''