from scipy.optimize import fsolve
from math import *

g, l1, l2, m1, m2 = 10, 1, 2, 2, 4

def getA1(p):
	alpha1, alpha2, p1, p2 = p
	return p1*p2*sin(alpha1-alpha2)/(l1*l2*(m1+m2*(sin(alpha1-alpha2))**2))

def getA2(p):
	alpha1, alpha2, p1, p2 = p
	A2num = sin(2*(alpha1-alpha2))*(p1**2*m2*l2**2-2*p1*p2*m2*l1*l2*cos(alpha1-alpha2)+p2**2*(m1+m2)*l1**2)
	A2den = 2*l1**2*l2**2*(m1+m2*(sin(alpha1-alpha2))**2)**2
	A2 = A2num / A2den
	return A2

def eq1(p):
	alpha1, alpha2, p1, p2 = p
	return ((p1*l2-p2*l1)*cos(alpha1-alpha2))/(l1**2*l2*(m1+m2*(sin(alpha1-alpha2))**2))

def eq2(p):
	alpha1, alpha2, p1, p2 = p
	return (p2*(m1+m2)*l1-p1*m2*l2*cos(alpha1-alpha2))/(m2*l1*l2**2*(m1+m2*(sin(alpha1-alpha2)**2)))

def eq3(p):
	alpha1, alpha2, p1, p2 = p
	return -(m1+m2)*g*l1*sin(alpha1)-getA1(p)+getA2(p)

def eq4(p):
	alpha1, alpha2, p1, p2 = p
	return -m2*g*l2*sin(alpha2)+getA1(p)-getA2(p)

def equations(p):
	alpha1, alpha2, p1, p2 = p
	return (eq1(p), eq2(p), eq3(p), eq4(p))

print(fsolve(equations, (-1.0, 4.0, 9.0, 9.0)))