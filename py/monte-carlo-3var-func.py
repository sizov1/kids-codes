import numpy as np
import math
import random as random

def g(x):
	return x[1] * np.cos(x[0]) * x[2]

def ExactSolution():
	V = GetV()
	return (1.0 / 4.0) * (np.sin(V[1]) - np.sin(V[0])) * (V[3] ** 2 - V[2] ** 2) * (V[5] ** 2 - V[4] ** 2)

def GetV():
	return  math.pi / 4.0, math.pi / 2.0, 0.0, 2.0, 0.0, 2.0

def MethodUniformGrid(n):
	V = GetV()
	n = pow(n,(1.0 / 3.0))

	x1i, h1 = np.linspace(V[0], V[1], n + 1, retstep = True)
	x2i, h2 = np.linspace(V[2], V[3], n + 1, retstep = True)
	x3i, h3 = np.linspace(V[4], V[5], n + 1, retstep = True)

	h = np.array([h1, h2, h3])

	l1, l2, l3 = np.size(x1i), np.size(x2i), np.size(x3i)
	gi = []

	for x1 in range (0, l1 - 1):
		for x2 in range (0, l2 - 1):
			for x3 in range (0, l3 - 1):
				x = np.array([x1i[x1], x2i[x2], x3i[x3]]) + h * 0.5
				gi.append(g(x))
	
	c, d = np.max(gi) + 0.05, np.min(gi) - 0.05

	return np.sum(gi) * h.prod(), c, d

def MethodMonteCarlo1(n, c, d):
	V = GetV()
	k1, k2 = 0.0, 0.0

	x1 = np.random.uniform(V[0], V[1], n)
	x2 = np.random.uniform(V[2], V[3], n)
	x3 = np.random.uniform(V[4], V[5], n)
	gu = np.random.uniform(d, c, n)
	x = np.array([x1, x2, x3])
	gx = g(x)

	for i in range (0, n):
		if gu[i] >= 0 and gu[i] < gx[i]:
			k1 += 1
		elif gu[i] < 0 and gu[i] > gx[i]:
			k2 += 1

	return ((k1 - k2) / n) * (V[1] - V[0]) * (V[3] - V[2]) * (V[5] - V[4]) * (c - d)

def MethodMonteCarlo2(n):
	V = GetV()
	x1i = np.random.uniform(V[0], V[1], n)
	x2i = np.random.uniform(V[2], V[3], n)
	x3i = np.random.uniform(V[4], V[5], n)
	x = np.array([x1i, x2i, x3i])
	gi = g(x)
	return np.sum(gi) * (V[1] - V[0]) * (V[3] - V[2]) * (V[5] - V[4]) / n
	

def Experiment(n):
	I = ExactSolution()
	I3 = MethodUniformGrid(n)
	I1 = MethodMonteCarlo1(n, I3[1], I3[2])
	I2 = MethodMonteCarlo2(n)
	return abs(I1 - I), abs(I2 - I), abs(I3[0] - I)


results = np.array([100, 0.0, 0, 0, 
					1000, 0, 0, 0, 
					10000, 0, 0, 0, 
					100000, 0, 0, 0, 
					1000000, 0, 0, 0]).reshape(5, 4)

ns = [100, 1000, 10000, 100000, 1000000]
for i in range(0, 5):
	results[i][1], results[i][2], results[i][3] = Experiment(ns[i])
print(results)
