import numpy as np
import matplotlib.pyplot as plt

'''
mu, sigma = 100, 15
x = np.random.exponential(1, 10000)

# the histogram of the data
n, bins, patches = plt.hist(x, 50, density=True, facecolor='g', alpha=0.75)

plt.xlabel('Smarts')
plt.ylabel('Probability')
plt.title('Histogram of IQ')
plt.text(60, .025, r'$\mu=100,\ \sigma=15$')
plt.xlim(40, 160)
plt.ylim(0, 0.03)
plt.grid(True)
plt.show()
'''


# Fixing random state for reproducibility
np.random.seed(19680801)

mu, sigma = 100, 15
x = np.random.exponential(1, 10000)

# the histogram of the data
n, bins, patches = plt.hist(x, 100, density=True, facecolor='g', alpha=0.75)

t = np.arange(0.0, 2.0, 0.01)
s = np.exp(-t)

plt.plot(t, s)

plt.grid(True)
plt.show()
