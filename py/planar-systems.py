# Program 03a: Linear systems in the plane. See Figure 3.8(a).
# Phase portrait with vector field. Check two systems are the same.
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
import pylab as pl

# The 2-dimensional linear system.
a0 = -2
def dx_dt(x, t):
	return [x[1], -x[0] * np.cos(x[0]) - np.sin(x[0])]


def DrawPhasePortraits():
	# Trajectories in forward time.
	ts = np.linspace(0, 100, 1000)
	ic = np.linspace(-8, 8, 9)
	for r in ic:
		for s in ic:
			x0 = [r, s]
			xs = odeint(dx_dt, x0, ts)
			plt.plot(xs[:,0], xs[:,1], "r-")


	# Trajectories in backward time.
	ts = np.linspace(0, -100, 1000)
	ic = np.linspace(-8, 8, 9)
	for r in ic:
		for s in ic:
			x0 = [r, s]
			xs = odeint(dx_dt, x0, ts)
			plt.plot(xs[:,0], xs[:,1], "r-")

	# Label the axes and set fontsizes.
	plt.xlabel('x', fontsize=15)
	plt.ylabel('y', fontsize=15)
	plt.tick_params(labelsize=15)
	plt.xlim(-8, 8)
	plt.ylim(-8, 8)

	# Plot the vectorfield.
	X,Y = np.mgrid[-8:8:10j, -8:8:10j]
	u = Y
	v = - X * np.cos(X) - np.sin(X)
	pl.quiver(X, Y, u, v, color = 'b')
	plt.show()


DrawPhasePortraits()

'''
plt.clf()
ts = np.linspace(0, 4, 100)
x0 = [7, 0]
xs = odeint(dx_dt, x0, ts)
plt.plot(ts, xs[:,0], "g-")
plt.show()
'''	