import numpy as np
import math
import random as random
import matplotlib.pyplot as plt

n = 10000

fig, ax = plt.subplots()
x1i = np.linspace(-1, 1, n)
y1i = np.ones(n)
y2i = np.ones(n) * (-1.0)

plt.xlim(-2, 2)
plt.ylim(-2, 2)

ax.set_xticks(np.arange(-2, 2, 0.25))
ax.set_yticks(np.arange(-2, 2, 0.25))

ax.scatter(0.0, 1.0, linewidth=0.5, marker = "<", color = "b")
ax.scatter(-0.25, 1.0, linewidth=0.5, marker = "<", color = "b")
ax.scatter(0.25, 1.0, linewidth=0.5, marker = "<", color = "b")

ax.scatter(0.0, -1.0, linewidth=0.5, marker = ">", color = "b")
ax.scatter(-0.25, -1.0, linewidth=0.5, marker = ">", color = "b")
ax.scatter(0.25, -1.0, linewidth=0.5, marker = ">", color = "b")

ax.scatter(1.0, 0.0, linewidth=0.5, marker = "^", color = "b")
ax.scatter(1.0, -0.25, linewidth=0.5, marker = "^", color = "b")
ax.scatter(1.0, 0.25, linewidth=0.5, marker = "^", color = "b")

ax.scatter(-1.0, 0.0, linewidth=0.5, marker = "v", color = "b")
ax.scatter(-1.0, -0.25, linewidth=0.5, marker = "v", color = "b")
ax.scatter(-1.0, 0.25, linewidth=0.5, marker = "v", color = "b")

plt.plot(x1i, y1i, color = 'g')
plt.plot(x1i, y2i, color = 'g')
plt.plot(y1i, x1i, color = 'g')
plt.plot(y2i, x1i, color = 'g')

plt.grid()
plt.show()