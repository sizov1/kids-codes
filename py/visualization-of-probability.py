# -*- coding: utf-8 -*-
"""
Created on Sat Sep 28 20:19:54 2019

@author: sizov
"""

import matplotlib.pyplot as plt
import numpy as np
import random as random

np.random.seed(19680801)

mu, sigma = 100, 30
x = mu + sigma * np.random.random(1000)

# the histogram of the data
n, bins, patches = plt.hist(x, 50, density=True, facecolor='g', alpha=0.75)


plt.xlabel('Smarts')
plt.ylabel('Probability')
plt.title('Histogram of IQ')
plt.text(60, .025, r'$\mu=100,\ \sigma=15$')
plt.xlim(40, 160)
plt.ylim(0, 0.03)
plt.grid(True)
plt.show()