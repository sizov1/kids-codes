from sympy import *
import matplotlib.pyplot as plt
import numpy as np

'''
x,y=symbols('x y')

print(trigsimp(cos(x) - cos(x)**3))

print(limit((sin(x)/x), x, 0))

print(diff(x**2 - 3*x + 6, x))

print(diff(x*y - 3*sin((y*x)**2), y))

print(integrate(tan(x)), x)

print(diff(-log(cos(x)), x))

print(exp(x).series(x, 0, 5))

print(summation((1 / x**2), (x, 1, oo)))

a = solve(x**5 - 1, x)
'''

'''
x = np.linspace(-2, 2, 50)
y = x**2

r * x * (1 - x / k) - l * x

plt.plot(x, y)
'''

'''
Мягкая живая система с отловом
t = symbols('t')    
r = symbols('r')
k = symbols('k')
l = symbols('l')
x = symbols('x', cls=Function)
deqn1 = Eq(x(t).diff(t), x(t) * r * (1 - x(t) / k) - l * x(t))
sol1 = dsolve(deqn1, x(t))
print(sol1)
'''

'''
x = Function('x')
ODE1 = x(t).diff(t) + t * x(t) - t**3
pprint(dsolve(ODE1, hint='1st_power_series', n = 8, ics={x(0):1}))
'''

'''
x = Function('x')
ODE1 = x(t).diff(t, 2) + 2*t**2*x(t).diff(t) + x(t)
pprint(dsolve(ODE1, hint='2nd_power_series_ordinary', n=6))
'''
'''
def ODE1(y, x):
    dydx = 2*x
    return dydx

y0 = 100
x = np.linspace(-10, 10, 10000)
sol = odeint(ODE1, y0, x)
y = sol[:, 0]

fig, ax = plt.subplots()
ax.plot(x, y, label='Numerical')
#ax.plot(t, 1 + t**2/2 + t**4/24, 'r-', label='Truncated series')
plt.xlabel('x', fontsize=15)
plt.ylabel('y', fontsize=15)
plt.xlim(-10, 10)
plt.ylim(0, 100)

plt.show()
'''

h = float(input())
A = np.matrix('-500.005, 499.995; 499.995, -500.005')
u0 = np.matrix('7;13')
print (u0 + h * A*(u0 + (h / 2) * A * u0))