\providecommand \autonum@processReference [2]{}
\select@language {russian}
\select@language {russian}
\contentsline {section}{\numberline {1}Постановка задачи}{3}
\contentsline {section}{\numberline {2}Описание методов}{3}
\contentsline {subsection}{\numberline {2.1}Метод Монте-Карло 1}{3}
\contentsline {subsubsection}{\numberline {2.1.1}Определенный интеграл по отрезку}{3}
\contentsline {subsubsection}{\numberline {2.1.2}Определенный интеграл по объему}{4}
\contentsline {subsection}{\numberline {2.2}Метод Монте-Карло 2}{5}
\contentsline {section}{\numberline {3}Результаты}{5}
\contentsline {subsection}{\numberline {3.1}Определенный интеграл по отрезку}{5}
\contentsline {subsection}{\numberline {3.2}Определенный интеграл по объему}{6}
\contentsline {section}{\numberline {4}Заключение}{9}
\contentsline {section}{\numberline {5}Код программ}{9}
\contentsline {subsection}{\numberline {5.1}Вычисление определенного интеграла по отрезку}{9}
\contentsline {subsection}{\numberline {5.2}Вычисление определенного интеграла по объему}{10}
