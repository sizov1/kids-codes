def RectangleRule(n):
	a, b = GetInterval()
	h = (b - a) / n
	xi = np.arange(a, b, h)
	si = f(xi - (h / 2.0))
	return np.sum(si) * h

def MonteCarlo2(n):
	uniform_xi = np.random.uniform(a, b, n)
	si = f(uniform_xi)
	return (b - a) * np.sum(si) / n

def MonteCarlo1(n):
	xi = np.arange(a, b, (b - a) / n)
	yi = f(xi)

	k1, k2 = 0.0, 0.0
	for i in range (0, n):
		x, y = np.random.uniform(a, b), np.random.uniform(d, c)
		if y >= 0 and y < f(x):
			k1 += 1
		elif y < 0 and y > f(x):
			k2 += 1

	S1 = ((k1 - k2) / n) * (b - a) * (c - d)
	return S1