def MethodUniformGrid(n):
	V = GetV()
	n = pow(n,(1.0 / 3.0))

	x1i, h1 = np.linspace(V[0], V[1], n + 1, retstep = True)
	x2i, h2 = np.linspace(V[2], V[3], n + 1, retstep = True)
	x3i, h3 = np.linspace(V[4], V[5], n + 1, retstep = True)
	h = np.array([h1, h2, h3])

	l1, l2, l3 = np.size(x1i), np.size(x2i), np.size(x3i)
	gi = []

	for x1 in range (0, l1 - 1):
		for x2 in range (0, l2 - 1):
			for x3 in range (0, l3 - 1):
				x = np.array([x1i[x1], x2i[x2], x3i[x3]]) + h * 0.5
				gi.append(g(x))
	
	c, d = np.max(gi) + 0.05, np.min(gi) - 0.05

	return np.sum(gi) * h.prod(), c, d

def MethodMonteCarlo1(n, c, d):
	V = GetV()
	k1, k2 = 0.0, 0.0

	x1 = np.random.uniform(V[0], V[1], n)
	x2 = np.random.uniform(V[2], V[3], n)
	x3 = np.random.uniform(V[4], V[5], n)
	gu = np.random.uniform(d, c, n)
	x = np.array([x1, x2, x3])
	gx = g(x)

	for i in range (0, n):
		if gu[i] >= 0 and gu[i] < gx[i]:
			k1 += 1
		elif gu[i] < 0 and gu[i] > gx[i]:
			k2 += 1

	return ((k1 - k2) / n) * (V[1] - V[0]) * (V[3] - V[2]) * (V[5] - V[4]) * (c - d)

def MethodMonteCarlo2(n):
	V = GetV()
	x1i = np.random.uniform(V[0], V[1], n)
	x2i = np.random.uniform(V[2], V[3], n)
	x3i = np.random.uniform(V[4], V[5], n)
	x = np.array([x1i, x2i, x3i])
	gi = g(x)
	return np.sum(gi) * (V[1] - V[0]) * (V[3] - V[2]) * (V[5] - V[4]) / n