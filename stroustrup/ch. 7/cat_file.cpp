#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char* argv[]) {
	argc--;
	argv++;
	char buff[100];
	while (argc--) {
		ifstream fin(*argv++);
		if (!fin.is_open()) cout << "cannot open file" << endl; 
		while (fin.getline(buff, 100))	cout << buff << endl;
		fin.close();
	}
}