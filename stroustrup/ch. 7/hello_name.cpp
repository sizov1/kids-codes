#include <iostream>

using namespace std;

int main(int argc, char* argv[]) {
	argv++;
	argc--;
	while (argc--)
		cout << "Hello, " << *argv++ << "!" << endl;
}