#include <iostream>
#include <string>

using namespace std;

string Shift(const string& a, const int& i);  //���������� �������� �������� ������������� ������

int main(int argc, char* argv[]) {
	argc++; argv++;
	char* key = argv[0];
	string in;
	cin >> in;
	size_t len = in.length();
	for (int i = 0; i < len; i++) {
		in[i] = in[i] ^ key[i];
	}

	for(int i = 0; i < 30; i++) {   //�������� ������� ������
		cout << "*" << endl;		//����� ������ �� � ������
	}

	int binStr[1024];
	for (int j = 0; j < len; j++) {   //�������� ������� ������������� ������ 
		for (int i = 0; i < 8; i++) {
			binStr[i + 8*j] = (in[j] >> (7 - i)) & 1;    //� ���������� ��� ������������� � ������
		}
	}
/*
	string shiftIn;
	shiftIn = Shift(in, 1);
	cout << in << endl << shiftIn << endl;

	int binStrShift[1024];
	for (int j = 0; j < shiftIn.length(); j++) {   //�������� ������� ������������� ������ 
		for (int i = 0; i < 8; i++) {
			binStrShift[i + 8 * j] = (shiftIn[j] >> (7 - i)) & 1;    //� ���������� ��� ������������� � ������
		}
	}

	for (int j = 0; j < len; j++) {
		cout << "|";
		for (int i = 0; i < 8; i++) {
			cout << binStr[i + 8 * j];
		}
	}
	cout << endl;
	for (int j = 0; j < shiftIn.length(); j++) {
		cout << "|";
		for (int i = 0; i < 8; i++) {
			cout << binStrShift[i + 8 * j];
		}
	}
*/

	cout << in << endl;
	for (int j = 0; j < len; j++) {
		string shiftIn = Shift(in, j);
		string sum;
		int* dec;
		dec = new int[len];
		double count = 0;
		sum.reserve(len);
		for (int i = 0; i < len; i++) {
			sum.push_back(in[i] ^ shiftIn[i]);
			dec[i] = in[i] ^ shiftIn[i];
		}
		for (int i = 0; i < len; i++) {
			if (in[i] == sum[i]) {
				count++;
			}
		}
		
		cout << "shiftIn = " << shiftIn << endl;
		cout << "sum = " << sum << endl;
		cout << "dec = ";
		for (int i = 0; i < len; i++) {
			cout << dec[i] << " ";
		}
		cout << endl << "count = " << count << endl;
		if ((count != 0)&& (count / len > 0.06)) {
			break;
		}
		free(dec);
	}

}

string Shift(const string& a, const int& i) {
	string res;
	res.reserve(a.length());
	for (int j = 0; j < i; j++) {
		res.push_back(0);
	}	
	for (int j = 0; j < a.length() - i; j++) {
		res.push_back(a[j]);
	}
	return res;
}
