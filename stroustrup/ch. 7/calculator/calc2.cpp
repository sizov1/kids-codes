#include "calc2.h"

Token curr_sum;			
queue<Token> token_queue;	
int no_of_errors = 0;
map<string, double> table;		 
map<string, Function> functions; 

Token_value get_token() { 			

	if (token_queue.size() > 0) {         //� ������ ���� ������� ������ ��������			 
		curr_sum = token_queue.front();	  //������ ���� �� ������� ���������� �������
		token_queue.pop();				  //� ����� ��������� �� ���� �������
		return curr_sum.curr_tok;		  
	}

	char ch;

	do {		// skip whitespace except '\n'
		if (!cin.get(ch)) return curr_sum.curr_tok = END;
	} while (ch != '\n' && isspace(ch));

	switch (ch) {
		case ';':
		case '\n':						//the value is recognized as the final 
			return curr_sum.curr_tok = PRINT;	//character of the expression
		case '%':
			return curr_sum.curr_tok = FUNC;
		case '*': case '/': case '+': case '-': case '(': case ')': case '=': case ',':
			return curr_sum.curr_tok = Token_value(ch);
		case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': 
		case '8': case '9': case '.':
			cin.putback(ch);
			cin >> curr_sum.number_value;
			return curr_sum.curr_tok = NUMBER;
		default:
			if (isalpha(ch)) {
				curr_sum.string_value = ch;
				while (cin.get(ch) && isalnum(ch)) {
					curr_sum.string_value.push_back(ch);
				}
				cin.putback(ch);
				return curr_sum.curr_tok = NAME;
			}
			error("bad token");
			return curr_sum.curr_tok = PRINT;
		}
}

ArgumentList arguments() {
	ArgumentList list;
	get_token();
	for ( ; ; ) {
		if (curr_sum.curr_tok == RP) {		//����� ����� ����������
			return list;
		} else if (curr_sum.curr_tok == SEP) {
			get_token();
			continue;
		} else if (curr_sum.curr_tok == PRINT || curr_sum.curr_tok == END) {
			error("')' expected");
			return list;
		} else { 							//�������� ��������� � ������� ���
			list.push_back(expr(false));    //� �������� �������������� ���������
		}
	}
}

ParamList parametrs() {
	ParamList list;
	for (; ; ) {
		get_token();			
		if (curr_sum.curr_tok == NAME) {			//���� ������� ���� - ���, �� ������� ��� � ��������� ���������
			list.push_back(curr_sum.string_value);
		} else if (curr_sum.curr_tok == SEP) {	
			continue;
		} else if (curr_sum.curr_tok == RP) {      //����� ����� ����������
			return list;
		} else if (curr_sum.curr_tok == PRINT || curr_sum.curr_tok == END) {
			error("')' expected");
			return list;
		}
	}
}

double call(string name) {
	
	if (functions.count(name) < 1) return error("undefined function"); //�������� �� ������� ���������� �������

	ArgumentList args = arguments();  //�������� ��������� ��� ������ �������
	Function func = functions[name]; 

	if (func.params.size() != args.size()) return error("incorrect number of arguments"); //�������� �� ������������ ��������� ����������

	if (func.type == FUNC_BUILTIN) return func.ptr(args[0]);  

	map<string, double> argMap;			//������� ���������� � �������� ����������, ������� ����� �������� � �������
	for (size_t i = 0; i < func.params.size(); i++) {  //���������� �������
		argMap[func.params[i]] = args[i];
	}

	for (TokenList::const_iterator i = func.tokens.begin(); i != func.tokens.end(); i++) {  //���� �� ������������������ ������, ����������� �
		Token tok = *i;																		//��������� ������ ������� 
		if (tok.curr_tok == NAME && argMap.count(tok.string_value) > 0) {		//���� ���� ��������� ������ � ��� ��� ����������� � ������ ����������
			Token val;															//������� ���������� 
			val.curr_tok = NUMBER;												//� �������� ����������� ������ ��������� �����. ���������
			val.number_value = argMap[tok.string_value];	
			token_queue.push(val);						//�������� ���������� � ������� ������
		} else {							//���� ���� �� ��� (����� ��� ����) 
			token_queue.push(*i);			//�������� ���������� � ������� ������
		}
	}

	return expr(true);
}

void create_func(string name, function_ptr fPtr) {
	Function func;
	func.params.push_back("d");
	func.type = FUNC_BUILTIN;
	func.ptr = fPtr;
	functions[name] = func;
}

double func(bool get) {
	
	if (get) get_token(); //�������� ����

	if (curr_sum.curr_tok != NAME) return error("function name expected");  //�������� �� ������� ��� �������
	string name = curr_sum.string_value; //��� ������� ����������� � ��������� ����������

	if (get_token() != LP) return error("'(' expected");  //�������� ������������ ����� ����������
	ParamList params = parametrs(); //�������� ���������� �������

	if (get_token() != ASSIGN) return error("'=' expected"); //�������� �� ������� ����������� �������

	Function funct; //������� ��������� �� �������
	get_token();  

	while (curr_sum.curr_tok != PRINT && curr_sum.curr_tok != END) {    //���������� ����������� �������, 
		funct.tokens.push_back(curr_sum);								//��� ������������������ ������
		get_token();
	}

	funct.params = params;
	funct.type = FUNC_STANDART;
	cout << "function created: " << name << endl;
	functions[name] = funct;   //��������� ��� ��������� ������� � ���������� ������ �������

	return 0;
}

double expr(bool get) {
	double left = term(get);

	for (; ; ) {
		switch (curr_sum.curr_tok) {
			case PLUS: {
				left += term(true);
				break;
			}
			case MINUS: {
				left -= term(true);
				break;
			}
			default:
				return left;
			}
	}
}

double term(bool get) {
	double left = prim(get);

	for (; ; ) {
		switch (curr_sum.curr_tok) {
			case MUL: {
				left *= prim(true);
				break;
			}
			case DIV: {
				if (double d = prim(true)) {
					left /= d;
					break;
				}
				return error("divide by 0");
			}
			default:
				return left;
			}
	}
}

double prim(bool get) {
	if (get) get_token();

	for ( ; ; ) {
		switch (curr_sum.curr_tok) {
			case FUNC:
				return func(true);
			case NUMBER: {
				double v = curr_sum.number_value;
				get_token();
				return v;
			}
			case NAME: {
				string name = curr_sum.string_value;
				if (get_token() == LP) {
					double v = call(name);
					return v;
				} else {
					double& v = table[curr_sum.string_value];
					if (get_token() == ASSIGN) v = expr(true);
					return v;
				}
			}
			case LP: {
				double e = expr(true);
				if (get_token() != RP) {
					return error("except ')'");
				}
				get_token(); //eat ')'
				return e;
			}
			default:
				return error("primary excepted");
			}
	}
}

double error(const string& s) {
	no_of_errors++;
	cerr << "error: " << s << endl;
	return 1;
}
