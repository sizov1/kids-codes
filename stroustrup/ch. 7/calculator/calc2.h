#ifndef __CALC2_H
#define __CALC2_H
#include <iostream>
#include <string>
#include <map>
#include <queue>
#include <list>
#include <cmath>
#include <vector>
using namespace std;

enum Token_value {
	NAME, NUMBER, END, FUNC = '%',
	PLUS = '+', MINUS = '-', MUL = '*', DIV = '/',
	PRINT = ';', ASSIGN = '=', LP = '(', RP = ')',
	SEP = ','
};

enum function_type {
	FUNC_STANDART,
	FUNC_BUILTIN
};

struct Token {
	Token_value curr_tok;
	double number_value;
	string string_value;
};

typedef vector<string> ParamList;		//������ ���������� �������
typedef vector<double> ArgumentList;	//������ ���������� �������
typedef vector<Token> TokenList;		//������ ������ ������� ����� ����� ����� � ����������� �������
typedef double(*function_ptr)(double x);

struct Function {
	ParamList params;
	TokenList tokens;
	function_type type;
	function_ptr ptr;
};

extern Token curr_sum;				//���������� ��������� �������� � ���� �������� � ������� �����
extern queue<Token> token_queue;	//������� ������ ����������� � parser ��� ������ �������
extern int no_of_errors;
extern map<string, double> table;		 //������� ����������
extern map<string, Function> functions; //������� �������		

Token_value get_token();	//������ �����, ����������� ������� ����

ArgumentList arguments();   //������� ���������� �� ��������� ���������� ��� ������ �������
ParamList parametrs();		//������� ���������� �� ��������� ���������� ��� ����������� �������
double call(string name);	//������� ���������� ��� ������������ �������
double func(bool get);		//������� ��� �������� ������� �������������
void create_func(string name, function_ptr fPtr);  //������� ��� �������� ������� �������������

double expr(bool get);  //��������� �������� � ���������
double term(bool get);	//��������� ��������� � �������
double prim(bool get);	//��������� ��������� ������
double error(const string& s);

#endif 
