#include "calc2.h"

int main() {

	create_func("sqrt", sqrt);		
	create_func("log", log);       
	create_func("sin", sin);

	while (cin) {
		get_token();
		if (curr_sum.curr_tok == END) break;
		if (curr_sum.curr_tok == PRINT) continue;
		cout << expr(false) << endl;
	}

	return no_of_errors;
}