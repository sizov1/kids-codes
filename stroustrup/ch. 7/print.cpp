#include <iostream>
#include <map>
#include <string>

using namespace std;

void print(int value, int base = 10);

int main() {
	print(316);
	print(316, 10);
	print(316, 16);
	print(316, 8);
	print(316, 2);
	
	return 0;
}

void print(int value, int base) {
	if (base == 10) {
		cout << value << endl;
		return;
	} 

	string out;
	string::iterator null = out.begin();
	if (base == 16) {
		map<int, char> hex;
		hex.emplace(10, 'a'); hex.emplace(11, 'b'); hex.emplace(12, 'c');
		hex.emplace(13, 'd'); hex.emplace(14, 'e'); hex.emplace(15, 'f');
		while (value >= 16) {
			int residue = value % base;
			value /= base;
			if (residue >= 10) out.insert(null, hex[residue]);
			else out.insert(null, residue + '0');
		}
	} else if ((base == 8)||(base == 2)) {
		while (value >= base) {
			int residue = value % base;
			value /= base;
			out.insert(null, residue + '0');
		}
	} 
	out.insert(null, value + '0');
	cout << out << endl;
}