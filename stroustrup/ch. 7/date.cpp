#include <iostream>
#include <string>

using namespace std;

struct Date {
	int day;
	int month;
	int year;
	string day_week;
};

Date init(int day, int month, int year);
Date read();
int get_range(Date date1, Date date2);
int get_count_days(Date date);
string get_day_week(Date date);
void print(Date date);
void add_day(Date date, int add) { date.day += add; }
void add_month(Date date, int add) { date.month += add; }
void add_year(Date date, int add) { date.year += add; }

int main() {
	Date today = init(29, 8, 2018);
	cout << get_day_week(today);
	system("pause");
	return 0;
}

Date init(int day, int month, int year) {
	Date date;
	date.day = day;
	date.month = month;
	date.year = year;
	return date;
}

Date read() {
	Date date;
	int day, month, year;
	cin >> day >> month >> year;
	date.day = day;
	date.month = month;
	date.year = year;
	return date;
}

void print(Date date) {
	cout << date.day << "."
		<< date.month << "."
		<< date.year << ".";
}

int get_range(Date date1, Date date2) {
	return (get_count_days(date1) - get_count_days(date2) > 0) ?
		get_count_days(date1) - get_count_days(date2) :
		(get_count_days(date1) - get_count_days(date2)) * 1;
}

int get_count_days(Date date) {
	int count_days = 0;
	count_days += date.year - 1;
	count_days += date.year / 4;
	if (date.month == 1) count_days += date.day - 1;
	else if (date.month == 2) count_days += (date.day + 30);
	else if (date.month == 3) count_days += (date.day + 58);
	else if (date.month == 4) count_days += (date.day + 89);
	else if (date.month == 5) count_days += (date.day + 119);
	else if (date.month == 6) count_days += (date.day + 150);
	else if (date.month == 7) count_days += (date.day + 180);
	else if (date.month == 8) count_days += (date.day + 211);
	else if (date.month == 9) count_days += (date.day + 242);
	else if (date.month == 10) count_days += (date.day + 272);
	else if (date.month == 11) count_days += (date.day + 303);
	else if (date.month == 12) count_days += (date.day + 333);
	
	return (date.year % 4 == 0) ? count_days + 1 : count_days;
}

string get_day_week(Date date) {
	Date begin = init(1, 1, 1970);
	begin.day_week = "Thursday";
	string day_week;
	int range = get_range(date, begin);
	if (range % 7 == 0) return day_week = "Thursday";
	else if (range % 7 == 1)  return day_week = "Friday";
	else if (range % 7 == 2)  return day_week = "Saturday";
	else if (range % 7 == 3)  return day_week = "Sunday";
	else if (range % 7 == 4)  return day_week = "Monday";
	else if (range % 7 == 5)  return day_week = "Tuesday";
	return day_week = "Wednesday";
}