#include <iostream>

/*
	Shell sort(Knuth, Vol3, pg84)
*/

using namespace std;

typedef int(*CFT)(const void* p, const void* q);

void ssort(void* base, size_t n, size_t sz, CFT cmp);  //shell sort
void qsort(void* base, size_t n, size_t sz, CFT cmp);  //qsort 
int cmp1(const void* p, const void* q);   
int cmp2(const void* p, const void* q);   
void print_id(void* heads, size_t n);

struct User {
	const char* name;
	const char* id;
	int dept;
};

User heads[] = { "Ritchie D.M",    "dmr",  11271,
"Sethi R.",       "ravi", 11272,
"Szymanski T.G",  "tgs",  11273,
"Schyer N.L",     "nsl",  11274 };

int main() {
	cout << "Shell sort" << endl << endl;
	cout << "Heads in order of departament number:" << endl;
	ssort(heads, 4, sizeof(User), cmp2);
	print_id(heads, 4);
	cout << endl;
	cout << "Heads in alphabetical order:" << endl;
	ssort(heads, 4, sizeof(User), cmp1);
	print_id(heads, 4);
	cout << endl << endl;

	cout << "Qsort" << endl << endl;
	cout << "Heads in alphabetical order:" << endl;
	qsort(heads, 4, sizeof(User), cmp1);
	print_id(heads, 4);
	cout << endl;
	cout << "Heads in order of departament number:" << endl;
	qsort(heads, 4, sizeof(User), cmp2);
	print_id(heads, 4);
	cout << endl;

	system("pause");
	return 0;
}

void ssort(void* base, size_t n, size_t sz, CFT cmp) {
	for (int gap = n / 2; gap > 0; gap /= 2) {
		for (int i = gap; i < n; i++) {
			for (int j = i - gap; j >= 0; j -= gap) {
				char* b = static_cast<char*>(base);		//mandatory type conversion
				char* pj = b + j * sz;					//&base[j]
				char* pig = b + (j + gap)*sz;			//&base[j + gap]

				if (cmp(pig, pj) < 0) {				    //swap base[j] and base[j + gap]
					for (int k = 0; k < sz; k++) {
						char temp = pj[k];
						pj[k] = pig[k];
						pig[k] = temp;
					}
				}
				else break;
			}
		}
	}
}

void qsort(void* base, size_t n, size_t sz, CFT cmp) {
	long i = 0, j = n - 1;
	char* b = static_cast<char*>(base);
	char* pi = b + i * sz;
	char* pj = b + j * sz;
	char* p = b + (n >> 1)*sz;
	do {
		while (cmp(pi, p) < 0) {
			pi += sz;
			i++;
		}
		while (cmp(pj, p) > 0) {
			pj -= sz;
			j--;
		}
		if (i++ <= j--) {
			for (int k = 0; k < sz; k++) {
				char temp = pj[k];
				pj[k] = pi[k];
				pi[k] = temp;
			}
		}
	} while (i <= j);

	if (j > 0) qsort(b, j, sz, cmp);
	if (n > i) qsort(b + i*sz, n - i, sz, cmp);

}

int cmp1(const void* p, const void* q) {
	return strcmp(static_cast<const User*>(p)->name,
		static_cast<const User*>(q)->name);
}

int cmp2(const void* p, const void* q) {
	return static_cast<const User*>(p)->dept -
		static_cast<const User*>(q)->dept;
}

void print_id(void* heads, size_t n) {
	const User* p = static_cast<const User*>(heads);
	for (int i = 0; i < n; i++) {
		cout << p[i].name << " "
			<< p[i].id << " "
			<< p[i].dept << endl;
	}
}