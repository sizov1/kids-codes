#include <iostream>
#include <string>
#include <map>

using namespace std;

string Encrypt(string key, string in);
void ShowPossibleMatch(string key, string match);

int main(int argc, char* argv[]) {
	string key = argv[1];
	string in = argv[2];
	string enc = Encrypt(key, in);
	cout << enc;
}

string Encrypt(string key, string in) {
	if ((key.empty()) || (in.empty())) {
		cout << "whats missing" << endl;
		return 0;
	}
	string enc;
	size_t len_key = key.length();
	size_t len_in = in.length();
	for (size_t i = 0; i < len_in; i++) {
		enc.push_back(in[i]^key[(i%len_key)]);
	}
	return enc;
}
