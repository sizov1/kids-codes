#include <iostream>

using namespace std;

void Transpose(int** matrix, int m, int n);

int main() {
	int** matrix = new int*[4];
	for (int i = 0; i < 4; i++) {
		matrix[i] = new int[5];
	}
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 5; j++) {
			matrix[i][j] = i + j;
		}
	}
	for (int i = 0; i < 4; i++) {
		cout << endl;
		for (int j = 0; j < 5; j++) {
			cout << matrix[i][j] << " ";
		}
	}

	//Transpose(matrix);

	system("pause");
	return 0;
}

void Transpose(int** matrix, int m, int n) {
	int** temp = new int*[n];
	for (int i = 0; i < n; i++) {
		temp[i] = new int[m];
	}
	while (n >= 0) {
		for (int j = 0; j < m; j++) {
			temp[n][j] = matrix[j][n];
		}
		n--;
	}
}
