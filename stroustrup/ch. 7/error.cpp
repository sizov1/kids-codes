#include <iostream>
#include <cstdarg>

void error(char* input ...) {
	va_list ap;
	va_start(ap, input);

	for (size_t i = 0; i < strlen(input); i++) {
		if (input[i] == '%') {
			if (input[i+1] == 's') {
				char* p = va_arg(ap, char*);
				cout << p;
			} else if (input[i+1] == 'd') {
				double d = va_arg(ap, double);
				cout << d;
			} else if (input[i+1] == 'c') {
				char c = va_arg(ap, char);
				cout << c;
			}
		}
		cout << input[i];
	}

	va_end(ap);
}