#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <fstream>


using namespace std;
ofstream fout("123.txt");

enum Token_value {
	NAME, OTHER, PRINT, END,
	SEP = ',',
	SPACE = ' ',
	SHARP = '#',
	LL = '_',
	LP = '(',
	RP = ')'
};

struct Token {
	char ch;
	Token_value curr_tok;
	string string_value;
};

typedef vector<string> ParamList;
typedef vector<string> ArgumList;
typedef vector<Token> TokenList;

struct Macro {
	string name;
	TokenList def;
	ParamList params;
};

Token curr;
map<string, Macro> mac_list;

Token_value get_name(char ch) {
	curr.string_value = ch;
	while (cin.get(ch) && isalnum(ch)) {
		curr.string_value.push_back(ch);
	}
	cin.putback(ch);
	return NAME;
}

Token_value get_token() {
	char ch;

	if (!cin.get(ch)) return curr.curr_tok = END;

	curr.ch = ch;

	if (ch == '\n') return curr.curr_tok = PRINT;
	else if (ch == ' ') return curr.curr_tok = SPACE;
	else if (ch == ',') return curr.curr_tok = SEP;
	else if (ch == '(') return curr.curr_tok = LP;
	else if (ch == ')') return curr.curr_tok = RP;
	else if (ch == '#') return curr.curr_tok = SHARP;
	else if (isalpha(ch) || ch == '_') return curr.curr_tok = get_name(ch);
    else return curr.curr_tok = OTHER;

}

ParamList parametrs() {
	ParamList list;
	for (; ; ) {
		get_token();
		if (curr.curr_tok == NAME) {
			list.push_back(curr.string_value);
		} else if (curr.curr_tok == SEP) {
			continue;
		} else if (curr.curr_tok == RP) {
			get_token();
			return list;
		} else if (curr.curr_tok == PRINT || curr.curr_tok == END) {
			fout << "ERROR: ) expected" << endl;
			return list;
		}
	}
}

double macr() {

	get_token();
	if (curr.curr_tok != NAME) fout << "ERROR: using #define" << endl;
	
	string name = curr.string_value;
	if (name != "define") {
		while (curr.curr_tok != PRINT) get_token();
		return 1;
	}

	get_token();
	if (curr.curr_tok != SPACE) fout << "ERROR: incorrect input: using #define <name> <def>" << endl;

	get_token();
	if (curr.curr_tok != NAME) fout << "ERROR: macro name expected" << endl;
	name = curr.string_value;

	get_token();
	ParamList param;
	if (curr.curr_tok == LP) param = parametrs();

	if (curr.curr_tok != SPACE) fout << "ERROR: definition macro expected" << endl;

	Macro mac;
	get_token();

	while (curr.curr_tok != PRINT && curr.curr_tok != END) {
		mac.def.push_back(curr);
		get_token();
	}

	mac.name = name;
	mac.params = param;
	mac_list[name] = mac;

	return 1;

}

double print(Token tok) {
	if (tok.curr_tok == NAME) fout << tok.string_value;
	else fout << tok.ch;
	return 1;
}

double print(TokenList list) {
	for (size_t i = 0; i < list.size(); i++) {
		print(list[i]);
	}
	return 1;
}

string get_arg() {
	string arg;
	while ((curr.curr_tok != SEP) && (curr.curr_tok != RP)) {
		arg.push_back(curr.ch);
		get_token();
	}
	return arg;
}

ArgumList argumets() {
	ArgumList list;
	get_token();
	for (; ; ) {
		if (curr.curr_tok == RP) {
			return list;
		} else if (curr.curr_tok == SEP) {
			get_token();
			continue;
		} else if (curr.curr_tok == PRINT || curr.curr_tok == END) {
			fout << "ERROR: ) expected" << endl;
			return list;
		} else {
			list.push_back(get_arg());
		}
	}
}

double prep(const string& name) {
	Macro mac = mac_list[name];

	if (mac.params.empty()) return print(mac.def);

	get_token();
	if (curr.curr_tok != LP) fout << "ERROR: agruments expected";

	ArgumList args = argumets();

	if (mac.params.size() != args.size()) fout << "ERROR: incorrect number of arguments" << endl;

	map<string, string> argMap;

	for (size_t i = 0; i < mac.params.size(); i++) {
		argMap[mac.params[i]] = args[i];
	}

	for (TokenList::const_iterator i = mac.def.begin(); i != mac.def.end(); i++) {
		if ((*i).curr_tok == NAME) {
			if (argMap.count((*i).string_value) >= 1) {
				fout << argMap[(*i).string_value];
			} else {
				fout << (*i).string_value;
			}
		} else {
			print(*i);
		}
	}
	
	return 1;

}


double prim() {
	if (curr.curr_tok == SHARP) {
		return macr();
	} else if (curr.curr_tok == NAME) {
		if (mac_list.count(curr.string_value) >= 1) return prep(curr.string_value);
		else print(curr);
	} else {
		return print(curr);
	}
}

int main() {
	while (cin) {
		get_token();
		prim();
	}
}