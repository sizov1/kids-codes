#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>

using namespace std;

struct Tnode {
	char* word;	 
	int count;  //����� ��������� �����
	Tnode* left;
	Tnode* right;
};

Tnode* CreateNode(Tnode* node, const char* word);
Tnode* DeleteTree(Tnode* tree);
void Print(Tnode* node);
void Sort(Tnode* node);

int main() {
	Tnode* Alph = NULL;
	Alph = CreateNode(Alph, "G");
	Alph = CreateNode(Alph, "B");
	Alph = CreateNode(Alph, "C");
	Alph = CreateNode(Alph, "Y");
	Alph = CreateNode(Alph, "F");
	Alph = CreateNode(Alph, "A");
	Alph = CreateNode(Alph, "H");
	Alph = CreateNode(Alph, "I");
	Print(Alph);
	cout << endl << endl;
	Sort(Alph);

	system("pause");
	return 0;
}

Tnode* CreateNode(Tnode* node, const char* word) {
	int cond = 0;
	if (node == NULL) {
		node = new Tnode;
		node->word = new char[strlen(word)];
		node->count = 1;
		strcpy(node->word, word);
		node->right = node->left = NULL; 
	} else if ((cond = strcmp(word, node->word)) == 0) {
		node->count++;
	} else if (cond < 0) {
		node->left = CreateNode(node->left, word);
	} else {
		node->right = CreateNode(node->right, word);
	}
	return node;
}

Tnode* DeleteTree(Tnode* tree) {
	if (tree != NULL) {
		DeleteTree(tree->left);
		DeleteTree(tree->right);
		free(tree);
	}
	return NULL;
}

void Print(Tnode* node) {
	if (node != NULL) {
		Print(node->left);
		Print(node->right);
		cout << node->word << endl;
	}
}

void Sort(Tnode* node) {
	if (node != NULL) {
		Sort(node->left);
		cout << node->word;
		Sort(node->right);
	}
}