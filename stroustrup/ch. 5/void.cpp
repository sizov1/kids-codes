#include <iostream>
using namespace std;

/*Переменной типа void* можно присвоить значение
указателя любого типа; одной переменной типа void*
можно присвоить значение другой переменной этого 
типа, а также сравнивать их на равенство (или нер-во)
*/

int main() {
    
    int* pi;
    *pi = 9;
    void* pv = pi;  //неявное преобразование из int* в void*
    
//  *pv нельзя! разыменовать void*
//  pv++ нельзя! инкрементировать void*
//  (неизвестен размер указуемого объекта)
    
    int* pi2 = static_cast<int*>(pv); //явное преобразование в int*
    
//  double* pd1 = pv;   error
//  double* pd2 = pi;   error
//  double* pd3 = static_cast<double*>(pv);   небезопасно
    
    return 0;
}
