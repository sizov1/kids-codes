#include <iostream>
#include <string.h>
using namespace std;

void StringCount(const string& str, const string& pair);
void CharCount(char* str, char* pair);

int main() {
    string str, pair;
    cin >> str;
    cin >> pair;
    StringCount(str, pair);
    
    char* cstr = "xabaacbaxabb";
    char* cpair = "ab";
    CharCount(cstr, cpair);
}

void StringCount(const string& str, const string& pair) {
    size_t count = 0;
    size_t dStr =  str.length();
    for(size_t i = 0; i < dStr - 1; i++) {
         if ((str[i] == pair[0])&&(str[i+1] == pair[1])) {
             count++;
         } 
    }
    cout << count << endl;
}

void CharCount(char* str, char* pair) {
    unsigned count = 0;
    for(unsigned i = 0; i < strlen(str) - 1; i++) {
        if ((str[i] == pair[0])&&(str[i+1] == pair[1])) {
            count++;
        }
    }
    cout << count << endl;
}