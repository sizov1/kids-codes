#include <iostream>
using namespace std;

void SwopByPointer(int* x1, int* x2);
void SwopByRef(int& x1, int &x2);

int main() {
    int a = 2, b = 3;
    cout << "a = " << a << " b = " << b << endl;
    SwopByPointer(&a, &b);
    cout << "a = " << a << " b = " << b << endl;
    
    int c = 4, d = 5;
    cout << "c = " << c << " d = " << d << endl;
    SwopByRef(c, d);
    cout << "c = " << c << " d = " << d << endl;
    
    return 0;
}

void SwopByPointer(int* x1, int* x2) {
    int temp = *x1;
    *x1 = *x2;
    *x2 = temp;
}

void SwopByRef(int& x1, int &x2) {
    int temp = x1;
    x1 = x2;
    x2 = temp;
}