#include <iostream>
#include <cstring>
#include <cmath>
using namespace std;

int atoii(const char* str);
char* itoa(int i, char b[]);
void rev(char* str);

int main() {
	const char* p = "-7031";
	int c = atoii(p);
	cout << c << endl;

	int i = -234;
	char b[] = "hello";
	char* res = itoa(i, b);
	cout << res;

	system("pause");
	return 0;
}

int atoii(const char* str) {
	if (!str) return 0;
	while (*str == ' ' || *str == '\t') str++;
	bool sign = (*str == '-');
	if (sign || *str == '+') str++;

	int res = 0;
	while (*str) {
		if ((*str < '0' || *str > '9')) break;
		res = res * 10 + (*str - '0');
		cout << res << endl;
		str++;
	}
	return (sign ? (-1)*res : res);
}

char* itoa(int i, char b[]) {
	int j = 0;
	bool sign = (i > 0);
	if (!sign) {
		b[j++] = '-';
		i *= -1;
	}
	do {
		b[j++] = '0' + (i % 10);
	} while ((i /= 10) > 0);
	*(b + j) = '\0';
	rev(b + 1);
	return b;
}

void rev(char* str) {
	char* end = str + strlen(str) - 1;
	char temp;
	while (str < end) {
		temp = *str;
		*str++ = *end;
		*end-- = temp;
	}
	str = end;
}
