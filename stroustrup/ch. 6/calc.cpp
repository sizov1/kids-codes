#include <iostream>
#include <string>
#include <map>
#include <cctype>
using namespace std;

/* ����������� ������� �� 4 �������� ������
�������������� ���������� (parser), ������� �����,
������� ��������, ����������� ��������� */

enum Token_value {
	NAME, NUMBER, END,
	PLUS = '+', MINUS = '-', MUL = '*', DIV = '/',
	PRINT = ';', ASSIGN = '=', LP = '(', RP = ')'
};

double Expr(bool get);   //������� expr() ������������ �������� � ���������
double Term(bool get);   //������� term() ������������ ��������� � �������
double Prim(bool get);   //������� prim() ������������ ��������� ��������
Token_value BadGetToken();  //������� ����� � ����� �����������
Token_value GetToken();     //������������ ������� �����
double error(const string& s);


Token_value currTok = PRINT;
double numberValue;
string stringValue;
map<string, double> table;
int noOfErrors;

int main() {
	table["pi"] = 3.1415926535897932385;
	table["e"] = 2.7182818284590452354;

	while (cin) {
		GetToken();
		if (currTok == END) {
			break;
		}
		if (currTok == PRINT) {
			continue;
		}
		cout << Expr(false) << '\n';
		return noOfErrors;
	}
}

double Expr(bool get) {
	double left = Term(get);

	for (; ; ) {
		switch (currTok) {
		case PLUS:
			left += Term(true);
			break;
		case MINUS:
			left -= Term(true);
			break;
		default:
			return left;
		}
	}
}

double Term(bool get) {
	double left = Prim(get);

	for (; ; ) {
		switch (currTok) {
		case MUL:
			left *= Prim(true);
			break;
		case DIV:
			if (double d = Prim(true)) {
				left /= d;
				break;
			}
			return error( "divide by 0" );
		default:
			return left;
		}
	}
}

double Prim(bool get) {
	if (get) {
		BadGetToken();
	}
	
	switch (currTok) {
		case NUMBER:
			double v = numberValue;
			BadGetToken();
			return v;
		case NAME:
			double& v = table[stringValue];
			if (BadGetToken() == ASSIGN) {
				v = Expr(true);
			}
			return v;
		case MINUS:						
			return -Prim(true);
		case LP:
			double e = Expr(true);
			if (currTok != RP) {
				return error("')' expected ");
			}
			BadGetToken();
			return e;
		default:
			return error("primary expected");
	}
}

Token_value BadGetToken() {
	/* ������� ����� ����������. ����� ��������� ������� ��� 
	����� � �������, ����� ����������� �� ������� ������������
	��������. � ����� ���������� ����������� ������� ����� ����,
	� ������ ��������� x=7 ��������� ��� �������������.*/

	char ch = 0;
	cin >> ch;		//��������� ������ ������ �� �������� ������

	switch (ch) {
		case 0:
			return currTok = END;
		case ';':
		case '*':
		case '/':
		case '+':
		case '-':
		case '(':
		case ')':
		case '=':
			return currTok = Token_value(ch);
		case '0': case '1': case '2': case '3': case '4':
		case '5': case '6': case '7': case '8': case '9':
		case '.':
			cin.putback(ch);
			cin >> numberValue;
			return currTok = NUMBER;
		default:
			if (isalpha(ch)) {
				cin.putback(ch);
				cin >> stringValue;
				return currTok = NAME;
			}
			error("bad token");
			return currTok = PRINT;
	}
}

Token_value GetToken() {
	char ch;
	            // �� ��������� get() �� ��������� ������������� ������� ���������� �������
	do {		// ���������� ��� ���������� ������� ����� '\n'
		if (!cin.get(ch)) {             // ����������� ����������� ������ ������ �� ������
			return currTok = END;			  
		} 
	} while (ch != '\n' && isspace(ch));   // ���� ������� ���������� ������� �� ������ ������� '\n'

	switch (ch) {
		case ';':
		case '\n':
			return currTok = PRINT;
		case '*':
		case '/':
		case '+':
		case '-':
		case '(':
		case ')':
		case '=':
			return currTok = Token_value(ch);
		case '0': case '1': case '2': case '3': case '4':
		case '5': case '6': case '7': case '8': case '9':
		case '.':
			cin.putback(ch);
			cin >> numberValue;
			return currTok = NUMBER;
		default:
			if (isalpha(ch)) {
				stringValue = ch;
				while (cin.get(ch) && isalnum(ch)) {
					stringValue.push_back(ch);
				}
				cin.putback(ch);
				return currTok = NAME;
			}
			error("bad token");
			return currTok = PRINT;
	}
}

double error(const string& s) {
	noOfErrors++;
	cerr << " error: " << s << '\n';
	return 1;
}