#include <iostream>
using namespace std;

size_t strlen(char* str);
void strcopy(char* str1, const char* str2);
int strcmp(char* str1, char* str2);
char* strcat(char* str1, char* str2);
void rev(char* str);
int atoi(char* str);


int main() {
	char str1[] = "Tory";
	char str2[] = "Lanez";

	cout << "strlen(str1) = " << strlen(str1) <<
		endl << "strlen(str2) = " << strlen(str2) << endl;

	char str3[] = "Daystar";
	char str4[] = "Peterson";

	cout << str1 << " " << str2 << endl;
	strcopy(str1, str3);
	strcopy(str2, str4);
	cout << str1 << " " << str2 << endl;

	cout << strcmp(str1, str2) << endl;

	cout << strcat(str1, str2) << endl;

	rev(str2);
	cout << str2 << endl;
}

size_t strlen(char *str) {
	size_t count = 0;
	while (*(str++)) count++;
	return count;
}

void strcopy(char* str1, const char* str2) {
	while (*str1) *str1++ = *str2++;
	*str1 = '\0';
}

int strcmp(char* str1, char* str2) {
	if (strlen(str1) > strlen(str2)) return 1;
	else if (strlen(str1) < strlen(str2)) return -1;
	else return 0;
}

char* strcat(char* str1, char* str2) {
	size_t resLen = strlen(str1) + strlen(str2) + 1;
	char* res = new char[resLen];
	while (*str1) *(res++) = *(str1++);
	while (*str2) *(res++) = *(str2++);
	*res = '\0';
	res -= resLen - 1;
	return res;
}

void rev(char* str) {
	char* end = str + strlen(str) - 1;
	char temp;
	while (str < end) {
		temp = *str;
		*str++ = *end;
		*end-- = temp;
	}
	str = end;
}


