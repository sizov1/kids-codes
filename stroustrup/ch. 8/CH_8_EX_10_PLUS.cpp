#include <iostream>
#include <string>
#include <limits>
#include <cfloat>

using namespace std;

/*
	Write plus(), minus(), multiply(), and divide() functions 
	that check for possible overfow and underflow and that 
	throw exceptions if such errrors happen
*/


double max = numeric_limits<double>::max();
double min = numeric_limits<double>::lowest();
double inf = numeric_limits<double>::infinity();

double _plus(double a, double b);
double _minus(double a, double b);
double _multiply(double a, double b);
double _divide(double a, double b);

void test_plus();
void test_minus();
void test_multiply();
void test_devide();

struct overflow {
	string s;

	overflow(string ss) { s = ss; }
};

struct divide_by_null {
	string s;

	divide_by_null() { s = "divide by null"; }
};

int main() {

	test_plus();
	test_minus();
	test_multiply();
	test_devide();

	cin.get();
}

double _plus(double a, double b) {

	if (a < min - b) throw overflow("addition failed, result < double_min ");

	if (a > max - b) throw overflow("addition failed, result > double_max");

	return a + b;
}

double _minus(double a, double b) {

	if (a < min + b) throw overflow("subtraction failed, result < double_min");

	if (a > max + b) throw overflow("subtraction failed, result > double_max");
	
	return a - b;
}

double _multiply(double a, double b) {

	if (a < min / b) throw overflow("multiplication failed, result < double_min ");

	if (a > max / b) throw overflow("multiplication failed, result > double_max");

	return a * b;
}

double _divide(double a, double b) {

	if (b == 0) throw divide_by_null();

	if (a < min * b) throw overflow("division failed, result < double_min ");

	if (a > max * b) throw overflow("division failed, result > double_max ");

	return a / b;
}

void test_plus() {
	cout << "test plus: " << endl;

	double a = min + 50;
	double b = min + 10;

	cout << "a = " << a << " b = " << b << endl;

	try {
		cout << "a + b = " << _plus(a, b) << endl;
	} catch (overflow err) {
		cout << "error: overflow double " + err.s << endl;
	}

	a = 14; b = 16;

	cout << "a = " << a << " b = " << b << endl;

	try {
		cout << "a + b = " << _plus(a, b) << endl;
	} catch (overflow err) {
		cout << "error: overflow double " + err.s << endl;
	}
}

void test_minus() {
	cout << endl << "test minus: " << endl;

	double a = 12;
	double b = (-1)*min * 10;

	cout << "a = " << a << " b = " << b << endl;

	try {
		cout << "a - b = " << _minus(a, b) << endl;
	} catch (overflow err) {
		cout << "error: overflow double " + err.s << endl;
	}

	a = 2.67849; b = 2.67845;

	cout << "a = " << a << " b = " << b << endl;

	try {
		cout << "a - b = " << _minus(a, b) << endl;
	} catch (overflow err) {
		cout << "error: overflow double " + err.s << endl;
	}
}

void test_multiply() {
	cout << endl << "test multiply: " << endl;

	double a = 10e+150;
	double b = 10e+200;

	cout << "a = " << a << " b = " << b << endl;

	try {
		cout << "a * b = " << _multiply(a, b) << endl;
	} catch (overflow err) {
		cout << "error: overflow double " + err.s << endl;
	}

	a = 12; b = 13;

	cout << "a = " << a << " b = " << b << endl;

	try {
		cout << "a * b = " << _multiply(a, b) << endl;
	} catch (overflow err) {
		cout << "error: overflow double " + err.s << endl;
	}
}

void test_devide() {
	cout << endl << "test divide: " << endl;

	double a = 10e+250;
	double b = 10e-100;

	cout << "a = " << a << " b = " << b << endl;

	try {
		cout << "a / b = " << _divide(a, b) << endl;
	} catch (overflow err) {
		cout << "error: overflow double " + err.s << endl;
	} catch (divide_by_null err) {
		cout << "error: " + err.s << endl;
	}

	a = 25; b = 5;

	cout << "a = " << a << " b = " << b << endl;

	try {
		cout << "a / b = " << _divide(a, b) << endl;
	} catch (overflow err) {
		cout << "error: overflow double " + err.s << endl;
	} catch (divide_by_null err) {
		cout << "error: " + err.s << endl;
	}
	
	b = 0;
	
	cout << "a = " << a << " b = " << b << endl;

	try {
		cout << "a / b = " << _divide(a, b) << endl;
	} catch (overflow err) {
		cout << "error: overflow double " + err.s << endl;
	} catch (divide_by_null err) {
		cout << "error: " + err.s << endl;
	}

}

