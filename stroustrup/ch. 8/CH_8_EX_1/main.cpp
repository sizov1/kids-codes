#include "dlist_interface.h"

int main() {
	MyList::dl_list* programm_langs = MyList::create();

	MyList::push_back(programm_langs, "BASIC");
	MyList::push_back(programm_langs, "FORTRAN");
	MyList::push_back(programm_langs, "C++");
	MyList::push_back(programm_langs, "C");
	MyList::push_back(programm_langs, "C#");
	MyList::push_back(programm_langs, "ADA");
	MyList::push_back(programm_langs, "COBOL");
	MyList::push_back(programm_langs, "Pascal");
	MyList::push_back(programm_langs, "LISP");
	MyList::push_back(programm_langs, "Algol");
	MyList::push_back(programm_langs, "Smalltalk");

	MyList::print(programm_langs);

	cout << endl << "test pop_back(): " << endl;
	MyList::pop_back(programm_langs);
	MyList::print(programm_langs);


	cout << endl << "test reverse: " << endl;
	MyList::reverse(programm_langs);
	MyList::print(programm_langs);

	cout << endl << "test sort: " << endl;
	MyList::sort(programm_langs->head, programm_langs->size);
	MyList::print(programm_langs);

	system("pause");
	return 0;
}