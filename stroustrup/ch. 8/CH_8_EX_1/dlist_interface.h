#ifndef  __DLIST_INTERFACE_H
#define  __DLIST_INTERFACE_H
#include <iostream>
#include <string>

using namespace std;

namespace MyList {

	struct node {
		string name;
		node* prev;
		node* next;
	};

	struct dl_list {
		size_t size;
		node* head;
		node* tail;
	};

	node* create_node(string s = "default");
	dl_list* create();

	void del(dl_list* list);

	void push_back(dl_list* list, string s);
	void pop_back(dl_list* list);

	void print(dl_list* list);

	void reverse(dl_list* list);
	void sort(node* head, size_t n);

}

#endif

