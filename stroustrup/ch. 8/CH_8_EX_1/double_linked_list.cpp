#include "dlist_interface.h"

MyList::node* MyList::create_node(string s) {
	node* nd;
	nd = new node;
	nd->name = s;
	nd->prev = NULL;
	nd->next = NULL;
	return nd;
}

MyList::dl_list* MyList::create() {
	dl_list* list;
	list = new dl_list;
	list->size = 0;
	list->head = list->tail = NULL;
	return list;
}

void MyList::del(dl_list* list) {
	node* temp = list->head;
	node* right = NULL;
	while (temp) {
		right = temp->next;
		delete temp;
		temp = right;
	}
}

void MyList::push_back(dl_list* list, string s) {
	node* add = create_node(s);
	if (!(list->size)) {
		list->head = list->tail = add;
	} else {
		list->tail->next = add;
		add->prev = list->tail;
		list->tail = add;
	}
	list->size++;
}

void MyList::pop_back(dl_list* list) {
	if (!(list->tail)) {
		del(list);
		return;
	}
	node* temp = list->tail;
	list->tail = list->tail->prev;
	list->tail->next = NULL;
	list->size--;
	delete temp;
}

void MyList::print(dl_list* list) {
	node* print = list->head;
	while (print) {
		cout << print->name << endl;
		print = print->next;
	}
}

void MyList::reverse(dl_list* list) {
	node* curr = list->head;
	node* sub = list->tail;
	size_t count = ((list->size) / 2);
	while (count--) {
		string temp = curr->name;
		curr->name = sub->name;
		sub->name = temp;
		curr = curr->next;
		sub = sub->prev;
	}

}

MyList::node* get_node(MyList::node* head, int i) {
	MyList::node* res = head;
	while (i--) res = res->next;
	return  res;
}

void MyList::sort(node* head, size_t n) {
	for (size_t i = 0; i < n; i++) {
		for (size_t j = n - 1; j > i; j--) {
			node* a = get_node(head, j - 1);
			node* b = get_node(head, j);
			if ((a->name) > (b->name)) {
				string tmp = a->name;
				a->name = b->name;
				b->name = tmp;
			}
		}
	}
}













