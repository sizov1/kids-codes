#include <iostream>

/*
	Write a program consisting of functions calling each other to a calling depth of 10. Give
	each function an argument that determines at which level an exception is thrown. Have
	main() catch these exceptions and print out which exception is caught. Don�t forget the case
	in which an exception is caught in the function that throws it.
*/

using namespace std;

int func1(int i, int& step);
int func2(int i, int& step);
int func3(int i, int& step);

struct exc_div {
	int e;
	exc_div(int ie) { e = ie; }
};

int func1(int i, int& step) {
	step++;
	if (!(i % 10)) throw exc_div(i);

	return func2(--i, step);
}

int func2(int i, int& step) {
	step++;
	if (!(i % 19)) throw exc_div(i);

	return func3(--i, step);
}

int func3(int i, int& step) {
	step++;
	if (!(i % 21)) throw exc_div(i);

	return func1(--i, step);
}

int main() {
	int i = 99, step = 0;
	try {
		if (!(i % 9)) throw exc_div(i);
		func1(i, step);
	} catch (exc_div) {
		cerr << "step = " << step;
	}

	cin.get();
}
