#include <iostream>
#include <ctime>
#include <Windows.h>

/*
	Write a function that either returns a value or that throws that value based on an argument.
	Measure the difference in runtime between the two ways.
*/

#define start QueryPerformanceCounter(&sQP)
#define stop QueryPerformanceCounter(&fQP)

using namespace std;

struct except_false {
	bool flag;
	int field;
	except_false() { field = 3; }
};

int function(int x, bool arg) {

	if (arg == false) throw except_false();

	return x;
}

int main() {
	int test;

	LARGE_INTEGER freq;
	LARGE_INTEGER sQP, fQP;

	QueryPerformanceFrequency(&freq);
	srand(GetTickCount());

	start;
	for (int i = 0; i < 10000; i++) {
		try {
			test = function(5, false);
		} catch (except_false err) {
			test = err.field;
		}
	}
	stop;
	cout << "time with error = " << (fQP.QuadPart - sQP.QuadPart) / (double)freq.QuadPart << endl;

	start;
	for (int i = 0; i < 10000; i++) {
		test = function(3, true);
	}
	stop;
	cout << "time without error = " << (fQP.QuadPart - sQP.QuadPart) / (double)freq.QuadPart << endl;

	system("pause");
	return 0;
}
