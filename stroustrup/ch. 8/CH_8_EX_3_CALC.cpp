#include <iostream>
#include <string>
#include <map>
#include <queue>
#include <list>
#include <cmath>
#include <vector>

/*
	Modify the desk calculator program into a module in the style of �2.4 using namespaces.
	Don�t use any global using directives.
	Keep a record of the mistakes you made. Suggest ways
	of avoiding such mistakes in the future.
*/

namespace ERROR {
	using std::string;
	using std::numeric_limits;

	double max = numeric_limits<double>::max();
	double min = numeric_limits<double>::lowest();
	double inf = numeric_limits<double>::infinity();
	int no_of_errors = 0;

	struct overflow {
		string s;

		overflow(string ss) { s = ss; }
	};

	struct div_by_zero {
		div_by_zero() { no_of_errors++; }
	};

	struct syntax_error {
		string msg;
		syntax_error(string p) {
			msg = p;
			no_of_errors++;
		}
	};

}

namespace LEXER {
	using ERROR::syntax_error;
	using std::cin;

	enum Token_value {
		NAME, NUMBER, END, FUNC = '%',
		PLUS = '+', MINUS = '-', MUL = '*', DIV = '/',
		PRINT = ';', ASSIGN = '=', LP = '(', RP = ')',
		SEP = ','
	};

	struct Token {
		Token_value curr_tok;
		double number_value;
		std::string string_value;
	};

	Token curr_sum;

	Token_value get_token();
}

namespace PARSER {
	using std::map;
	using std::string;
	using LEXER::curr_sum;
	using LEXER::PLUS;
	using LEXER::MINUS;
	using LEXER::MUL;
	using LEXER::DIV;
	using LEXER::get_token;
	using LEXER::ASSIGN;
	using LEXER::LP;
	using LEXER::FUNC;
	using LEXER::NAME;
	using LEXER::NUMBER;
	using LEXER::RP;
	using ERROR::div_by_zero;
	using ERROR::syntax_error;

	map<string, double> table;

	double expr(bool get);
	double term(bool get);
	double prim(bool get);
}

namespace FUNCTION {
	using std::cout;
	using std::endl;
	using std::vector;
	using std::string;
	using std::queue;
	using std::map;
	using LEXER::Token;
	using LEXER::get_token;
	using LEXER::PRINT;
	using LEXER::END;
	using LEXER::RP;
	using LEXER::SEP;
	using LEXER::NAME;
	using LEXER::NUMBER;
	using LEXER::ASSIGN;
	using LEXER::LP;
	using LEXER::curr_sum;
	using PARSER::expr;
	using ERROR::syntax_error;

	typedef vector<string> ParamList;
	typedef vector<double> ArgumentList;
	typedef vector<Token> TokenList;
	typedef double(*function_ptr)(double x);

	enum function_type {
		FUNC_STANDART,
		FUNC_BUILTIN
	};

	struct Function {
		ParamList params;
		TokenList tokens;
		function_type type;
		function_ptr ptr;
	};

	queue<Token> token_queue;
	map<string, Function> functions;

	ArgumentList arguments();
	ParamList parametrs();
	double call(string name);
	double func(bool get);
	void create_func(string name, function_ptr fPtr);

}


LEXER::Token_value LEXER::get_token() {

	if (FUNCTION::token_queue.size() > 0) {
		curr_sum = FUNCTION::token_queue.front();
		FUNCTION::token_queue.pop();
		return curr_sum.curr_tok;
	}

	char ch;

	do {
		if (!cin.get(ch)) return curr_sum.curr_tok = END;
	} while (ch != '\n' && isspace(ch));

	switch (ch) {
	case ';':
	case '\n':
		return curr_sum.curr_tok = PRINT;
	case '%':
		return curr_sum.curr_tok = FUNC;
	case '*': case '/': case '+': case '-': case '(': case ')': case '=': case ',':
		return curr_sum.curr_tok = Token_value(ch);
	case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7':
	case '8': case '9': case '.':
		cin.putback(ch);
		cin >> curr_sum.number_value;
		return curr_sum.curr_tok = NUMBER;
	default:
		if (isalpha(ch)) {
			curr_sum.string_value = ch;
			while (cin.get(ch) && isalnum(ch)) {
				curr_sum.string_value.push_back(ch);
			}
			cin.putback(ch);
			return curr_sum.curr_tok = NAME;
		}
		throw syntax_error("bad token");
		//ERROR::error("bad token");
		//return curr_sum.curr_tok = PRINT;
	}

}

double _plus(double a, double b) {

	if (a < ERROR::min - b) throw ERROR::overflow("addition failed, result < double_min ");

	if (a > ERROR::max - b) throw ERROR::overflow("addition failed, result > double_max");

	return a + b;
}

double _minus(double a, double b) {

	if (a < ERROR::min + b) throw ERROR::overflow("subtraction failed, result < double_min");

	if (a > ERROR::max + b) throw ERROR::overflow("subtraction failed, result > double_max");

	return a - b;
}

double _multiply(double a, double b) {

	if (a < ERROR::min / b) throw ERROR::overflow("multiplication failed, result < double_min ");

	if (a > ERROR::max / b) throw ERROR::overflow("multiplication failed, result > double_max");

	return a * b;
}

double _divide(double a, double b) {

	if (b == 0) throw ERROR::div_by_zero();

	if (a < ERROR::min * b) throw ERROR::overflow("division failed, result < double_min ");

	if (a > ERROR::max * b) throw ERROR::overflow("division failed, result > double_max ");

	return a / b;
}



double PARSER::expr(bool get) {
	double left = term(get);

	for (; ; ) {
		switch (curr_sum.curr_tok) {
		case PLUS: {
			left = _plus(left, term(true));
			break;
		}
		case MINUS: {
			left = _minus(left, term(true));
			break;
		}
		default:
			return left;
		}
	}
}

double PARSER::term(bool get) {
	double left = prim(get);

	for (; ; ) {
		switch (curr_sum.curr_tok) {
		case MUL: {
			left = _multiply(left, prim(true));
			break;
		}
		case DIV: {
			if (double d = prim(true)) {
				left = _divide(left, d);
				break;
			}
			throw div_by_zero();
			//return ERROR::error("divide by 0");
		}
		default:
			return left;
		}
	}
}

double PARSER::prim(bool get) {
	if (get) get_token();

	for (; ; ) {
		switch (curr_sum.curr_tok) {
		case FUNC:
			return FUNCTION::func(true);
		case NUMBER: {
			double v = curr_sum.number_value;
			get_token();
			return v;
		}
		case NAME: {
			string name = curr_sum.string_value;
			if (get_token() == LP) {
				double v = FUNCTION::call(name);
				return v;
			}
			else {
				double& v = table[curr_sum.string_value];
				if (get_token() == ASSIGN) v = expr(true);
				return v;
			}
		}
		case LP: {
			double e = expr(true);
			if (get_token() != RP) {
				throw syntax_error("except ')'");
				//return ERROR::error("except ')'");
			}
			get_token(); //eat ')'
			return e;
		}
		default:
			throw syntax_error("primary excepted");
			//return ERROR::error("primary excepted");
		}
	}
}

FUNCTION::ArgumentList FUNCTION::arguments() {
	ArgumentList list;
	get_token();
	for (; ; ) {
		if (curr_sum.curr_tok == RP) {
			return list;
		}
		else if (curr_sum.curr_tok == SEP) {
			get_token();
			continue;
		}
		else if (curr_sum.curr_tok == PRINT || curr_sum.curr_tok == END) {
			throw syntax_error("')' expected");
			//error("')' expected");
			// return list;
		}
		else {
			list.push_back(expr(false));
		}
	}
}

FUNCTION::ParamList FUNCTION::parametrs() {
	ParamList list;
	for (; ; ) {
		get_token();
		if (curr_sum.curr_tok == NAME) {
			list.push_back(curr_sum.string_value);
		}
		else if (curr_sum.curr_tok == SEP) {
			continue;
		}
		else if (curr_sum.curr_tok == RP) {
			return list;
		}
		else if (curr_sum.curr_tok == PRINT || curr_sum.curr_tok == END) {
			throw syntax_error("')' expected");
			//error("')' expected");
			//return list;
		}
	}
}

double FUNCTION::call(string name) {

	if (functions.count(name) < 1) throw syntax_error("undefined function"); //return error("undefined function");

	ArgumentList args = arguments();
	Function func = functions[name];

	if (func.params.size() != args.size()) throw syntax_error("incorrect number of arguments");// return error("incorrect number of arguments"); 

	if (func.type == FUNC_BUILTIN) return func.ptr(args[0]);

	map<string, double> argMap;
	for (size_t i = 0; i < func.params.size(); i++) {
		argMap[func.params[i]] = args[i];
	}

	for (TokenList::const_iterator i = func.tokens.begin(); i != func.tokens.end(); i++) {
		Token tok = *i;
		if (tok.curr_tok == NAME && argMap.count(tok.string_value) > 0) {
			Token val;
			val.curr_tok = NUMBER;
			val.number_value = argMap[tok.string_value];
			token_queue.push(val);
		}
		else {
			token_queue.push(*i);
		}
	}

	return expr(true);
}


void FUNCTION::create_func(string name, function_ptr fPtr) {
	Function func;
	func.params.push_back("d");
	func.type = FUNC_BUILTIN;
	func.ptr = fPtr;
	functions[name] = func;
}

double FUNCTION::func(bool get) {

	if (get) get_token();

	if (curr_sum.curr_tok != NAME) throw syntax_error("function name expected"); //return error("function name expected");  
	string name = curr_sum.string_value;

	if (get_token() != LP) throw syntax_error("'(' expected"); //return error("'(' expected");  
	ParamList params = parametrs();

	if (get_token() != ASSIGN) throw syntax_error("'=' expected"); //return error("'=' expected");

	Function funct;
	get_token();

	while (curr_sum.curr_tok != PRINT && curr_sum.curr_tok != END) {
		funct.tokens.push_back(curr_sum);
		get_token();
	}

	funct.params = params;
	funct.type = FUNC_STANDART;
	cout << "function created: " << name << endl;
	functions[name] = funct;

	return 0;
}

int main() {

	FUNCTION::create_func("sqrt", sqrt);
	FUNCTION::create_func("log", log);
	FUNCTION::create_func("sin", sin);

	while (std::cin) {
		try {
			LEXER::get_token();
			if (LEXER::curr_sum.curr_tok == LEXER::END) break;
			if (LEXER::curr_sum.curr_tok == LEXER::PRINT) continue;
			std::cout << PARSER::expr(false) << std::endl;
		}
		catch (ERROR::div_by_zero) {
			std::cout << "divide by zero" << std::endl;
		}
		catch (ERROR::syntax_error err) {
			std::cout << err.msg << std::endl;
		}
		catch (ERROR::overflow err) {
			std::cout << "error: overflow double " + err.s << std::endl;
		}
	}

	return ERROR::no_of_errors;
}