#include <iostream>

/*
	Write a program that throws an exception in one function and catches it in another.
*/

using namepsace std;

struct exception {
	string s;
	exception (string s) { s = "error";}
}

void generate_exc() {
	throw exception();
}

int main() {
	try {
		generate_exc();
	} catch (exception ex) {
		cerr << "error: " << ex.s << endl;
	}
}