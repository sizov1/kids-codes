\providecommand \autonum@processReference [2]{}
\babel@toc {russian}{}
\contentsline {section}{\numberline {1}Постановка задачи}{2}%
\contentsline {section}{\numberline {2}Анализ выпуклости целевой функции и функции ограничений}{2}%
\contentsline {section}{\numberline {3}Исследование регулярности области}{4}%
\contentsline {subsection}{\numberline {3.1}Регулярность в точках $(x,y)\in D$:$\wp (I(x,y)) = 1$}{4}%
\contentsline {subsection}{\numberline {3.2}Регулярность в точках $(x,y)\in D$:$\wp (I(x,y)) = 2$}{5}%
\contentsline {subsection}{\numberline {3.3}Регулярность в точках $(x,y)\in D$:$\wp (I(x,y)) = 3$}{7}%
\contentsline {subsection}{\numberline {3.4}Регулярность в точках $(x,y)\in D$:$I(x,y) = \emptyset $}{8}%
\contentsline {subsection}{\numberline {3.5}Итог}{8}%
\contentsline {section}{\numberline {4}Поиск минимума функции с использованием условий Каруша-Куна-Таккера}{9}%
\contentsline {subsection}{\numberline {4.1}Проверка необходимого условия локального минимума}{9}%
\contentsline {subsection}{\numberline {4.2}Анализ полученных результатов}{18}%
\contentsline {subsection}{\numberline {4.3}Итог}{20}%
\contentsline {section}{\numberline {5}Решение задачи при помощи мат.пакета}{21}%
\contentsline {section}{\numberline {6}Сравнение полученных решений}{21}%
