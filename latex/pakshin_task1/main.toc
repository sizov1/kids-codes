\providecommand \autonum@processReference [2]{}
\babel@toc {russian}{}
\contentsline {section}{\numberline {1}Способы генерации случайных и псевдослучайных чисел}{3}%
\contentsline {subsection}{\numberline {1.1}Упражнение 2.1}{3}%
\contentsline {subsection}{\numberline {1.2}Упражнение 2.2}{4}%
\contentsline {section}{\numberline {2}Способы генерации случайных чисел с экспоненциальным и нормальным распределением}{5}%
\contentsline {subsection}{\numberline {2.1}Упражнение 2.3}{6}%
\contentsline {subsection}{\numberline {2.2}Упражнение 2.4}{7}%
\contentsline {subsection}{\numberline {2.3}Упражнение 2.5}{8}%
\contentsline {section}{\numberline {3}Генерация случайных чисел с различными распределениями с помощью встроенных функций Scilab}{10}%
\contentsline {subsection}{\numberline {3.1}Упражнение 2.6}{11}%
\contentsline {section}{\numberline {4}Независимость случайных величин}{15}%
\contentsline {subsection}{\numberline {4.1}Упражнение 2.7}{16}%
\contentsline {subsection}{\numberline {4.2}Упражнение 2.8}{16}%
\contentsline {section}{\numberline {5}Случайные последовательности}{18}%
\contentsline {subsection}{\numberline {5.1}Упражнение 2.9}{18}%
