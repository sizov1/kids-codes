\providecommand \autonum@processReference [2]{}
\babel@toc {russian}{}
\contentsline {section}{\numberline {1}Марковские процессы}{3}%
\contentsline {subsection}{\numberline {1.1}Дискретные марковские цепи}{3}%
\contentsline {subsubsection}{\numberline {1.1.1}Упражнение 2.11}{3}%
\contentsline {subsubsection}{\numberline {1.1.2}Упражнение 2.12}{5}%
\contentsline {subsubsection}{\numberline {1.1.3}Упражнение 2.13}{6}%
\contentsline {subsection}{\numberline {1.2}Эргодические марковские цепи}{7}%
\contentsline {subsubsection}{\numberline {1.2.1}Упражнение 2.14}{7}%
\contentsline {subsubsection}{\numberline {1.2.2}Упражнение 2.15}{9}%
\contentsline {subsubsection}{\numberline {1.2.3}Упражнение 2.16}{10}%
\contentsline {subsubsection}{\numberline {1.2.4}Упражнение 2.17}{13}%
\contentsline {section}{\numberline {2}Диффузионные процессы}{14}%
\contentsline {subsection}{\numberline {2.1}Вычисление винеровских интегралов}{14}%
\contentsline {subsubsection}{\numberline {2.1.1}Упражнение 2.18}{14}%
\contentsline {subsubsection}{\numberline {2.1.2}Упражнение 2.19}{16}%
\contentsline {subsection}{\numberline {2.2}Приближенное построение броуновских траекторий}{18}%
\contentsline {subsubsection}{\numberline {2.2.1}Упражнение 2.20}{18}%
