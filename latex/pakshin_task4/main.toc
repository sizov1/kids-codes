\providecommand \autonum@processReference [2]{}
\babel@toc {russian}{}
\contentsline {section}{\numberline {1}Стохастическое разложение Тейлора-Ито}{3}%
\contentsline {subsection}{\numberline {1.1}Аппроксимации многократных интегралов Стратоновича}{4}%
\contentsline {subsubsection}{\numberline {1.1.1}Пример 2.5.1}{4}%
\contentsline {section}{\numberline {2}Численные методы для решения обычных дифференциальных уравнений}{10}%
\contentsline {subsection}{\numberline {2.1}Метод Эйлера}{11}%
\contentsline {subsubsection}{\numberline {2.1.1}Пример 3.1}{11}%
\contentsline {subsubsection}{\numberline {2.1.2}Пример 3.2}{14}%
\contentsline {subsection}{\numberline {2.2}Методы высших порядков}{15}%
\contentsline {subsubsection}{\numberline {2.2.1}Пример 3.3}{16}%
\contentsline {subsection}{\numberline {2.3}Другие схемы высоких порядков}{18}%
\contentsline {subsubsection}{\numberline {2.3.1}Пример 3.4}{18}%
\contentsline {subsubsection}{\numberline {2.3.2}Упражнение 3.1}{21}%
\contentsline {subsubsection}{\numberline {2.3.3}Пример 3.5}{23}%
\contentsline {subsection}{\numberline {2.4}Погрешность округления}{25}%
\contentsline {subsubsection}{\numberline {2.4.1}Пример 3.6}{26}%
\contentsline {subsubsection}{\numberline {2.4.2}Пример 3.7}{27}%
