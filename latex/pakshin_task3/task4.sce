function w = wiener_process(delta, left)
    // функция вычисляет приближенные значения
    // винеровского процесса на временном отрезке
    t = 0:delta:left;
    n = length(t);
    w = zeros(n, 1); // значения винеровского процесса
    // вычисляем значения винеровского процесса
    w(1) = grand(1, 1, 'nor', 0, 1)
    for k = 2:n
        w(k) = w(k - 1) + grand(1, 1, 'nor', 0, 1);
    end
endfunction

function ww = multi_wiener_process(p, delta, left)
    // функция вычисляет приближенные значения p-мерного
    // винервского процесса на временном отрезке
    t = 0:delta:left;
    n = length(t);
    ww = zeros(n, p);  // значения p-мерного винеровского процесса
                       // i - ый столбец соответсвует значениям
                       // винеровского процесса W^{i}_{delta}
    // вычисляем значения p-мерного винеровского процесса
    for k = 1:p
        ww(:, k) = wiener_process(delta, left)
    end
endfunction

function res = get_rhop(p)
    // функция вычисляет коэффициент rho_p
    res = 0.0;
    for r = 1:p
        res = res + 1 / (r * r);
    end
    res = 1 / 12 - res / (2 * %pi * %pi)
endfunction

function res = get_alphap(p)
    // функция вычисляет коэффициент alpha_p
    res = 0;
    for r = 1:p
        res = res + 1 / (r^4);
    end
    res = -res / (2 * %pi * %pi);
    res = res + %pi * %pi / 180;
endfunction

function res = get_aj0(delta, p)
    // функция вычисляет коэффициент a_{j, 0}
    res = 0;
    rho = get_rhop(p)
    mu = grand(1, 1, 'nor', 0, 1);
    for r = 1:p
        zeta = grand(1, 1, 'nor', 0, 1);
        res = res + zeta / r;
    end
    res = -res * sqrt(2 * delta) / %pi;
    res = res - 2 * sqrt(delta * rho) * mu;
endfunction

function res = get_bj0(delta, p)
    // функция вычисляет коэффициент b_{j, 0}
    res = 0;
    alpha = get_alphap(p);
    phi = grand(1, 1, 'nor', 0, 1);
    for r = 1:p
        eta = grand(1, 1, 'nor', 0, 1);
        res = res + eta / (r * r);
    end
    res = sqrt(delta / 2) * res;
    res = res + sqrt(delta * alpha) * phi;
endfunction

function res = get_Aj1j2(p)
    // функция вычисляет коэффициент A^{p}_{j1, j2}
    res = 0;
    for r = 1:p
        zeta_j1 = grand(1, 1, 'nor', 0, 1);
        zeta_j2 = grand(1, 1, 'nor', 0, 1);
        eta_j1 = grand(1, 1, 'nor', 0, 1);
        eta_j2 = grand(1, 1, 'nor', 0, 1);
        res = res + (zeta_j2 * eta_j1 - zeta_j1 * eta_j2) / r;
    end
    res = res / (2 * %pi);
endfunction

function res = get_Bj1j2(p)
    res = 0;
    for r = 1:p
        zeta_j1 = grand(1, 1, 'nor', 0, 1);
        zeta_j2 = grand(1, 1, 'nor', 0, 1);
        eta_j1 = grand(1, 1, 'nor', 0, 1);
        eta_j2 = grand(1, 1, 'nor', 0, 1);
        res = res + (zeta_j2 * zeta_j1 + eta_j1 * eta_j2) / (r * r);
    end
    res = res / (4 * %pi * %pi);
endfunction

function res = get_Cj1j2(p)
    res = 0;
    for r = 1:p
        for l = 1:p
            if l ~= r
                xi_j1 = grand(1, 1, 'nor', 0, 1);
                xi_j2 = grand(1, 1, 'nor', 0, 1);
                eta_j1 = grand(1, 1, 'nor', 0, 1);
                eta_j2 = grand(1, 1, 'nor', 0, 1);
                res = res + r * (xi_j1 * xi_j2 / l ...
                      - eta_j1 * eta_j2 * l / r) ...
                      / (r^2 - l^2);
            end
        end
    end
    res = - res / (2 * %pi * %pi);
endfunction

function res = Jj1(j1, p, delta, left)
    // используется формула Jj1p = W^{1}_{delta}
    t = 0:delta:left;   // массив точек на временном отрезке
    n = length(t);      // кол-во точек на временном отрезке
    res = zeros(n, 1); // значения аппроксимации траектории
                        // интеграла Стратоновича на отрезке
    ww = multi_wiener_process(p, delta, left);
    // вычисляем значения аппроксимации
    for k = 1:n
        res(k, 1) = ww(k, j1);
    end
endfunction

function res = J0j1(j1, p, delta, left)
    // используется формула Jj1p = delta (W^{1}_{delta} - a10) / 2
    t = 0:delta:left;   // массив точек на временном отрезке
    n = length(t);      // кол-во точек на временном отрезке
    res = zeros(n, 1);
    ww = multi_wiener_process(p, delta, left);
    aj0 = get_aj0(delta, p);
    for k = 1:n
        res(k, 1) = delta * (ww(k, p) - aj0) / 2;
    end
endfunction

function res = Jj10(j1, p, delta, left)
    // используется формула Jj1p = delta (W^{1}_{delta} + a10) / 2
    t = 0:delta:left;   // массив точек на временном отрезке
    n = length(t);      // кол-во точек на временном отрезке
    res = zeros(n, 1);
    ww = multi_wiener_process(p, delta, left);
    aj0 = get_aj0(delta, p);
    for k = 1:n
        res(k, 1) = delta * (ww(k, p) + aj0) / 2;
     end
endfunction

function res = Jj1j2(j1, j2, p, delta, left)
    // используется формула:
    // Jj1j2p = W^{j1}_{delta} W^{j2}_{delta} / 2 -
    // - (aj2 W^{j1}_{delta} - aj1 W^{j2}_{delta}) / 2 +
    // + delta Aj1j2
    t = 0:delta:left;   // массив точек на временном отрезке
    n = length(t);      // кол-во точек на временном отрезке
    res = zeros(n, 1);
    ww = multi_wiener_process(p, delta, left);
    Aj1j2 = get_Aj1j2(p);
    aj1 = get_aj0(delta, p);
    aj2 = get_aj0(delta, p);
    for k = 1:n
        res(k, 1) = ww(k, j1) * ww(k, j1) / 2 ...
            - (aj2 * ww(k, j1) - aj1 * ww(k, j2)) / 2 ...
            + delta * Aj1j2;
    end
endfunction

function res = J0j1j2(j1, j2, p, delta, left)
    // используется формула:
    // J0j1j2p = delta W^{j1}_{delta} W^{j2}_{delta} / 6 -
    // - delta bj1 W^{j1}_{delta} / pi +
    // + delta^2 Bj1j2 - delta aj20 W^{j1}_{delta} / 4 +
    // + delta W^{j1}_{delta} bj2 / (2 pi) +
    // + delta^2 C_{j1 j2} + delta^2 A_{j1, j2} / 2
    t = 0:delta:left;   // массив точек на временном отрезке
    n = length(t);      // кол-во точек на временном отрезке
    res = zeros(n, 1);
    ww = multi_wiener_process(p, delta, left);
    bj1 = get_bj0(delta, p); bj2 = get_bj0(delta, p);
    aj2 = get_aj0(delta, p);
    Bj1j2 = get_Bj1j2(p);
    Aj1j2 = get_Aj1j2(p);
    Cj1j2 = get_Cj1j2(p);
    for k = 1:n
       res(k, 1) = delta * ww(k, j1) * ww(k, j2) / 6 ...
                   - delta * ww(k, j2) * bj1 / %pi ...
                   + delta^2 * Bj1j2 ...
                   - delta * aj2 * ww(k, j1) / 4 ...
                   + delta * bj2 * ww(k, j1) / (2 * %pi) ...
                   + delta * delta * Cj1j2 ...
                   + delta^2 * Aj1j2 / 2;
    end
endfunction

function res = Jj10j2(j1, j2, p, delta, left)
    // используется формула:
    // Jj10j2 = delta W^{j1}_{delta} W^{j2}_{delta} / 6 +
    // + aj1 J0j2p / 2 + delta bj1 W^{j2}_{delta} / (2 pi) -
    // - delta^2 Bj1j2 - delta aj2 W^{j1}_{delta} / 4 + 
    // + delta bj2 W^{j1}_{delta} / (2 pi)
    t = 0:delta:left;   // массив точек на временном отрезке
    n = length(t);      // кол-во точек на временном отрезке
    res = zeros(n, 1);
    ww = multi_wiener_process(p, delta, left);
    j0j2 = J0j1(j2, p, delta, 1.0);
    bj1 = get_bj0(delta, p); bj2 = get_bj0(delta, p);
    aj1 = get_aj0(delta, p); aj2 = get_aj0(delta, p);
    Bj1j2 = get_Bj1j2(p);
    for k = 1:n
        res(k, 1) = delta * ww(k, j1) * ww(k, j2) / 6 ...
                    + aj1 * j0j2(k, 1) / 2 ...
                    + bj1 * delta * ww(k, j2) / (2 * %pi) ...
                    - aj2 * delta * ww(k, j1) / 4 ...
                    + bj2 * delta * ww(k, j1) / (2 * %pi) ...
                    - delta * delta * Bj1j2;
    end
endfunction

function res = Jj1j20(j1, j2, p, delta, left)
    // используется формула:
    // Jj1j20 = delta W^{j1}_{delta} W^{j2}_{delta} / 2 -
    // - delta (aj2 W^{j1}_{delta} - aj1 W^{j2}_{delta}) / 2 +
    // + delta^2 A_{j1,j2} - J_{j1, 0, j2} - J_{0, j1, j2}
    t = 0:delta:left;   // массив точек на временном отрезке
    n = length(t);      // кол-во точек на временном отрезке
    res = zeros(n, 1);
    ww = multi_wiener_process(p, delta, left);
    jj10j2 = Jj10j2(j1, j2, p, delta, 1.0);
    j0j1j2 = J0j1j2(j1, j2, p, delta, 1.0);
    Aj1j2 = get_Aj1j2(p)
    aj1 = get_aj0(delta, p); aj2 = get_aj0(delta, p);
    for k = 1:n
        res(k, 1) = delta * ww(k, j1) * ww(k, j2) / 2 ...
                    - delta * (aj2 * ww(k, j1) ...
                               - aj1 * ww(k, j2)) / 2 ...
                    + delta * delta * Aj1j2 - ...
                    - jj10j2(k, 1) - j0j1j2(k, 1);
    end
endfunction

delta = 2^(-9);
p = 5;
j1 = Jj1(1, p, delta, 1.0);
j01 = J0j1(1, p, delta, 1.0);
j10 = Jj10(1, p, delta, 1.0);
j11 = Jj1j2(1, 1, p, delta, 1.0);
j011 = J0j1j2(1, 1, p, delta, 1.0);
j101 = Jj10j2(1, 1, p, delta, 1.0);
j110 = Jj1j20(1, 1, p, delta, 1.0);
disp(j110)
