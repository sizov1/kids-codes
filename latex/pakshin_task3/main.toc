\providecommand \autonum@processReference [2]{}
\babel@toc {russian}{}
\contentsline {section}{\numberline {1}Моделирование стохастических интегралов}{3}%
\contentsline {section}{\numberline {2}Аппроксимация стохастического интеграла суммами Ито}{4}%
\contentsline {section}{\numberline {3}Решение линейного стохастического дифференциального уравнения}{5}%
\contentsline {section}{\numberline {4}Процесс Орнштейна-Уленбека}{7}%
\contentsline {section}{\numberline {5}Упражнение 2.21}{7}%
\contentsline {section}{\numberline {6}Упражнение 2.22}{9}%
\contentsline {section}{\numberline {7}Упражнение 2.23}{12}%
