from numpy.lib.shape_base import array_split
from base_objects import *
from intense_cases import *
from itertools import *
from matrix_algebra import *
from spliting_number import split_number
from output_module import *
import numpy as np
import scipy.linalg

class Model:
    def __init__(self, sp, dp):
        self.layers_size = []
        self.c, self.s, self.r = sp.c, sp.c + 1, sp.r
        self.nhomo = self.s-self.c
        self.lymbda, self.alpha, self.mu = dp.lymbda, dp.alpha, dp.mu
        self.generate_states()
        self.number_of_states = len(self.states)
        self.Nmatrix()
        self.Lmatrix()
        self.Mmatrix()
        self.Rmatrix()
        self.stationary_distribution()
        self.calculate_effecient()
        print('Checking...')
        self.check_layers_size()
        self.check_R_is_root()
        self.check_get_layer()
        print('All checks passed')

    def generate_unsorted_states(self, n):
        '''
        Генерируем полный неупорядоченный
        набор состояний системы, по след. принципу:
        -> итерируемся по количеству требований
           j от 1 до c + 1
           так как при j > c + 1 слои перестают стуртурно изменяться
        -> на каждой итерации ищем:
           сколькими способами мы можем разбить число j на r + 1-но
           слагаемое.
           Такой принцип связан с тем, что у нас такая структура
           состояний: 1 компонента выделена под кол-во требований j
                      r компонент выделено под фазы разогрева
                      1 компонента выделена под число активных серверов
           и число требований распределено между r + 1 компонентой фаз и
           активных серверов.
        '''
        layers = []
        for j in range(1, n + 1):
            layer = []
            splits = split_number(j, self.r)
            for split in splits:
                if len(split) < self.r + 1:
                    addzero = [0 for i in range (self.r + 1 - len(split))]
                    split += addzero
                for state in set([i for i in permutations(split)]):
                    layer.append(list(state))
            layers.append(layer)
            self.layers_size.append(len(layer))
        return layers

    def getN(self, i):
        if (i > self.c):
            return self.N[-1]
        else:
            return self.N[i]

    def getL(self, i):
        if (i > self.c):
            return self.L[-1]
        else:
            return self.L[i]

    def getM(self, i):
        if (i > self.c + 1):
            return self.M[-1]
        else:
            return self.M[i]

    def check_layers_size(self):
        for j in range(self.c + 1):
            val = len([st for st in self.states if st[1] == j])
            assert  val == self.layers_size[j], 'j=%s;layers_size[j]=%s; val=%s' % (j, self.layers_size[j], val)

    def check_R_is_root(self):
        actual = self.L[-1] + self.R.dot(self.N[-1]) \
            + ((self.R).dot(self.R)).dot(self.M[-1])
        excepted = np.zeros(self.L[-1].shape)
        assert np.isclose(actual, excepted, rtol=1e-05).all()

    def check_get_layer(self):
        number_of_layers = len(self.layers_size)
        for i in range(number_of_layers):
            a = [st for st in self.states if st[1] == i]
            b = self.get_layer(i)
            assert (a == b).all()

    def generate_states(self):
        n = min(self.c, self.s)
        layers = self.generate_unsorted_states(n)
        layers = [sorted(x)[::-1] for x in layers]
        liststates = []
        liststates.append([0 for i in range(self.r + 2)])
        for layer in layers:
            for state in layer:
                liststates.append([state[-1], sum(state)] + state[:-1])
        if n == self.c:
            self.layers_size.append(len(layers[-1]))
            for state in layers[-1]:
                liststates.append([state[-1], self.c + 1] + state[:-1])
        self.states = np.array(liststates)
        self.layers_size = [1] + self.layers_size

    def get_layer(self, i):
        return self.states[sum(self.layers_size[0:i]):sum(self.layers_size[0:(i+1)])]

    def get_state(self, i):
        if i > self.states.shape[0] - 1:
            v = i - (self.states.shape[0] - 1)
            tmp = self.get_layer(self.c)[v % self.layers_size[-1] - 1].copy()
            tmp[1] = self.c + v // self.layers_size[-1] + 2
            return tmp
        else:
            return self.states[i]

    def get_indexes_pi(self, i):
        if i > self.c + 1:
            a = sum(self.layers_size) + self.layers_size[-1] * (i - self.c - 2)
            b = sum(self.layers_size) + self.layers_size[-1] * (i - self.c - 1)
            return a, b
        return sum(self.layers_size[0:i]), sum(self.layers_size[0:(i+1)])

    def Nmatrix(self):
        self.N = []
        for i, ni in enumerate(self.layers_size):
            Ni = np.zeros((ni, ni))
            statesi = self.get_layer(i)
            for j in range(ni):
                for k in range(ni):
                    stj, stk = statesi[j], statesi[k]
                    if j == k:
                        Ni[j][j] = -self.lymbda \
                            - np.sum(stj[2:]) * self.alpha \
                            - stj[0] * self.mu
                    elif j < k:
                        Ni[j][k] = self.alpha * is_new_phase(stj, stk)
            self.N.append(Ni)

    def Lmatrix(self):
        self.L = []
        for i, ni in enumerate(self.layers_size[:-1]):
            Li = np.zeros((ni, self.layers_size[i + 1]))
            Li[0:ni,0:ni] = np.eye(ni) * self.lymbda
            self.L.append(Li)

    def Mmatrix(self):
        self.M = []
        self.M.append(np.zeros((1,1)))
        for i in range(1, len(self.layers_size)):
            Mi = np.zeros((self.layers_size[i], self.layers_size[i - 1]))
            states1, states2 = self.get_layer(i - 1), self.get_layer(i)
            for j in range(self.layers_size[i]):
                for k in range(self.layers_size[i - 1]):
                    stj, stk = states2[j], states1[k]
                    Mi[j][k] = is_end_serve(stj, stk, self.c) * self.mu
            self.M.append(Mi)

    def Rmatrix(self):
        self.R = quadratic_matrix_eq(self.L[-1],
                                     self.N[-1], self.M[-1], 1000)

    def Qmatrix(self):
        """function create finite part of intensity matrix"""
        N = np.sum(self.layers_size[:-1])
        Q = np.zeros((N,N))
        for i in range(self.c + 1):
            a, b = self.get_indexes_pi(i)
            Q[a:b,a:b] = self.N[i]
            if i != 0:
                c, d = self.get_indexes_pi(i - 1)
                Q[a:b, c:d] = self.M[i]
            if i != self.c:
                c, d = self.get_indexes_pi(i + 1)
                Q[a:b, c:d] = self.L[i]
        a, b = self.get_indexes_pi(self.c)
        c, d = self.get_indexes_pi(self.c + 1)
        Q[a:b,a:b] = self.N[self.c] + self.R.dot(self.M[self.c+1])
        write_Q_in_file(Q, self.states)
        return Q

    def foo(self):
        Rs = []
        Rs.append(self.R)
        for i in range(self.c, 1, -1):
            Ls = self.L[i - 1]
            Ns = self.N[i]
            Ms = self.M[i + 1]
            E = np.eye(Ns.shape[0])
            newRs = Ls.dot(np.linalg.inv(E - Ns - Rs[-1].dot(Ms)))
            Rs.append(newRs)
        print(self.M[1])
        print(self.N[1] + Rs[-1].dot(self.M[2]))

    def solution_exists(self):
        A = self.N[-1] + self.M[-1] + self.L[-1]
        f = scipy.linalg.null_space(np.transpose(A))
        f = np.transpose(f)
        f = f / np.sum(f)
        return np.sum(np.dot(f, self.L[-1])) < np.sum(f.dot(self.M[-1]))

    def stationary_distribution(self):
        Q = self.Qmatrix()

        # check the existence of a solution
        if not np.linalg.matrix_rank(Q) < Q.shape[0]:
            self.pi = np.zeros((1,1))
            return

        # solving finite system of linear equations
        res = scipy.linalg.null_space(np.transpose(Q))
        res = np.transpose(res)

        # normalize finite solution
        maxlayer = self.c + 100
        a, b = self.get_indexes_pi(self.c)
        # V = E + R + R^2 + ... + R^(maxlayer - c)
        V = sum_matrix_geom_progression(self.R, maxlayer - self.c)
        normalize_vector = np.sum(V, axis=1)
        normalize_vector = np.transpose(normalize_vector)
        res = res / (np.dot(res[0:1,a:b], normalize_vector)
                    + np.sum(res[0:1, 0:a]))

        # adding to our finite solution homogeneous components
        # and construct final result
        solution_dimension = res.shape[1] + \
            (maxlayer - self.c) * self.layers_size[self.c]
        solution = np.zeros((1, solution_dimension))
        solution[0:1, 0:b] = res
        for i in range(self.c + 1, maxlayer + 1):
            lprev, rprev = self.get_indexes_pi(i - 1)
            lcurr, rcurr = self.get_indexes_pi(i)
            solution[0:1, lcurr:rcurr] = solution[0:1, lprev:rprev].dot(self.R)

        self.pi = solution

    def calculate_effecient(self):
        self.EX = 0.0  # mean numbers of active servers
        self.EY = 0.0  # mean numbers of servers in setup mode
        self.EZ = 0.0  # mean queue length

        for i, p in enumerate(self.pi[0]):
            self.EX += float(self.get_state(i)[0]) * p
            self.EY += float(np.sum(self.get_state(i)[2:])) * p
            self.EZ += float(self.get_state(i)[1]) * p

