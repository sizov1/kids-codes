from collections import namedtuple

sys_parameters = namedtuple("sys_parameters", ["c", "s", "r"])
distr_parameters = namedtuple("distr_parameters", ["lymbda", "alpha", "mu"])