import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import on_idle_system as ois

filename1 = r'.\results\15_2_rho_0.1_1.0.csv'
filename2 = r'.\results\15_2_rho_1.0_1.0.csv'
filename3 = r'.\results\15_2_rho_10.0_1.0.csv'
ro1 = 0.7
r = 2
a1, a2, a3 = 0.1, 1.0, 10.0
c = 15

data1 = np.genfromtxt(filename1, delimiter=';')
data2 = np.genfromtxt(filename2, delimiter=';')
data3 = np.genfromtxt(filename3, delimiter=';')
on_idle1 = np.ones(data1.shape)
for i in range(1, data1.shape[0]):
    on_idle1[i, 0] = data1[i, 0]
    on_idle1[i, 1] = on_idle1[i, 1] * ois.power_consumption(c, on_idle1[i, 0])
    on_idle1[i, 2] = on_idle1[i, 2] * ois.mean_active_servers(c, on_idle1[i, 0])
    on_idle1[i, 4] = on_idle1[i, 4] * ois.mean_calls(c, on_idle1[i, 0])

sizes = np.ones(data1[:, 0].shape) * 12
fig, ax = plt.subplots()

col = 4

ax.scatter(data1[:, 0], data1[:, col], sizes, marker='s', color='blue')
ax.scatter(data2[:, 0], data2[:, col], sizes, marker='s', color='red')
ax.scatter(data3[:, 0], data3[:, col], sizes, marker='s', color='orange')
ax.scatter(data1[:, 0], on_idle1[:, col], sizes, marker='s', color='black')

plt.rc('font', family='serif')

leg_titles = [
    'α = ' + str(a1),
    'α = ' + str(a2),
    'α = ' + str(a3),
    'Cистема без выключений ',
]

y_labels = [
    '',
    'Среднее энергопотребление',
    '',
    '',
    'Cреднее число требований в системе'
]

plt.legend(leg_titles, fontsize=10, loc='upper left')
ax.set_xlabel('ρ', fontsize=12)
ax.set_ylabel(y_labels[col], fontsize=12)
plt.grid()
plt.show()