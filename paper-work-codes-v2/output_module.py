import csv

def get_filename(parameters, variative=None):
    filename = ""
    sp = {k: str(v) for k, v in parameters.items()}
    if variative == "c":
        filename = "_".join(["c", sp["r"], sp["rho"], sp["a"], sp["m"]])
    elif variative == "r":
        filename = "_".join([sp["c"], "r", sp["rho"], sp["a"], sp["m"]])
    elif variative == "rho":
        filename = "_".join([sp["c"], sp["r"], "rho", sp["a"], sp["m"]])
    elif variative == "a":
        filename = "_".join([sp["c"], sp["r"], sp["rho"], "a", sp["m"]])
    elif variative == "m":
        filename = "_".join([sp["c"], sp["r"], sp["rho"], sp["a"], "m"])
    return ".\\results\\" + filename + ".csv"

def state_name(state):
    row = ""
    for i in state:
        row += str(i)
    return row

def str_row_from_array(arr, deli):
    str_row = ""
    for value in arr:
        str_row += str(value) + deli
    return str_row

def header(states, deli):
    n_states = len(states)
    header_row = deli
    for i in range(n_states):
        header_row += state_name(states[i])
        header_row += deli
    return header_row

def write_Q_in_file(Q, states, deli=';'):
    with open('mt.csv', 'w', newline='') as csvfile:
        table = csv.writer(csvfile, delimiter=deli, quotechar=' ', quoting=csv.QUOTE_MINIMAL)
        header_row = header(states, deli)
        table.writerow([header_row])
        for i, row in enumerate(Q):
            str_row = state_name(states[i]) + deli
            str_row += str_row_from_array(row, deli)
            table.writerow([str_row[:-1]])

def write_matrix_in_file(mt, name, deli=';'):
    with open(name, 'w', newline='') as csvfile:
        table = csv.writer(csvfile, delimiter=deli, quotechar=' ', quoting=csv.QUOTE_MINIMAL)
        for row in mt:
            str_row = str_row_from_array(row, deli)
            table.writerow([str_row[:-1]])
