import numpy as np

def only_one_operation(a, b):
    d = a - b
    if np.count_nonzero(d) > 2:
        return False
    if np.count_nonzero(d) == 2:
        inzeod = np.nonzero(d)[0]
        if abs(inzeod[1]-inzeod[0]) > 1:
            if (inzeod[0] != 0 and inzeod[1] != 0):
                return False
    if not np.all(np.abs(d) <= 1):
        return False
    return True

def is_new_request(state_i, state_j):
    sb = state_j - state_i

    if sb[1] == 1:
        if sb[2] == 1:
            return True
        if np.count_nonzero(sb) == 1:
            return True

def is_end_serve(state_i, state_j, number_of_servers):
    i2, i1 = state_j[0], state_i[0]
    j2, j1 = state_j[1], state_i[1]
    y2, y1 = state_j[2:], state_i[2:]

    if i1 == 0:
        return 0

    if j2 != j1 - 1:
        return 0

    if np.all(y1 == 0):
        if not np.all(y2 == 0):
            return 0
        if i2 == i1 - 1 and j2 < number_of_servers:
            return i1
        if i2 == i1 and j2 >= number_of_servers:
            return i1

    if i2 != i1:
        return 0

    if j1 <= number_of_servers:
        k = np.nonzero(y1)[0]
        k1 = k[0]
        if y2[k1] == y1[k1] - 1 and all_equal_expected_i(y1, y2, k1):
            return i1
        else:
            return 0

    if j1 > number_of_servers:
        if np.all(y2 == y1):
            return i1
        else:
            return 0
    return 0

def all_equal_expected_i(a, b, i):
    if np.size(a) != np.size(b):
        return False
    for j in range(np.size(a)):
        if a[j] != b[j] and j != i:
            return False
    return True

def is_new_phase(state_i, state_j):
    i2, i1 = state_j[0], state_i[0]
    j2, j1 = state_j[1], state_i[1]
    y2, y1 = state_j[2:], state_i[2:]

    if j2 != j1:
        return 0

    if y2[-1] == y1[-1] - 1:
        if np.all(y2[:-1] - y1[:-1] == 0) and i2 == i1 + 1:
            return y1[-1]

    if i2 != i1:
        return 0

    k = np.nonzero(y2 - y1)[0]

    if np.size(k) != 2:
        return 0

    if k[1] - k[0] != 1:
        return 0 # переход внутри слоя разрешен только на соседнюю фазу

    if (y2[k[1]] - y1[k[1]] != 1) or (y1[k[0]] - y2[k[0]] != 1):
        return 0

    return y1[k[0]]
