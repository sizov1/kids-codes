from model import Model
from base_objects import sys_parameters, distr_parameters
from output_module import *
import numpy as np

sp = sys_parameters(3, 4, 2)
dp = distr_parameters(4.5, 1.0, 2.0)
m = Model(sp, dp)

m.foo()
print(m.pi)