from model import Model
from base_objects import sys_parameters, distr_parameters
from output_module import write_matrix_in_file, get_filename
import matplotlib.pyplot as plt
import time
import numpy as np
import os
import csv

mu = 1.0
Ca = 1
Cm = 1
rho = 0.7
lyamda, alpha, mu = 1.0, 2.0, 1.0
c, r = 5, 2

parameters = {"c": c, "r": r, "a": alpha, "l": lyamda, "m": mu, "rho": rho}

variative = "rho"
if variative == "a":
    N = 100
    alphas = np.linspace(0.01, 1, N)
    result_table = np.ones((N, 5))
    for i, alpha in enumerate(alphas):
        # run numerical experiment
        t = time.time()
        processes_parameters = distr_parameters(lyamda, alpha, mu)
        system_parameters = sys_parameters(c, c + 1, r)
        model = Model(system_parameters, processes_parameters)
        iteration_time = time.time() - t

        # write results of experiment in the table
        result_table[i, 0] = alpha
        result_table[i, 1] = Ca * model.EX + Cm * model.EY
        result_table[i, 2] = model.EX
        result_table[i, 3] = model.EY
        result_table[i, 4] = model.EZ

        os.system('cls')
        print("Completed {:.2f} %".format(((i + 1) / N) * 100))
        print("Last iteration time {:.2f} sec.".format(iteration_time))
        print("{:.2f} min left".format(iteration_time * (N - i + 1) / 60.0))
elif variative == "c":
    N = 20
    cs = range(1, N + 1, 1)
    result_table = np.ones((N, 5))
    for i, c in enumerate(cs):
        # run numerical experiment
        lyamda = rho * mu * c
        t = time.time()
        processes_parameters = distr_parameters(lyamda, alpha, mu)
        system_parameters = sys_parameters(c, c + 1, r)
        model = Model(system_parameters, processes_parameters)
        iteration_time = time.time() - t

        # write results of experiment in the table
        result_table[i, 0] = c
        result_table[i, 1] = Ca * model.EX + Cm * model.EY
        result_table[i, 2] = model.EX
        result_table[i, 3] = model.EY
        result_table[i, 4] = model.EZ

        os.system('cls')
        print("Current c value = ", c)
        print("Completed {:.2f} %".format(((i + 1) / N) * 100))
        print("Last iteration time {:.2f} sec.".format(iteration_time))
        print("{:.2f} min left".format(iteration_time * (N - i + 1) / 60.0))
elif variative == "rho":
    N = 100
    rhos = np.linspace(0.7, 0.99, N)
    result_table = np.ones((N, 5))
    for i, rho in enumerate(rhos):
        lyamda = rho * mu * c
        # run numerical experiment
        t = time.time()
        processes_parameters = distr_parameters(lyamda, alpha, mu)
        system_parameters = sys_parameters(c, c + 1, r)
        model = Model(system_parameters, processes_parameters)
        iteration_time = time.time() - t

        # write results of experiment in the table
        result_table[i, 0] = rho
        result_table[i, 1] = Ca * model.EX + Cm * model.EY
        result_table[i, 2] = model.EX
        result_table[i, 3] = model.EY
        result_table[i, 4] = model.EZ

        os.system('cls')
        print("Completed {:.2f} %".format(((i + 1) / N) * 100))
        print("Last iteration time {:.2f} sec.".format(iteration_time))
        print("{:.2f} min left".format(iteration_time * (N - i + 1) / 60.0))
        print("Solution exists: ", model.solution_exists())

write_matrix_in_file(result_table, get_filename(parameters, variative))
fig, ax = plt.subplots()
ax.scatter(result_table[:, 0], result_table[:, 1])
plt.grid(True)
plt.show()

"""
result_table has following structure:
1st column: values of variative parameter
2nd column: values of mean power consumption
3rd column: values of mean number active servers
4th column: values of mean number servers in setup mode
5th column: values of mean customers in system
"""