from math import factorial
from math import trunc

def power_consumption(c, ro, mu=1.0):
    return c * ro + 0.6 * c * (1 - ro)

def mean_active_servers(c, ro, mu=1.0):
    return c * ro

def mean_calls(c, ro, mu=1.0):
    r = c * ro
    r0 = 0.0
    assert trunc(c) == c, 'oh, number of server not integer'
    for k in range(int(c) + 1):
        r0 += r ** k / factorial(k)
    r0 += r ** (c + 1) / (factorial(c) * (c - r))
    r0 = 1 / r0
    res =  r ** (c + 1) * r0
    res /= c * factorial(c) * (1 - ro) * (1 - ro)
    res += r
    return res