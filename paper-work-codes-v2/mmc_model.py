import numpy as np
from output_module import write_matrix_in_file

c = int(input('c = '))
rho = int(input('rho = '))

N = 100
Ca = 1
Ci = 0.6 * Ca

alphas = np.linspace(0.01, 10, N)
results = np.ones((N, 2))
results[:, 0] = alphas
results[:, 1] = Ci * c * rho + Ca * c * (1 - rho)


