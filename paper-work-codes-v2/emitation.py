import numpy as np 
import time 

c = 2
r = 1

l = 1.0
a = 2.0
m = 3.0

currentState = np.zeros(r + 2)

while True:
	X = []
	print(currentState)
	X.append(np.random.exponential(1.0/l))
	k = np.nonzero(currentState[2:])[0]
	i = currentState[0]
	totalServers = i + np.sum(currentState[2:])
	for kq in k:
		ykq = currentState[2:][kq]
		X.append(np.random.exponential(ykq/a))
	Y = 100000.0
	if i != 0:
		Y = np.random.exponential(i/m)

	t = 0
	Xmin = 100000.0
	for tt, Xt in enumerate(X):
		if Xt < Xmin:
			Xmin = Xt
			t = tt

	if Y < Xmin:
		t = -1		
#	print("Y = ", Y)
	if t == 0:
#		print("task arived")
		currentState[1] = currentState[1] + 1
		if totalServers < c:
			currentState[2] = currentState[2] + 1
	elif t == -1:
#		print("task down")
		currentState[1] = currentState[1] - 1
		if len(k) != 0 and currentState[1] <= c - 1:
			currentState[2:][k[0]] = currentState[2:][k[0]] - 1
		else:
			if currentState[1] <= c - 1:
				currentState[0] = currentState[0] - 1 
	elif t > 0:
#		print("t = ", t)
#		print("len(k)", len(k))
#		print("new phase from", k[t-1])
		if k[t-1] == r-1:
			currentState[2:][k[t-1]] = currentState[2:][k[t-1]] - 1
			currentState[0] = currentState[0] + 1
		else:	
			currentState[2:][k[t-1]] = currentState[2:][k[t-1]] - 1
			currentState[2:][k[t-1]+1] = currentState[2:][k[t-1]+1] + 1
	time.sleep(1.0)
#	print(X)	
#	input()




