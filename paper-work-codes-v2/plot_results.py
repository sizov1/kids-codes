import matplotlib.pyplot as plt
import os
import numpy as np
from math import factorial
from numpy import genfromtxt

colors = [
    'blue',
    'springgreen',
    'red',
    'orange',
    'black'
]

def data_has_same_x(experiments):
    x = experiments[0][:, 0]
    for data in experiments:
        xx = data[:, 0]
        if not all(x == xx):
            return False
    return True

def mean_calls(r, c):
    r0 = 0
    for i in range(c + 1):
        r0 += ((r * c) ** i) / factorial(i)
    r0 = ((r * c) ** (c + 1)) / (factorial(c) * c * (1 - r))
    r0 = 1 / r0
    return ((r * c) ** (c + 1) * r0) \
        / (c * factorial(c) * (1 - r)**2) \
        + r * c

def get_legend(rhos):
    idle = 'Система без выключений ρ = '
    setup = 'Система с разогревом ρ = '
    return [idle + str(-rho) if rho < 0 else setup + str(rho)
         for rho in rhos]

def get_data():
    print('Choose your files:')
    files = [os.path.join('.\\results', f)
        for f in os.listdir('.\\results') if f.endswith('.csv')]
    for i, f in enumerate(files):
        print(i, f)

    user_files = [files[int(a)] for a in input().split()]
    experiments = []
    rho_set = {}
    rhos = []
    for f in user_files:
        res = genfromtxt(f, delimiter=';')
        experiments.append(res)
        filename = os.path.basename(f)
        c = int(filename.split('_')[0])
        rho = float(filename.split('_')[2])
        rhos.append(rho)
        if rho not in rho_set:
            n = res.shape[0]
            rhos.append(-rho)
            mmc_res = np.ones((n, 3)) * mean_calls(rho, c)
            mmc_res[:, 0] = res[:, 0]
            experiments.append(mmc_res)

    if not data_has_same_x(experiments):
        assert('I cant plot with different x data')
    return experiments, rhos

plt.rc('font', family='serif')

fig, ax = plt.subplots()
experiments, rhos = get_data()
sizes = 12.0 * np.ones(experiments[0].shape[0])
for i, data in enumerate(experiments):
    ax.scatter(data[:, 0], data[:, 2], sizes, marker = 's', color=colors[i])

ax.set_xlabel('α', fontsize=14)
ax.set_ylabel('Среднее число требований', fontsize=14)
#ax.set_title('c = 30; r = 2')

plt.legend(get_legend(rhos))
plt.grid()
plt.show()

# to - do:
# - create more data
# - add beaty legend
# - analyze
# - write about what you see