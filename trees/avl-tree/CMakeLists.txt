cmake_minimum_required (VERSION 3.0)

project(AvlTree VERSION 1.2.3.4 LANGUAGES C CXX)

add_executable(main ./src/main.cpp)

add_library(AvlTree ./src/avl-tree.cpp)

target_link_libraries(main AvlTree)