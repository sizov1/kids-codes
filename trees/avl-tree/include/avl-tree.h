#ifndef __AVLTREE_H
#define __AVLTREE_H

#include <iostream>
template <class T> 
class Node
{
    public:
        T data;
        Node<T>* left;
        Node<T>* right;

        Node(T value) : data(value), left(nullptr), right(nullptr) {}
};

template <typename T>
class Tree 
{
    private:
        Node<T>* phead;
    public:
        Tree() : phead(nullptr) {}
        ~Tree() { delete phead; }

        void Insert(T val);
};

#endif
