#ifndef  __TREE_H
#define __TREE_H

#include <iostream>
#include <queue>
#include "myIterator.h"

using namespace std;

template <typename T>
struct Node
{
    T data;
    PTNode left;
    PTNode right;

    Node() : data(), left(NULL), right(NULL) {}
    Node(T _data) : data(_data), left(NULL), right(NULL) {}

};

template <typename T>
void PrintBFS(PTNode root);

template <typename T>
void PrintDFS(PTNode root);

template <typename T>
class Tree
{
    private:
        PTNode root;
        stack<PTNode> st;

        bool isLeaf(PTNode pn);    
        void DeleteLeaf(T _data);  

        PTNode GetMax(PTNode pn);  
        PTNode GetMin(PTNode pn);  

    public:
        Tree();

        void Insert(T _data);
        void Delete(T _data);
        PTNode Find(T _data);

        void Print();

        myIterator<T> begin();
        myIterator<T> end();
};


template <typename T>
void PrintBFS(PTNode root)
{
    queue<PTNode> qn;
    qn.push(root);
    while (!qn.empty()) {
        PTNode curr = qn.front();
        cout << qn.front()->data << endl;

        if (curr->left) {
            qn.push(curr->left);
        }
        if (curr->right) {
            qn.push(curr->right);
        }

        qn.pop();
    }
}

template <typename T>
void PrintDFS(PTNode root)
{
    stack<PTNode> stn;
    PTNode curr = root;
    stn.push(curr);
    while (!stn.empty()) {
        PTNode pcn = stn.top();
        cout << pcn->data << endl;

        stn.pop();

        if (pcn->right) {
            stn.push(pcn->right);
        }
        if (pcn->left) {
            stn.push(pcn->left);
        }
    }
}

template <typename T>
Tree<T>::Tree()
{
    root = NULL;
}

template <typename T>
void Tree<T>::Print()
{
    PrintDFS(root);
}

template <typename T>
void Tree<T>::Insert(T _data)
{
    Node<T>* ins = new Node<T>(_data);
    if (!root) {
        root = ins;
        return;
    }

    bool flag = true;  // true => left; false => right
    PTNode curr = root;
    PTNode prev = curr;

    while (curr) {
        prev = curr;

        if (curr->data < _data) {
            curr = curr->right;
            flag = false;
        }
        else if (curr->data > _data) {
            curr = curr->left;
            flag = true;
        }
        else {
            return;
        }
    }

    if (flag) {
        prev->left = ins;
    }
    else {
        prev->right = ins;
    }

}

template <typename T>
PTNode Tree<T>::GetMax(PTNode pn)
{
    PTNode curr = pn;
    while (curr) {
        if (curr->right) {
            curr = curr->right;
        }
        else {
            break;
        }
    }
    return curr;
}

template <typename T>
PTNode Tree<T>::GetMin(PTNode pn)
{
    PTNode curr = pn;
    while (curr) {
        if (curr->left) {
            curr = curr->left;
        }
        else {
            break;
        }
    }
    return curr;
}

template <typename T>
PTNode Tree<T>::Find(T _data)
{
    PTNode curr = root;
    while (curr) {
        if (curr->data == _data) {
            return curr;
        }
        else if (curr->data > _data) {
            curr = curr->left;
        }
        else {
            curr = curr->right;
        }
    }
    return NULL;
}

template <typename T>
bool Tree<T>::isLeaf(PTNode pn)
{
    return !((pn->left) || (pn->right));
}

template <typename T>
void Tree<T>::DeleteLeaf(T _data)
{

    PTNode curr = root;
    PTNode prev = curr;
    bool flag = true;  // true => left; false => right

    while (curr) {
        if (curr->data > _data) {
            flag = true;
            prev = curr;
            curr = curr->left;
        }
        else if (curr->data < _data) {
            flag = false;
            prev = curr;
            curr = curr->right;
        }
        else {

            if (isLeaf(curr)) {
                if (flag) {
                    prev->left = NULL;
                }
                else {
                    prev->right = NULL;
                }
            }
            return;
        }
    }
}

template <typename T>
void Tree<T>::Delete(T _data)
{
    PTNode del = Find(_data);
    if (!del) return;

    if (del->left) {
        T max = GetMax(del->left)->data;

        DeleteLeaf(max);
        del->data = max;
    }
    else if (del->right) {
        T min = GetMin(del->right)->data;

        DeleteLeaf(min);
        del->data = min;
    }
    else {
        DeleteLeaf(del->data);
    }

}

template <typename T>
myIterator<T> Tree<T>::begin()
{
    myIterator<T> mit;

    PTNode curr = root;
    while (curr) {
        if (curr->left) {
            mit.st.push(curr);
            curr = curr->left;
        }
        else {
            break;
        }
    }
    
    mit.pn = curr;
    return mit;
}

template <typename T>
myIterator<T> Tree<T>::end()
{
    myIterator<T> mit(NULL);

    return mit;
}






#endif 

