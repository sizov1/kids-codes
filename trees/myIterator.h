#ifndef __MYITERATOR_H
#define __MYITERATOR_H

#include <stack>
using std::stack;

template <typename T>
struct Node;

#define PTNode Node<T>*

template <typename T>
struct myIterator : public std::iterator<std::input_iterator_tag, Node<T>>
{
    private:
        PTNode pn;
        stack<PTNode> st;

    public:
        myIterator(PTNode _pn = NULL) : pn(_pn) {}
        myIterator(const myIterator& mit) : pn(mit.pn), st(mit.st) {}

        myIterator<T>& operator++();
        bool operator == (const myIterator& mit) { return (pn == mit.pn); }
        bool operator != (const myIterator& mit) { return (pn != mit.pn); }
        PTNode operator*() { return pn; }

        template <typename T>
        friend class Tree;
};


template <typename T>
myIterator<T>& myIterator<T>::operator++()
{
    if (st.empty()) {
        pn = NULL;
    }
    else {
        pn = st.top();
        st.pop();
        if (pn->right) {
            PTNode curr = pn->right;
            while (curr) {
                st.push(curr);
                curr = curr->left;
            }
        }
    }
    return *this;
}

#endif 
