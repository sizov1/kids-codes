﻿#include <iostream>
#include <queue>
#include <stack>

using namespace std;

#define PTNode Node<T>*

template <typename T>
struct Node
{
    T data;
    PTNode left;
    PTNode right;

    Node() : data(), left(NULL), right(NULL) {}
    Node(T _data) : data(_data), left(NULL), right(NULL) {}

};

template <typename T>
void PrintBFS(PTNode root)
{
    queue<PTNode> qn;
    qn.push(root);
    while (!qn.empty()) {
        PTNode curr = qn.front();
        cout << qn.front()->data << endl;

        if (curr->left) {
            qn.push(curr->left);
        }
        if (curr->right) {
            qn.push(curr->right);
        }

        qn.pop();
    }
}

template <typename T>
void PrintDFS(PTNode root)
{
    stack<PTNode> stn;
    PTNode curr = root;
    stn.push(curr);
    while (!stn.empty()) {
        PTNode pcn = stn.top();
        cout << pcn->data << endl;

        stn.pop();

        if (pcn->right) {
            stn.push(pcn->right);
        }
        if (pcn->left) {
            stn.push(pcn->left);
        }
    }
}

template <typename T>
class Tree
{
    private:
        PTNode root;

        bool isLeaf(PTNode pn);    // является ли узел листом?
        void DeleteLeaf(T _data);  // удаляет лист с заданным значение

        PTNode GetMax(PTNode pn);  // возращает указатель на максимальный в поддереве
        PTNode GetMin(PTNode pn);  // возращает указатель на минимальный в поддереве

        
      
    public: 
        Tree();

        void Insert(T _data, PTNode pn = NULL);
        void Delete(T _data);
        PTNode Find(T _data);

        void Print();
};

template <typename T>
Tree<T>::Tree()
{
    root = NULL;
}

template <typename T>
void Tree<T>::Print()
{
    PrintDFS(root);
}

//template <typename T>
//void Tree<T>::Insert(T _data)
//{    
//    Node<T>* ins = new Node<T>(_data);
//    if (!root) {
//        root = ins;
//        return;
//    }
//
//    bool flag = true;  // true => left; false => right
//    PTNode curr = root;
//    PTNode prev = curr;
//
//    while (curr) {
//        prev = curr;        
//
//        if (curr->data < _data) {
//            curr = curr->right;
//            flag = false;
//        }
//        else if (curr->data > _data) {
//            curr = curr->left;
//            flag = true;
//        }
//        else {
//            return;
//        }
//    }
//
//    if (flag) {
//        prev->left = ins;
//    }
//    else {
//        prev->right = ins;
//    }
//
//}

template <typename T>
void Tree<T>::Insert(T _data)
{
    InsertNode(_data, root);
}

template <typename T>
PTNode Tree<T>::GetMax(PTNode pn)
{
    PTNode curr = pn;
    while (curr) {
        if (curr->right) {
            curr = curr->right;
        }
        else {
            break;
        }
    }
    return curr;
}

template <typename T>
PTNode Tree<T>::GetMin(PTNode pn)
{
    PTNode curr = pn;
    while (curr) {
        if (curr->left) {
            curr = curr->left;
        }
        else {
            break;
        }
    }
    return curr;
}

template <typename T>
PTNode Tree<T>::Find(T _data)
{
    PTNode curr = root;
    while (curr) {
        if (curr->data == _data) {
            return curr;
        }
        else if (curr->data > _data) {
            curr = curr->left;
        }
        else {
            curr = curr->right;
        }
    }
    return NULL;
}

template <typename T>
bool Tree<T>::isLeaf(PTNode pn)
{
    return !((pn->left) || (pn->right));
}

template <typename T>
void Tree<T>::DeleteLeaf(T _data)
{
    // функция удаляет лист дерева

    PTNode curr = root;
    PTNode prev = curr;
    bool flag = true;  // true => left; false => right

    // ищем нужный лист в дереве
    while (curr) {
        if (curr->data > _data) {
            flag = true;
            prev = curr;
            curr = curr->left;
        }
        else if (curr->data < _data) {
            flag = false;
            prev = curr;
            curr = curr->right;
        }
        else {
            // проверяем является ли найденный узел листом
            if (isLeaf(curr)) {
                if (flag) {
                    prev->left = NULL;
                }
                else {
                    prev->right = NULL;
                }
            }
            return;
        }
    }
}

template <typename T>
void Tree<T>::Delete(T _data)
{
    PTNode del = Find(_data);  //ищем узел, который нужно удалить
   
    if (!del) return;

    if (del->left) {
        // если удаляемое звено имеет поддеwрево в левой ветке
        // то ищем максимальный элемент в этом поддереве
        T max = GetMax(del->left)->data;

        // удаляем лист со значением максимального в поддереве
        DeleteLeaf(max);
        // присваем удаляемому звену значение максимального в поддереве
        del->data = max;
    } 
    else if (del->right) {
        T min = GetMin(del->right)->data;
    
        DeleteLeaf(min);
        del->data = min;
    }
    else {
        DeleteLeaf(del->data);
    }
    
}

int main()
{
    Tree<int> alph;

    alph.Insert(10);
    alph.Insert(7);
    alph.Insert(4);
    alph.Insert(8);
    alph.Insert(5);
    alph.Insert(3);
    alph.Insert(9);
    alph.Insert(19);
    alph.Insert(27);
    alph.Insert(30);
    alph.Insert(22);
    alph.Insert(28);
    alph.Insert(21);
    alph.Insert(20);

    alph.Print();
    alph.Delete(17);
    cout << "===========" << endl;
    alph.Print();

    system("pause");
    return 0;
}
  