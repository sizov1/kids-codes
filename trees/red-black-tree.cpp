#include <iostream>
#include <stack>
#include <algorithm>

using namespace std;

enum Color
{
    RED, BLACK
};

class Node
{
public:

    Node() : pdata_(NULL), color_(BLACK), left_(NULL), right_(NULL) {}
    Node(int data);

    void SetData(int dt) { pdata_ = new int(dt); }
    void SetLeft(Node* pn) { left_ = pn; }
    void SetRight(Node* pn) { right_ = pn; }
    void SetColor(Color clr) { color_ = clr; }
    void SetTwoFixChild();

    int* pdata() { return pdata_; }
    int data() { return *pdata_; }
    Node* left() { return left_; }
    Node* right() { return right_; }
    Color color() { return color_; }

    bool isRed() { return color_ == RED; }
    bool isBlack() { return color_ == BLACK; }

private:
    int* pdata_;
    Node* left_;
    Node* right_;
    Color color_;
};

Node::Node(int data)
{
    pdata_ = new int(data);
    left_ = NULL;
    right_ = NULL;
    color_ = BLACK;
}

void Node::SetTwoFixChild()
{
    Node* pl = new Node();
    Node* pr = new Node();
    SetLeft(pl);
    SetRight(pr);
}

int height(Node* root)
{
    int a = 0, b = 0;
    if (root->left()->pdata()) {
        a = height(root->left());
    }

    if (root->right()->pdata()) {
        b = height(root->right());
    }

    return max(a, b) + 1;
}

class Tree
{
    public:
        Tree() : root() {}

        void print();
        int get_height();
        void insert(int data);
        void del(int data);
        void rbdel(Node* pn);
        Node* getNode(int data);
        Node* getParent(Node* child);
        Node* getBrother(Node* parent, Node* child);
        Node* getUncle(Node* grandparent, Node* parent);
        Node* getMinimum(Node* pn);
    private:
        Node * root;

        Node* find(int data);

        void MovePointerParent(Node* oldchild, Node* newchild);
        void PullUp(Node* child, Node* parent);
        void RedChildCase(Node* child, Node* parent, Node* brother);
        void LeftRedBrotherRotate(Node* child, Node* parent, Node* brother);
        void RightRedBrotherRotate(Node* child, Node* parent, Node* brother);
        void RightNephewRotate(Node* brother, Node* rightNephew);
        void LeftNephewRotate(Node* brother, Node* leftNephew);
        void ParentBlackBrotherRedCase(Node* child, Node* parent, Node* brother);
        void ParentBlackBrotherBlackCase(Node* child, Node* parent, Node* brother);
        void ParentRedBrotherBlackCase(Node* child, Node* parent, Node* brother);
    
        Node* BTSinsert(int data);
        void balance(Node* child);
        void case4(Node* child, Node* parent, Node* grandparent, Node* uncle);
        void LeftLeftCase(Node* child, Node* parent, Node* grandparent, Node* uncle);
        void LeftRightCase(Node* child, Node* parent, Node* grandparent, Node* uncle);
        void RightRightCase(Node* child, Node* parent, Node* grandparent, Node* uncle);
        void RightLeftCase(Node* child, Node* parent, Node* grandparent, Node* uncle);

};

void Tree::print()
{
    stack<Node*> stn;
    Node* curr = root;
    stn.push(curr);
    while (!stn.empty()) {
        Node* pcn = stn.top();
        cout << pcn->data() << endl;

        stn.pop();

        if (pcn->right()->pdata()) {
            stn.push(pcn->right());
        }
        if (pcn->left()->pdata()) {
            stn.push(pcn->left());
        }
    }
}

int Tree::get_height()
{
    return height(root);
}

Node* Tree::BTSinsert(int data)
{
    Node* ins = new Node(data);

    if (!root) {
        root = ins;
        return ins;
    }

    Node* curr = root;
    Node* prev = curr;
    while (curr->pdata()) {
        prev = curr;
        if (ins->data() > curr->data()) {
            curr = curr->right();
        }
        else if (ins->data() < curr->data()) {
            curr = curr->left();
        }
        else {
            return NULL;
        }
    }

    if (ins->data() > prev->data()) {
        prev->SetRight(ins);
    }
    else if (ins->data() < prev->data()) {
        prev->SetLeft(ins);
    }
    return ins;
}

Node* Tree::getNode(int data)
{
    Node* curr = root;
    while (curr->pdata()) {
        if (data > curr->data()) {
            curr = curr->right();
        }
        else if (data < curr->data()) {
            curr = curr->left();
        }
        else {
            return curr;
        }
    }
}

Node* Tree::getParent(Node* child)
{
    Node* curr = root;
    Node* parent = curr;
    while (curr->pdata()) {
        if (child->data() > curr->data()) {
            parent = curr;
            curr = curr->right();
        }
        else if (child->data() < curr->data()) {
            parent = curr;
            curr = curr->left();
        }
        else {
            return parent;
        }
    }
    cerr << "cannot get_parent: child not found" << endl;
    return NULL;
}

Node* Tree::getUncle(Node* grandparent, Node* parent)
{
    if (parent->data() > grandparent->data()) {
        return grandparent->left();
    }
    else {
        return grandparent->right();
    }
}

Node* Tree::getBrother(Node* parent, Node* child)
{
    if (!parent->pdata()) {
        cout << "cannot get brother: parent is NULL";
        return NULL;
    }

    if (child->data() > parent->data()) {
        return parent->left();
    }

    return parent->right();
}

void Tree::MovePointerParent(Node* oldchild, Node* newchild)
{
    Node* parent = getParent(oldchild);

    if (oldchild->data() > parent->data()) {
        parent->SetRight(newchild);
    }
    else if (oldchild->data() < parent->data()) {
        parent->SetLeft(newchild);
    }
    else if (oldchild == root) {
        root = newchild;
    }
}

void Tree::LeftLeftCase(Node* child, Node* parent, Node* grandparent, Node* uncle)
{
    MovePointerParent(grandparent, parent);

    grandparent->SetLeft(parent->left());
    parent->SetRight(grandparent);

    parent->SetColor(BLACK);
    grandparent->SetColor(RED);
}

void Tree::LeftRightCase(Node* child, Node* parent, Node* grandparent, Node* uncle)
{
    grandparent->SetLeft(child);
    parent->SetRight(child->left());
    child->SetLeft(parent);
    LeftLeftCase(parent, child, grandparent, uncle);
}

void Tree::RightRightCase(Node* child, Node* parent, Node* grandparent, Node* uncle)
{
    MovePointerParent(grandparent, parent);

    grandparent->SetRight(parent->left());
    parent->SetLeft(grandparent);

    parent->SetColor(BLACK);
    grandparent->SetColor(RED);
}

void Tree::RightLeftCase(Node* child, Node* parent, Node* grandparent, Node* uncle)
{
    grandparent->SetRight(child);
    parent->SetLeft(child->left());
    child->SetRight(parent);
    RightRightCase(parent, child, grandparent, uncle);
}

void Tree::case4(Node* child, Node* parent, Node* grandparent, Node* uncle)
{
    if ((parent->left() == child) && (grandparent->left() == parent)) {
        LeftLeftCase(child, parent, grandparent, uncle);
    }
    else if ((parent->right() == child) && (grandparent->left() == parent)) {
        LeftRightCase(child, parent, grandparent, uncle);
    }
    else if ((parent->right() == child) && (grandparent->right() == parent)) {
        RightRightCase(child, parent, grandparent, uncle);
    }
    else if ((parent->left() == child) && (grandparent->right() == parent)) {
        RightLeftCase(child, parent, grandparent, uncle);
    }
}

void Tree::balance(Node* child)
{
    if (child == root) {
        child->SetColor(BLACK);
        return;
    }

    Node* parent = getParent(child);

    if (parent->isRed()) {
        Node* grandparent = getParent(parent);
        Node* uncle = getUncle(grandparent, parent);

        if (uncle->isRed()) {
            parent->SetColor(BLACK);
            uncle->SetColor(BLACK);
            grandparent->SetColor(RED);
            balance(grandparent);
        }
        else {
            case4(child, parent, grandparent, uncle);
        }
    }
}

void Tree::insert(int data)
{
    Node* child;
    child = BTSinsert(data);

    if (!child) {
        return;
    }

    child->SetColor(RED);
    child->SetTwoFixChild();

    balance(child);
}

Node* Tree::find(int data)
{
    Node* curr = root;
    while (curr->pdata()) {
        if (data > curr->data()) {
            curr = curr->right();
        }
        else if (data < curr->data()) {
            curr = curr->left();
        }
        else {
            return curr;
        }
    }
    cout << "cannot delete: not found" << endl;
    return NULL;
}

Node* Tree::getMinimum(Node* pn)
{
    Node* curr = pn->left();
    if (!curr->pdata()) {
        return pn;
    }
    while (curr->pdata()) {
        if (curr->left()->pdata()) {
            curr = curr->left();
        }
        else {
            return curr;
        }
    }
}

void Tree::del(int data)
{
    Node* ndel = find(data);

    if (ndel->right() && ndel->left()) {
        // ���� � ���������� ���� ���� ��� �������
        // ���� ����������� ���� � ������� ���������
        Node* nmin = getMinimum(ndel->right());
        // ���������� �������� ������������ ����            
        int minData = nmin->data();
        // ������� ����������� � ��������
        rbdel(nmin);
    }
    else {
        rbdel(ndel);
    }
}

void Tree::PullUp(Node* child, Node* parent)
{
    Node* childLeft = child->left();
    Node* childRight = child->right();
    bool direct = child->data() > parent->data() ? 1 : 0;
    Node* newchild = new Node();
    if (!childRight->pdata() && !childLeft->pdata()) {
        // ��� ������� �������� => �� ����� ���������� ������ ���� ��������
        newchild = childLeft;
    }
    else if (!childLeft->pdata() && childRight->pdata()) {
        newchild = childRight;
    }
    else if (childLeft->pdata() && !childRight->pdata()) {
        newchild = childLeft;
    }
    else {
        cout << "cannot pullup" << endl;
        return;
    }

    newchild->SetTwoFixChild();
    if (direct) {
        parent->SetRight(newchild);
    }
    else {
        parent->SetLeft(newchild);
    }
}

void Tree::RedChildCase(Node* child, Node* parent, Node* brother)
{
   PullUp(child, parent);
}

void Tree::RightRedBrotherRotate(Node* child, Node* parent, Node* brother)
{
    MovePointerParent(parent, brother);
    parent->SetRight(brother->left());
    brother->SetLeft(parent);
}

void Tree::LeftRedBrotherRotate(Node* child, Node* parent, Node* brother)
{
    MovePointerParent(parent, brother);
    parent->SetLeft(brother->right());
    brother->SetRight(parent);
}

void Tree::ParentBlackBrotherRedCase(Node* child, Node* parent, Node* brother)
{
    if (brother->data() > parent->data()) {
        RightRedBrotherRotate(child, parent, brother);
    }
    else {
        LeftRedBrotherRotate(child, parent, brother);
    }
    brother->SetColor(BLACK);
    parent->SetColor(RED);
}

void Tree::ParentBlackBrotherBlackCase(Node* child, Node* parent, Node* brother)
{
    Node* leftNephew = brother->left();
    Node* rightNephew = brother->right();
    brother->SetColor(RED);
    if (leftNephew->isRed() || rightNephew->isRed()) {
        if (leftNephew->isRed()) {
            leftNephew->SetColor(BLACK);
        }
        if (rightNephew->isRed()) {
            rightNephew->SetColor(BLACK);
        }
        ParentBlackBrotherRedCase(child, parent, brother);
    }
    else {
        PullUp(child, parent);
    }
}

void Tree::RightNephewRotate(Node* brother, Node* rightNephew)
{
    MovePointerParent(brother, rightNephew);
    brother->SetRight(rightNephew->left());
    rightNephew->SetLeft(brother);
}

void Tree::LeftNephewRotate(Node* brother, Node* leftNephew)
{
    MovePointerParent(brother, leftNephew);
    brother->SetLeft(leftNephew->right());
    leftNephew->SetRight(brother);
}

void Tree::ParentRedBrotherBlackCase(Node* child, Node* parent, Node* brother)
{
    Node* leftNephew = brother->left();
    Node* rightNephew = brother->right();

    if (leftNephew->isBlack() && rightNephew->isBlack()) {
        parent->SetColor(BLACK);
        brother->SetColor(RED);
        ParentBlackBrotherRedCase(child, parent, brother);
    }
    else if (leftNephew->isRed() && rightNephew->isBlack()) {
        LeftNephewRotate(brother, leftNephew);
        parent->SetColor(BLACK);
        leftNephew->SetColor(RED);
        rightNephew->SetColor(RED);
        ParentBlackBrotherRedCase(child, parent, leftNephew);
    }
    else if (leftNephew->isBlack() && rightNephew->isRed()) {
        RightNephewRotate(brother, rightNephew);
        parent->SetColor(BLACK);
        rightNephew->SetColor(RED);
        leftNephew->SetColor(RED);
        ParentBlackBrotherRedCase(child, parent, rightNephew);
    }
    else {
        parent->SetColor(BLACK);
        brother->SetColor(RED);
        rightNephew->SetColor(BLACK);
        leftNephew->SetColor(BLACK);
        ParentBlackBrotherRedCase(child, parent, brother);
    }
}

void Tree::rbdel(Node* child)
{
    Node* parent = getParent(child);
    Node* brother = getBrother(parent, child);
    
    if (child->isRed()) {
        RedChildCase(child, parent, brother);
    }
    else {
        if (parent->isBlack() && brother->isRed()) {
            ParentBlackBrotherRedCase(child, parent, brother);
        }
        else if (parent->isBlack() && brother->isBlack()) {
            ParentBlackBrotherBlackCase(child, parent, brother);
        }
        else if (parent->isRed() && brother->isBlack()) {
            ParentRedBrotherBlackCase(child, parent, brother);
        }
    }
}

int main()
{
    Tree banzai;
    for (int i = 0; i < 1024; i++) {
        banzai.insert(i);
    }

    banzai.del(255);

    banzai.print();
 

    system("pause");
    return 0;
}