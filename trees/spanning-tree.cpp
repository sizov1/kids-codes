#include <random>
#include <vector>
#include <iomanip>
#include <iostream>
#include <queue>

using namespace std;

typedef vector<vector<int>> matrix;

struct Edge
{
    int v1;
    int v2;
    int weight;

    Edge(int _v1, int _v2, int _weight) : 
                v1(_v1), v2(_v2), weight(_weight) {}

    bool operator > (const Edge& e) const
    {
        return weight > e.weight;
    }
};

void PrintIntVector(vector<int> vec)
{
    int size = vec.size();
    for (int i = 0; i < size; i++) {
        cout << i << " ";
    }
}

void PrintMatrix(matrix mt)
{
    int size = mt.size();
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            cout << setw(20) << mt[i][j];
        }
        cout << endl;
    }
}

matrix zeros(int size)
{
    matrix mt(size);
    for (int i = 0; i < size; i++) {
        vector<int> tmp(size);
        mt[i] = tmp;
    }
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            mt[i][j] = 0;
        }
    }
    return mt;
}

int GetIntRandom(int begin, int end)
{
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> dis(begin, end);
    return dis(gen);
}

double GetDoubleRandom()
{
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> dis(0.0, 10.0);
    return dis(gen);
}

matrix GenerateGraph(int size, double precent)
{
    matrix graph;
    graph = zeros(size);

    vector<int> exist;  // пройденные вершины 
    vector<int> free;   // непройденные вершины

    exist.push_back(0);  // добавляем первую вершину
    for (int i = 1; i < size; i++) {  // инициализация массива непройденных вершин
        free.push_back(i);
    }

    for (int i = 0; i < size - 1; i++) {
        // обеспечиваем односвязность  
        int j = GetIntRandom(0, free.size() - 1); // индекс (в списке непройденных) добавляемой вершины 
        int k = GetIntRandom(0, exist.size() - 1);  // индекс (в списке пройденных) вершины с которой будем связывать
        int val_jk = GetIntRandom(0, 20);  // генерируем вес ребра связывающий вершины
        graph[free[j]][exist[k]] = graph[exist[k]][free[j]] = val_jk;     // добавляем новое ребро со сгенерированным весом 
        int tmp = free[j];
        free[j] = free[free.size() - 1];  // удаляем добавленную вершину из списка непройденных
        free.pop_back();
        exist.push_back(tmp);  // добавляем новую вершину в список пройденных
    }
    // добавляем оставшиеся ребра 
    // ищем пары несвязанных вершин
    vector<pair<int, int>> zeros_vertices;
    for (int i = 0; i < size; i++) {
        for (int j = i; j < size; j++) {
            if (i != j && graph[i][j] == 0) {
                pair<int, int> zeros_vertex(i, j);
                zeros_vertices.push_back(zeros_vertex);
            }
        }
    }
    long long int nedges = (long long int)((size * (size - 1) / 2) * precent); // требуемое кол-во ребер     
    int curr_nedges = size - 1;  // текущее кол-во ребер 
    while (curr_nedges < nedges) {
        int p = GetIntRandom(0, zeros_vertices.size() - 1);  // генерируем случайную пару, кот. будем связывать
        int val = GetIntRandom(0, 20);  // генерируем вес ребра связывающий вершины
        graph[zeros_vertices[p].first][zeros_vertices[p].second] = val; // добавляем новое ребро со сгенерированным весом 
        graph[zeros_vertices[p].second][zeros_vertices[p].first] = val;
        zeros_vertices[p] = zeros_vertices[zeros_vertices.size() - 1];  // удаляем пару из списка несвязнных вершин
        zeros_vertices.pop_back();
        curr_nedges++;
    }
    return graph;
}

int CountNotNull(matrix graph)
{
    int count = 0;
    int size = graph.size();
    for (int i = 0; i < size; i++) {
        for (int j = i; j < size; j++) {
            if (i != j && graph[i][j] != 0.0) {
                count++;
            }
        }
    }

    return count;
}

vector<Edge> CreateSpanningTree(matrix graph)
{
    int size = graph.size();
    vector<Edge> spanning_tree; // ребра, добавленные в остовное дерево
    priority_queue<Edge, vector<Edge>, greater<Edge>> q;
    vector<bool> complete_vertex(size);
    for (auto it : complete_vertex) {
        it = false;
    }

    int curr_vertex = 0;    
    complete_vertex[0] = true;

    while (spanning_tree.size() != size - 1) {
        for (int j = 0; j < size; j++) {
            if (graph[curr_vertex][j] != 0) {
                if (complete_vertex[j] == false) {
                    Edge etmp(curr_vertex, j, graph[curr_vertex][j]);
                    q.push(etmp);
                }
            }
        }
        spanning_tree.push_back(q.top());
        curr_vertex = (q.top()).v2;
        complete_vertex[curr_vertex] = true;
        q.pop();
    }
    return spanning_tree;
}


int main()
{
    /*
    int size;
    cin >> size;
    matrix graph = GenerateGraph(size, 0.5);
    PrintMatrix(graph);
    double count_edges = CountNotNull(graph);
    double nedges = size * (size - 1);
    cout << (count_edges / nedges);
    */

    /*
    priority_queue<Edge, vector<Edge>, greater<Edge>> q;
    Edge e3(2, 4, 7);
    Edge e1(1, 2, 3);
    Edge e2(1, 3, 6);
    q.push(e3);
    q.push(e2);
    q.push(e1);
    cout << q.top().weight;
    q.pop();
    cout << q.top().weight;
    cin.get();  
    */

    int size;
    cin >> size;
    matrix graph = GenerateGraph(size, 0.8);
    PrintMatrix(graph);
    cout << endl << endl;
    vector<Edge> edgeList = CreateSpanningTree(graph);
    for (auto it : edgeList) {
        cout << it.v1 << " " << it.v2 << " " << it.weight << endl;
    }
    system("pause");
}