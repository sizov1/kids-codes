#include "tree.h"

int main()
{
    Tree<int> alph;

    alph.Insert(7);
    alph.Insert(3);
    alph.Insert(1);
    alph.Insert(5);
    alph.Insert(4);
    alph.Insert(6);
    alph.Insert(10);
    alph.Insert(8);
    alph.Insert(9);
    alph.Insert(11);



    for (myIterator<int> it = alph.begin(); it != alph.end(); ++it) {
        cout << (*(it))->data << endl;
    }
    
    system("pause");
    return 0;
}