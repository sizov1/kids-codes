﻿#include <iostream>
#include <string>
#include <vector>
#include <random>

using std::vector;
using std::cerr;
using std::cout;
using std::string;

class Exception
{
    protected:
        string msg;
    public:
        Exception(string _msg) : msg(_msg) {};

        string GetNameError()
        {
            return msg;
        }
};

template<typename T>
void swap(T* a, T* b)
{
    T tmp;
    tmp = *a;
    *a = *b;
    *b = tmp;
}

// вставка
// удаление минимума
// окучивание
// shiftup, shiftdown
template <typename T, int D>
class Heap
{
    private:
        vector<T> harr;
        int heapSize;
        int parent(int i);
        int leftChild(int i);
        int rightChild(int i);
        int getMinChild(int i);
        void shiftUp(int i);
        void shiftDown(int i);
    public:
        Heap() : heapSize(0) {}
        T getMin() { return harr[0]; }
        void delMin();
        void insert(T data);
        void heaplify(vector<T> data);
        void print()
        {
            for (int i = 0; i < heapSize; i++) {
                cout << harr[i] <<std::endl;
            }
        }
};

template <typename T, int D>
int Heap<T, D>::parent(int i)
{
    int p = (i - 1) / D;
}

template <typename T, int D>
int Heap<T, D>::leftChild(int i)
{
   int left = D * i + 1;
   if (heapSize <= left) {
        throw Exception("leftChild: cannot get left child");
   }
   else {
       return left;
   }
}

template <typename T, int D>
int Heap<T, D>::rightChild(int i)
{
    int right = D * i + D;
    if (heapSize <= right) {
        return heapSize - 1;
    }
    else {
        return right;
    }
}

template <typename T, int D>
int Heap<T, D>::getMinChild(int i)
{
    int left = leftChild(i);
    int right = rightChild(i);
    int min = left;
    for (int j = left; j <= right; j++) {
        if (harr[j] < harr[min]) {
            min = j;
        }
    }
    return min;
}

template <typename T, int D>
void Heap<T, D>::delMin()
{
    harr[0] = harr[heapSize - 1];
    harr.pop_back();
    heapSize--;
    shiftDown(0);
}

template <typename T, int D>
void Heap<T, D>::shiftUp(int i)
{
    if (i == 0) {
        return;
    }

    int p = (i - 1) / D;

    if (harr[i] < harr[p]) {
        swap(&harr[i], &harr[p]);
        shiftUp(p);
    }
}

template <typename T, int D>
void Heap<T, D>::shiftDown(int i)
{
    if ((i * D + 1) >= heapSize) {
        return;
    }
    
    int mc = getMinChild(i);
    if (harr[i] > harr[mc]) {
        swap(&harr[i], &harr[mc]);
        shiftDown(mc);
    }
}

template <typename T, int D>
void Heap<T, D>::insert(T data)
{
    harr.push_back(data);
    heapSize++;
    shiftUp(heapSize - 1);
}

template <typename T, int D>
void Heap<T, D>::heaplify(vector<T> data)
{
    harr = data;
    heapSize = harr.size();
    for (int i = heapSize / D; i >= 0; i--) {
        shiftDown(i);
    }
}

int main()
{
    std::random_device rd;
    std::uniform_real_distribution<double> dist(-100, 100);
    //try {
    //    Heap<double, 3> myHeap;
    //    for (int i = 0; i < 90; i++) {
    //        myHeap.insert(dist(rd));
    //    }
    //    double* res = new double[90];
    //    for (int i = 0; i < 90; i++) {
    //        res[i] = myHeap.getMin();
    //        myHeap.delMin();
    //    }
    //    for (int i = 0; i < 90; i++) {
    //        cout << res[i] << std::endl;
    //    }
    //}
    //catch (Exception exc) {
    //    std::cout << exc.GetNameError();
    //}

    vector<double> pd;
    for (int i = 0; i < 5; i++) {
        pd.push_back(dist(rd));
    }
    Heap<double, 3> testHeaplify;
    testHeaplify.heaplify(pd);
    testHeaplify.print();

    system("pause");
}
